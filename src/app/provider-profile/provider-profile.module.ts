import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProviderProfileRoutingModule } from './provider-profile-routing.module';
import { MatSnackBarModule } from '@angular/material/snack-bar';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ProviderProfileRoutingModule,
    MatSnackBarModule
  ]
})
export class ProviderProfileModule { }
