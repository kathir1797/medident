import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router, ActivatedRoute } from '@angular/router';
import { DdlistService } from 'app/services/config/ddlist.service';
import { ProviderService } from 'app/services/dataManager/provider.service';

@Component({
  selector: 'app-provider-profile',
  templateUrl: './provider-profile.component.html',
  styleUrls: ['./provider-profile.component.css']
})
export class ProviderProfileComponent implements OnInit {

  providerForm: FormGroup;
  stateList = new Array();
  cityList = new Array();
  UserList = new Array();
  toptitle: string ='Add Provider';
  btntext: string = 'Save';
  urlText : any;
  isDisabled: boolean = true;
  obj = localStorage.getItem('currentuser');

  constructor(private fb: FormBuilder, private ddlist: DdlistService, private provider: ProviderService, private snackBar: MatSnackBar, private router: Router, private activatedRoute: ActivatedRoute) {
    const url =this.router.url;
    this.urlText = url.split('/');
    console.log(this.urlText);
    
    this.providerForm = this.fb.group({
      Provider_ID: 0,
      Provider_Name: new FormControl({ value: '', disabled: this.urlText[2] == 'view'?  true: false }, Validators.required),
      Provider_Address: new FormControl({ value: '', disabled: this.urlText[2] == 'view'?  true: false }, Validators.required),
      Provider_City_ID: new FormControl({ value: '', disabled: this.urlText[2] == 'view'?  true: false }, Validators.required),
      Provider_State_ID: new FormControl({ value: '', disabled: this.urlText[2] == 'view'?  true: false }),
      Provider_PostCode: new FormControl({ value: '', disabled: this.urlText[2] == 'view'?  true: false }),
      Provider_Contact_No: new FormControl({ value: '', disabled: this.urlText[2] == 'view'?  true: false }),
      Provider_Contact_Person: new FormControl({ value: '', disabled: this.urlText[2] == 'view'?  true: false }),
      Provider_Status_ID: 1,
      User_ID: JSON.parse(this.obj).User_ID,
    });
    this.activatedRoute.params.subscribe(params => {
      if (params['Provider_ID'] != 'create') {
        this.toptitle = 'Edit Provider';
        this.btntext ='Update';
        this.provider.EditProvider(params['Provider_ID']).subscribe(response => {
          if (response.success) {
            this.providerForm.patchValue({
              Provider_ID: response.data[0].Provider_ID,
              Provider_Name: response.data[0].Provider_Name,
              Provider_Address: response.data[0].Provider_Address,
              Provider_City_ID: response.data[0].Provider_City_ID,
              Provider_State_ID: response.data[0].Provider_State_ID,
              Provider_PostCode: response.data[0].Provider_PostCode,
              Provider_Contact_No: response.data[0].Provider_Contact_No,
              Provider_Contact_Person: response.data[0].Provider_Contact_Person,
              Provider_Status_ID: response.data[0].Provider_Status_ID,
              User_ID: JSON.parse(this.obj).User_ID,
            });
          } else {
            this.snackBar.open(response.errorMessage, 'OK',{duration:3000});(response.errorMessage);
          }
        });
        
      }
    });
  }

  ngOnInit() {
    this.getStateDDList();
    this.getCityDDList();
   // this.getUserDDList();
    if(this.urlText[2] == 'view'){
      this.toptitle ="Provider Detail";
      this.isDisabled = false;
    }
  }

  getStateDDList() {
    this.ddlist.getStateList().subscribe(response => {
      if (response.success) {
        this.stateList = response.data;
      } else {
        this.snackBar.open(response.errorMessage, 'OK',{duration:3000});(response.errorMessage, "Ok");
      }
    });
  }

  getCityDDList() {
    this.ddlist.getCityList().subscribe(response => {
      if (response.success) {
        this.cityList = response.data;
      } else {
        this.snackBar.open(response.errorMessage, 'OK',{duration:3000});(response.errorMessage, "Ok");
      }
    });
  }

  getUserDDList() {
    this.ddlist.getUserList().subscribe(response => {
      if (response.success) {
        this.UserList = response.data;
      } else {
        this.snackBar.open(response.errorMessage, 'OK',{duration:3000});(response.errorMessage, "Ok");
      }
    });
  }

  addProvider() {
    if (this.providerForm.valid) {
      this.provider.addProvider(JSON.stringify(this.providerForm.value)).subscribe(response => {
        if (response.success) {
          this.snackBar.open('Provider Saved Successfully', 'OK');
          this.providerForm.reset();
          this.router.navigate(['/provider']);
        } else {
          this.snackBar.open(response.errorMessage, 'OK');
        }
      });
    } else {
      this.validateAllFormFields(this.providerForm);
      this.snackBar.open('Form cannot Invalid', 'OK');
    }
  }

  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach((field) => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({
          onlySelf: true
        });
      } else if (control instanceof FormGroup) { this.validateAllFormFields(control); }
    });
  }


}