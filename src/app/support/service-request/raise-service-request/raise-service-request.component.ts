import { SupportService } from './../../../services/support/support.service';
import { ConfigService } from './../../../services/config/config.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router, ActivatedRoute } from '@angular/router';
import { DdlistService } from 'app/services/config/ddlist.service';

@Component({
  selector: 'app-raise-service-request',
  templateUrl: './raise-service-request.component.html',
  styleUrls: ['./raise-service-request.component.css']
})
export class RaiseServiceRequestComponent implements OnInit {

  serviceRequestForm: FormGroup;
  urlText: any;
  obj = localStorage.getItem('currentuser');


  statusList = new Array();
  requestTypeList = new Array();





  raiseRequest = this.config.configJson.modules.Support.ServiceRequest.Add;
  toptitle: string = this.raiseRequest.Title;
  btntext: string = this.raiseRequest.btnText;
  isDisabled: boolean = true;

  constructor(private fb: FormBuilder, private router: Router, private ddlist: DdlistService,
    private snackBar: MatSnackBar, private activated: ActivatedRoute, private config: ConfigService, private support: SupportService) {
    const url = this.router.url
    this.urlText = url.split("/");
    this.initializeServiceRequestForm();
  }

  ngOnInit(): void {
    this.getStatusDDLists();
    this.getRequestType();
  }

  initializeServiceRequestForm() {
    this.serviceRequestForm = this.fb.group({
      Service_Request_ID: 0,
      Request_Type_ID: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      Request_Date: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      Request_Description: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      Request_By: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      Request_Status_ID: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      User_ID: JSON.parse(this.obj).User_ID
    })
  }

  getStatusDDLists() {
    this.ddlist.getStatusDDList().subscribe(response => {
      if (response.success) {
        this.statusList = response.data;
      } else {
        this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 })
      }
    });
  }

  getRequestType() {
    this.support.getRequestType().subscribe(response => {
      if (response.success) {
        this.requestTypeList = response.data
      } else {
        this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 })
      }
    })
  }


  saveServiceRequest() {
    this.support.getServiceRequest(JSON.stringify(this.serviceRequestForm.value)).subscribe(response => {
      if (response.success) {
        this.serviceRequestForm.reset();
        this.router.navigate(['/support']);
        this.snackBar.open('Request Raised Successfully', 'ok', { duration: 3000 })
      } else {
        this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 })
      }
    })
  }

}
