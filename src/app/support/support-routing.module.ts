import { RaiseServiceRequestComponent } from './service-request/raise-service-request/raise-service-request.component';
import { ServiceRequestComponent } from './service-request/service-request.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {
    path: '',
    component: ServiceRequestComponent
  },
  {
    path: 'raise',
    component: RaiseServiceRequestComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SupportRoutingModule { }
