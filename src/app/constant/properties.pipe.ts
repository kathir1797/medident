import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'properties'
})
export class PropertiesPipe implements PipeTransform {

  transform(value: string): any {
    let retNumber = Number(value);
    return isNaN(retNumber) ? 0 : retNumber;
  }

}
