import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router, ActivatedRoute } from '@angular/router';
import { DdlistService } from 'app/services/config/ddlist.service';
import { CompanyService } from 'app/services/dataManager/company.service';

@Component({
  selector: 'app-company-profile',
  templateUrl: './company-profile.component.html',
  styleUrls: ['./company-profile.component.css']
})
export class CompanyProfileComponent implements OnInit {

  date = new Date();
  companyForm: FormGroup;
  stateList = new Array();
  cityList = new Array();
  providerList = new Array();
  UserList = new Array();
  toptitle: string = 'Add Company';
  btntext: string = 'Save';
  urlText: any;
  isDisabled: boolean = true;
  obj = localStorage.getItem('currentuser');
  constructor(private fb: FormBuilder, private ddlist: DdlistService, private company: CompanyService, private snackBar: MatSnackBar, private router: Router, private activatedRoute: ActivatedRoute) {
    const url = this.router.url;
    this.urlText = url.split('/');
    console.log(this.urlText);

    this.companyForm = this.fb.group({
      Company_ID: 0,
      Registered_Date: new FormControl({ value: this.date, disabled: this.urlText[2] == 'view' ? true : false }),
      Company_Code: new FormControl({ value: '', disabled: this.urlText[2] == 'view' ? true : false }, Validators.required),
      Company_Name: new FormControl({ value: '', disabled: this.urlText[2] == 'view' ? true : false }, Validators.required),
      Registration_No: new FormControl({ value: '', disabled: this.urlText[2] == 'view' ? true : false }, Validators.required),
      Company_Address: new FormControl({ value: '', disabled: this.urlText[2] == 'view' ? true : false }),
      Company_City_ID: new FormControl({ value: '', disabled: this.urlText[2] == 'view' ? true : false }),
      Company_State_ID: new FormControl({ value: '', disabled: this.urlText[2] == 'view' ? true : false }),
      PostCode: new FormControl({ value: '', disabled: this.urlText[2] == 'view' ? true : false }),
      Telephone_No: new FormControl({ value: '', disabled: this.urlText[2] == 'view' ? true : false }, [Validators.pattern("^[0-9]*$"), Validators.minLength(11)]),
      Fax_No: new FormControl({ value: '', disabled: this.urlText[2] == 'view' ? true : false }),
      Company_Email: new FormControl({ value: '', disabled: this.urlText[2] == 'view' ? true : false }),
      Contact_Person_1: new FormControl({ value: '', disabled: this.urlText[2] == 'view' ? true : false }),
      Contact_Person_Email: new FormControl({ value: '', disabled: this.urlText[2] == 'view' ? true : false }),
      Telephone_No_1: new FormControl({ value: '', disabled: this.urlText[2] == 'view' ? true : false }, [Validators.pattern("^[0-9]*$"), Validators.minLength(11)]),
      Mobile_No_1: new FormControl({ value: '', disabled: this.urlText[2] == 'view' ? true : false }, [Validators.pattern("^[0-9]*$"), Validators.minLength(10)]),
      Agent_ID: new FormControl({ value: '', disabled: this.urlText[2] == 'view' ? true : false }, [Validators.pattern("^[0-9]*$"), Validators.required]),
      Facebook_ID: new FormControl({ value: '', disabled: this.urlText[2] == 'view' ? true : false }),
      LinkedIN_ID: new FormControl({ value: '', disabled: this.urlText[2] == 'view' ? true : false }),
      Twitter_ID: new FormControl({ value: '', disabled: this.urlText[2] == 'view' ? true : false }),
      Company_Status_ID: 1,
      User_ID: JSON.parse(this.obj).User_ID,
      // Provider_ID: new FormControl({ value: '', disabled: this.urlText[2] == 'view' ? true : false }),
      Contact_Person_2: "",
      Contact_Person_2_Email: "",
      Telephone_No_2: "",
      Mobile_No_2: "",

    });
    this.activatedRoute.params.subscribe(params => {
      if (params['Company_ID'] != 'create') {
        this.toptitle = 'Edit Company';
        this.btntext = 'Update';
        this.company.EditCompany(params['Company_ID']).subscribe(response => {
          if (response.success) {
            this.companyForm.patchValue({
              Company_ID: response.data[0].Company_ID,
              Registered_Date: response.data[0].Registered_Date,
              Company_Code: response.data[0].Company_Code,
              Company_Name: response.data[0].Company_Name,
              Registration_No: response.data[0].Registration_No,
              Company_Address: response.data[0].Company_Address,
              Company_City_ID: response.data[0].Company_City_ID,
              Company_State_ID: response.data[0].Company_State_ID,
              PostCode: response.data[0].PostCode,
              Telephone_No: response.data[0].Telephone_No,
              Fax_No: response.data[0].Fax_No,
              Company_Email: response.data[0].Company_Email,
              Contact_Person_1: response.data[0].Contact_Person_1,
              Contact_Person_Email: response.data[0].Contact_Person_Email,
              Telephone_No_1: response.data[0].Telephone_No_1,
              Mobile_No_1: response.data[0].Mobile_No_1,
              Agent_ID: response.data[0].Agent_ID,
              Facebook_ID: response.data[0].Facebook_ID,
              LinkedIN_ID: response.data[0].LinkedIN_ID,
              Twitter_ID: response.data[0].Twitter_ID,
              User_ID: JSON.parse(this.obj).User_ID,
              // Provider_ID: response.data[0].Provider_ID,
              Contact_Person_2: "",
              Contact_Person_2_Email: "",
              Telephone_No_2: "",
              Mobile_No_2: "",
              Company_Status_ID: 1,
            });
          } else {
            this.snackBar.open(response.errorMessage, 'OK', { duration: 3000 }); (response.errorMessage);
          }
        });

      }
    });
  }

  ngOnInit() {
    this.getStateDDList();
    this.getCityDDList();
    // this.getProviderDDList();
    // this.getUserDDList();
    if (this.urlText[2] == 'view') {
      this.toptitle = "Company Detail";
      this.isDisabled = false;
    }
  }

  getStateDDList() {
    this.ddlist.getStateList().subscribe(response => {
      if (response.success) {
        this.stateList = response.data;
      } else {
        this.snackBar.open(response.errorMessage, 'OK', { duration: 3000 }); (response.errorMessage, "Ok");
      }
    });
  }

  getCityDDList() {
    this.ddlist.getCityList().subscribe(response => {
      if (response.success) {
        this.cityList = response.data;
      } else {
        this.snackBar.open(response.errorMessage, 'OK', { duration: 3000 }); (response.errorMessage, "Ok");
      }
    });
  }

  getProviderDDList() {
    this.ddlist.getProviderList().subscribe(response => {
      if (response.success) {
        this.providerList = response.data;
      } else {
        this.snackBar.open(response.errorMessage, 'OK', { duration: 3000 }); (response.errorMessage, "Ok");
      }
    });
  }

  getUserDDList() {
    this.ddlist.getUserList().subscribe(response => {
      if (response.success) {
        this.UserList = response.data;
      } else {
        this.snackBar.open(response.errorMessage, 'OK', { duration: 3000 }); (response.errorMessage, "Ok");
      }
    });
  }

  addCompany() {
    if (this.companyForm.valid) {
      const companyCode = this.companyForm.get('Company_Name').value.substring(0, 5).split(' ').join('');
      this.companyForm.get('Company_Code').patchValue(companyCode)
      this.company.addCompany(JSON.stringify(this.companyForm.value)).subscribe(response => {
        if (response.success) {
          this.snackBar.open('Company Saved Successfully', 'OK');
          this.companyForm.reset();
          this.router.navigate(['/company']);
        } else {
          this.snackBar.open(response.errorMessage, 'OK');
        }
      });
    } else {
      this.validateAllFormFields(this.companyForm);
      this.snackBar.open('Form cannot Invalid', 'OK');
    }
  }

  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach((field) => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({
          onlySelf: true
        });
      } else if (control instanceof FormGroup) { this.validateAllFormFields(control); }
    });
  }
}
