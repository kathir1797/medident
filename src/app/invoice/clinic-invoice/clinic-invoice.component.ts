import { ConfigService } from './../../services/config/config.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-clinic-invoice',
  templateUrl: './clinic-invoice.component.html',
  styleUrls: ['./clinic-invoice.component.css']
})
export class ClinicInvoiceComponent implements OnInit {

  invoiceManager = this.cs.configJson.modules.InvoiceManager.ClinicInvoice.Add;
  invoicedetail = this.cs.configJson.modules.InvoiceManager.ClinicInvoice.DetailTitle;
  topTitle = this.cs.configJson.modules.InvoiceManager.CompanyInvoice.Title;

  constructor(private cs: ConfigService) { }

  ngOnInit(): void {
  }

}
