import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClinicInvoiceComponent } from './clinic-invoice.component';

describe('ClinicInvoiceComponent', () => {
  let component: ClinicInvoiceComponent;
  let fixture: ComponentFixture<ClinicInvoiceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClinicInvoiceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClinicInvoiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
