import { ConfigService } from './../../services/config/config.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-company-invoice',
  templateUrl: './company-invoice.component.html',
  styleUrls: ['./company-invoice.component.css']
})
export class CompanyInvoiceComponent implements OnInit {

  invoiceManager = this.cs.configJson.modules.InvoiceManager.CompanyInvoice.Add;
  // customertype = this.invoiceManager.CustomerType;
  // customerTypeSelection = this.invoiceManager.CustomerName;
  // invoiceNumber = this.invoiceManager.InvoiceNumber
  // POSONumber = this.invoiceManager.POSONumber;
  // InvoiceDate = this.invoiceManager.InvoiceDate
  // paymentDue = this.invoiceManager.PaymentDue.values;
  invoicedetail = this.cs.configJson.modules.InvoiceManager.CompanyInvoice.DetailTitle;
  topTitle = this.cs.configJson.modules.InvoiceManager.CompanyInvoice.Title;

  constructor(private cs: ConfigService) { }

  ngOnInit(): void {
  }

}
