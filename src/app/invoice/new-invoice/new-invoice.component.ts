import { InvoiceService } from './../../services/invoice/invoice.service';
import { SubmissionService } from './../../services/claims/submission.service';
import { CompanyService } from './../../services/dataManager/company.service';
import { ConfigService } from './../../services/config/config.service';
import { MatSelectChange } from '@angular/material/select';
import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, FormArray } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router, ActivatedRoute } from '@angular/router';
import { clinicsRequest } from 'app/models/dataManager';
import { DdlistService } from 'app/services/config/ddlist.service';
import { ClinicsService } from 'app/services/dataManager/clinics.service';
import { tap } from 'rxjs/operators';
import { getCompanyRequest } from 'app/models/getCompanyRequest';

@Component({
  selector: 'app-new-invoice',
  templateUrl: './new-invoice.component.html',
  styleUrls: ['./new-invoice.component.css']
})
export class NewInvoiceComponent implements OnInit {


  //Input Parameters From Child Componenet(CompanyInvoice, ClinicInvoice)
  @Input() toptitle: string;
  @Input() detailTitle: string;
  @Input() invoiceType: string;
  @Input() invoiceManager: any;




  btntext: string = 'Save';
  isDisabled: boolean = true;
  invoiceForm: FormGroup;
  invoiceDetailsForm: FormGroup
  obj = localStorage.getItem('currentuser');
  urlText: any;

  fieldArray: Array<any> = [];
  newAttribute: any = {};

  subTotal: number;

  companyInvoice: FormGroup;
  companyDetailsInvoice: FormGroup;

  clinicInvoice: FormGroup;
  clinicDetailsInvoice: FormGroup;


  statusList = new Array();
  clinicsList = new Array();
  companyList = new Array();
  claimsList = new Array();
  customerTypeList = new Array();
  taxTypeList = new Array();

  clinicsData: clinicsRequest = new clinicsRequest();
  getCompanyRequest: getCompanyRequest = new getCompanyRequest()

  constructor(private fb: FormBuilder, private router: Router, private activatedRoute: ActivatedRoute, private ddlist: DdlistService,
    private snackBar: MatSnackBar, private clinics: ClinicsService, private invoiceService: InvoiceService, private companyService: CompanyService, private tpaClaims: SubmissionService) {
    const url = this.router.url;
    this.urlText = url.split('/');
    this.initializeCompanyInvoiceForm();
    this.initializeClinicInvoiceForm();
    this.newAttribute.name = 1

  }

  ngOnInit(): void {
    this.clinicsData.Clinic_ID = 0;
    this.getCompanyRequest.Company_ID = 0
    this.getCustomerType();
    this.getTaxType();
    this.getStatusDDLists();
    this.getClinics().subscribe(response => { })
    this.getCompany().subscribe(response => { })
    this.getTPAClaims();
    this.generateInvoiceNumber(this.invoiceType).subscribe(response => { })
    this.companyDetailsInvoice = this.fb.group({
      companyinvoiceDetails: this.fb.array(
        [this.addCompanyInvoiceDetailsFormGroup()]
      )
    });
    if (this.invoiceType === 'Clinic') {
      this.clinicDetailsInvoice = this.fb.group({
        clinicinvoiceDetails: this.fb.array(
          [this.addInvoiceDetailsFormGroup()]
        )
      })
    }
    console.log(this.invoiceDetails)
  }

  //Customer Dropdown
  getClinics() {
    return this.clinics.getClinicsList(this.clinicsData).pipe(
      tap(response => {
        if (response.success) {
          this.clinicsList = response.data;
        } else {
          this.snackBar.open(response.errorMessage, "Ok", { duration: 3000 });
        }
      })
    );
  }

  getCompany() {
    return this.companyService.getCompany(this.getCompanyRequest).pipe(tap(response => {
      if (response.success) {
        this.companyList = response.data;
      } else {
        this.snackBar.open(response.errorMessage, "Ok", { duration: 3000 });
      }
    }))
  }

  getTPAClaims() {
    this.tpaClaims.getTPAClaims(0).subscribe(response => {
      if (response.success) {
        this.claimsList = response.data;
      } else {
        this.snackBar.open(response.errorMessage, "Ok", { duration: 3000 });
      }
    })
  }

  //Status Dropdown
  getStatusDDLists() {
    this.ddlist.getStatusDDList().subscribe(response => {
      if (response.success) {
        this.statusList = response.data;
      } else {
        this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 })
      }
    });
  }

  //CustomerType
  getCustomerType() {
    this.ddlist.getCustomerType(0).subscribe(response => {
      if (response.success) {
        this.customerTypeList = response.data;
      } else {
        this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 });
      }
    })
  }

  //TaxType
  getTaxType() {
    this.ddlist.getTaxType(0).subscribe(response => {
      if (response.success) {
        this.taxTypeList = response.data;
      } else {
        this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 });
      }
    })
  }

  initializeClinicInvoiceForm() {
    this.clinicInvoice = this.fb.group({
      TPA_Invoice_ID: 0,
      Customer_Type_ID: new FormControl({ value: 2, disabled: true }),
      Clinic_ID: new FormControl(),
      Invoice_Number: new FormControl(),
      TPA_Claim_ID: new FormControl(),
      Invoice_Date: new FormControl(),
      Payment_Term: new FormControl(7),
      Invoice_Amount: new FormControl(),
      Tax_Type_ID: new FormControl(),
      Tax_Amount: new FormControl(),
      Invoice_Status_ID: new FormControl({ value: 1, disabled: true }),
      User_ID: JSON.parse(this.obj).User_ID,
    });
    // this.clinicDetailsInvoice = this.fb.group({
    //   clinicinvoiceDetails: this.fb.array(
    //     [{
    //       TPA_Invoice_Detail_ID: 0,
    //       TPA_Invoice_ID: new FormControl(),
    //       Item_Name: new FormControl(),
    //       Item_Description: new FormControl(),
    //       Item_Quantity: new FormControl(1),
    //       Item_Price: new FormControl(),
    //       Item_Amount: new FormControl(),
    //       Invoice_Detail_Status_ID: 1,
    //       User_ID: JSON.parse(this.obj).User_ID,
    //     }]
    //   )
    // })
  }

  initializeCompanyInvoiceForm() {
    this.companyInvoice = this.fb.group({
      TPA_Invoice_ID: 0,
      Customer_Type_ID: new FormControl({ value: 1, disabled: true }),
      Customer_ID: new FormControl(),
      Invoice_Number: new FormControl(),
      TPA_Claim_ID: new FormControl(),
      Invoice_Date: new FormControl(),
      Payment_Term: new FormControl(7),
      Invoice_Amount: new FormControl(),
      Tax_Type_ID: new FormControl(),
      Tax_Amount: new FormControl(),
      Invoice_Status_ID: new FormControl({ value: 1, disabled: true }),
      User_ID: JSON.parse(this.obj).User_ID,
    });
    // this.companyDetailsInvoice = this.fb.group({
    //   companyinvoiceDetails: this.fb.array(
    //     [this.addCompanyInvoiceDetailsFormGroup()]
    //   )
    // })
  }

  addFieldValue() {
    this.fieldArray.push(this.newAttribute)
    this.newAttribute = {};
    console.log(this.fieldArray)
  }

  deleteFieldValue(index) {
    this.invoiceDetails.removeAt(index)
  }

  onCustomerTypeChange(event: MatSelectChange) {

  }

  get invoiceDetails() {
    if (this.invoiceType === 'Company') return this.companyDetailsInvoice.get('companyinvoiceDetails') as FormArray
    if (this.invoiceType === 'Clinic') return this.clinicDetailsInvoice.get('clinicinvoiceDetails') as FormArray
  }

  addInvoiceDetails() {
    if (this.invoiceType === 'Company') {
      (<FormArray>this.companyDetailsInvoice.get('companyinvoiceDetails')).push(this.addCompanyInvoiceDetailsFormGroup())
    }
    if (this.invoiceType === 'Clinic') {
      (<FormArray>this.clinicDetailsInvoice.get('clinicinvoiceDetails')).push(this.addInvoiceDetailsFormGroup())
    }
  }

  addCompanyInvoiceDetailsFormGroup(): FormGroup {
    return this.fb.group({
      TPA_Company_Invoice_Detail_ID: 0,
      TPA_Company_Invoice_ID: '',
      Item_Name: '',
      Item_Description: '',
      Item_Quantity: 1,
      Item_Price: 0,
      Item_Amount: '',
      Invoice_Detail_Status_ID: 1,
      User_ID: JSON.parse(this.obj).User_ID
    })
  }

  addInvoiceDetailsFormGroup(): FormGroup {
    return this.fb.group({
      TPA_Clinic_Invoice_Detail_ID: 0,
      TPA_Clinic_Invoice_ID: new FormControl(),
      Item_Name: new FormControl(),
      Item_Description: new FormControl(),
      Item_Quantity: new FormControl(1),
      Item_Price: new FormControl(),
      Item_Amount: new FormControl(),
      Invoice_Detail_Status_ID: 1,
      User_ID: JSON.parse(this.obj).User_ID,
    })
  }

  detectValueChange(index: number) {
    let controls = this.companyDetailsInvoice.get('companyinvoiceDetails') as FormArray
    this.changeTotal(controls.controls[index])
  }

  changeTotal(control) {
    let price = parseFloat(control.get('Item_Price'));
    let quantity = parseInt(control.get('Item_Quantity'));
    if (!isNaN(price) && !isNaN(quantity)) {
      let totalCost = (price * quantity).toFixed(2);
      control.get('Item_Amount').patchValue(totalCost, { emitEvent: false })
      console.log(control.get('Item_Amount'))
      // control['Item_Amount'].setValue(totalCost, { emitEvent: false })
    }
  }

  generateInvoiceNumber(type: string) {
    return this.invoiceService.generateInvoiceNumber(type).pipe(
      tap(response => {
        if (response.success) {
          if (this.invoiceType === 'Company') {
            this.companyInvoice.get('Invoice_Number').patchValue(response.data.New_Invoice_Number)
          }
          if (this.invoiceType === 'Clinic') {
            this.clinicInvoice.get('Invoice_Number').patchValue(response.data.New_Invoice_Number)
          }
        } else {
          this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 });
        }
      })
    )
  }

  calculateSubTotal() {
    var control = this.companyDetailsInvoice.get('companyinvoiceDetails') as FormArray
    console.log(control.controls[0].get('Item_Quantity').value)


  }

  saveInvoice() {
    // console.log(this.companyDetailsInvoice.get('companyinvoiceDetails').value)
    if (this.invoiceType === 'Company') {
      this.invoiceService.saveCompanyInvoice(this.companyInvoice.getRawValue()).subscribe(response => {
        if (response.success) {
          var data = response.data.TPA_Company_Invoice_ID;
          for (let i = 0; i < this.invoiceDetails.length; i++) {
            this.companyDetailsInvoice.get('companyinvoiceDetails').get('TPA_Company_Invoice_ID').patchValue(data);
            this.invoiceService.saveCompanyInvoiceDetails(JSON.stringify(this.companyDetailsInvoice.get('companyinvoiceDetails').value)).subscribe(response => {
              if (response.success) {
                console.log(response.data)
              } else {
                console.error('Details', response);
              }
            })
          }
        } else {
          console.error('CompInvoice', response);
        }
      })
    }
    if (this.invoiceType === 'Clinic') {

    }
  }

}
