import { ClinicInvoiceComponent } from './clinic-invoice/clinic-invoice.component';
import { CompanyInvoiceComponent } from './company-invoice/company-invoice.component';
import { NewInvoiceComponent } from './new-invoice/new-invoice.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {
    path: 'newInvoice',
    component: NewInvoiceComponent
  },
  {
    path: 'companyInvoice',
    component: CompanyInvoiceComponent
  },
  {
    path: 'clinicInvoice',
    component: ClinicInvoiceComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InvoiceRoutingModule { }
