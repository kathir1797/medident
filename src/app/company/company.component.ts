import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { getCompanyRequest } from 'app/models/getCompanyRequest';
import { MatSnackBar } from '@angular/material/snack-bar';
import { CompanyService } from 'app/services/dataManager/company.service';

@Component({
  selector: 'app-company',
  templateUrl: './company.component.html',
  styleUrls: ['./company.component.css']
})
export class CompanyComponent implements AfterViewInit, OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  dataSource = new MatTableDataSource<any>();
  length: number
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['Registration_No', 'Company_Name', 'Registered_Date', 'Company_Code','Contact_Person_1', 'Action'];

  companyData: getCompanyRequest = new getCompanyRequest();

  constructor(private company: CompanyService, private snackBar: MatSnackBar) { }

  
  ngOnInit() {
    this.companyData.Company_ID = 0;
    // this.dataSource = new TableDataSource();
    this.getCompany();
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  getCompany() {
    this.company.getCompany(this.companyData).subscribe(response => {
      if (response.success) {
        this.dataSource.data = response.data;
        this.dataSource.sort = this.sort;
        this.length = response.data.length;
      } else {
        this.snackBar.open(response.errorMessage, "Ok", { duration: 3000 });
      }
    });
  }

  deleteCompany(Company_ID) {
    this.company.deleteCompany(Company_ID).subscribe(response => {
      if (response.success) {
        this.snackBar.open("Record deleted", "Ok");
        this.getCompany();
      } else {
        this.snackBar.open(response.errorMessage, "close");
      }
    });
  }

}
