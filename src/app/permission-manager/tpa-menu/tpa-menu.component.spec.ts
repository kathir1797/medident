import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TpaMenuComponent } from './tpa-menu.component';

describe('TpaMenuComponent', () => {
  let component: TpaMenuComponent;
  let fixture: ComponentFixture<TpaMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TpaMenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TpaMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
