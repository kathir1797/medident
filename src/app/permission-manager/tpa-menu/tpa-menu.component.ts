import { MatTableDataSource } from '@angular/material/table';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { NavigationItem, NavItem } from 'app/components/menu';
import { MenuService } from 'app/components/menu.service';
import { DdlistService } from 'app/services/config/ddlist.service';

@Component({
  selector: 'app-tpa-menu',
  templateUrl: './tpa-menu.component.html',
  styleUrls: ['./tpa-menu.component.css']
})
export class TpaMenuComponent implements OnInit {

  menuForm: FormGroup;
  obj = localStorage.getItem('currentuser');
  statusList = new Array();
  dataSource = new MatTableDataSource<any>();

  displayedColumns: string[] = ['displayName', 'children'];
  displayedColumns2 = ['DisplayName1', 'Children1']
  displayedColumns3 = ['children']
  columnsToDisplay: string[] = this.displayedColumns.slice();

  constructor(private fb: FormBuilder, private nav: NavigationItem, private ddlist: DdlistService, private snackBar: MatSnackBar, private menuService: MenuService) {
    this.initializeForm();
    this.dataSource.data = [{
      displayName: 'Data Manager',
      iconName: 'perm_identity',
      children: [
        {
          displayName: 'TPA Setup',
          iconName: 'code',
          children: [
            {
              displayName: 'Branch',
              route: '/dataManager/branch',
            },
            {
              displayName: 'Products',
              route: '/dataManager/product',
            },
            {
              displayName: 'Master TPA ID Setup',
              route: '/dataManager/master-tpasetup',
            },
            {
              displayName: 'Master TPA Company Setup',
              route: '/dataManager/master-tpa',
            },
            {
              displayName: 'TPA',
              route: '/dataManager/tpamember',
            },
          ]
        },
        {
          displayName: 'Dental Services',
          iconName: 'healing',
          children: [
            {
              displayName: 'All Clinics',
              iconName: 'person',
              children: [
                {
                  displayName: 'Panel Clinics',
                  route: '/dataManager/clinic',
                },
                {
                  displayName: 'Non-Panel Clinics',
                  route: '/dataManager/clinic',
                },
              ]
            },

            {
              displayName: 'Doctors',
              iconName: 'person',
              children: [
                {
                  displayName: 'Doctors Master',
                  route: '/dataManager/doctor'
                },
                {
                  displayName: 'Doctors-Clinic',
                  route: '/dataManager/doctor/clinics'
                }
              ]
            },
            {
              displayName: 'Specialist Equipment',
              route: 'dataManager/specialistEquipment'
            },
            {
              displayName: 'Tooth Surface Master',
              route: '/dataManager/toothSurfaceCode'
            },
            {
              displayName: 'Tooth Code Master',
              route: '/treatmentManager/toothCode',
            },
          ]
        },
        {
          displayName: 'Membership',
          iconName: 'business',
          children: [
            {
              displayName: 'Company',
              route: '/company',
            },
            {
              displayName: 'Principal Master',
              route: '/employee',
            },
            {
              displayName: 'Dependants Master',
              route: '/dataManager/dependent',
            },
            {
              displayName: 'Individual Policies',
            }
          ]
        },
        {
          displayName: 'Users',
          iconName: 'group',
          children: [
            {
              displayName: 'TPA User',
              route: '/tpa-users'
            },
            {
              displayName: 'TPA User Type',
              route: '/tpa-users/userTypes'
            },
            {
              displayName: 'Broker',
            },
            {
              displayName: 'Clinic Admin',
            },
            {
              displayName: 'TPC Admin',
            },
          ]
        },
        {
          displayName: 'Static',
          iconName: 'business',
          children: [
            {
              displayName: 'Service Type Master',
              route: '/dataManager/servicetype'
            },
            {
              displayName: 'Clinic Type Master',
              route: '/dataManager/clinicType'
            },
            {
              displayName: 'States Master',
            },
            {
              displayName: 'City Master',
              route: '/city'
            },
            {
              displayName: 'Nationality Master',
              route: '/nationality'
            },
            {
              displayName: 'Zone Master',
            },
            {
              displayName: 'Location',
            },
          ]
        },
        {
          displayName: 'Insurer',
          iconName: 'group',
          children: [
            {
              displayName: 'Insurance Company',
              route: '/dataManager/insuranceCompany'
            }
          ]
        },
        {
          displayName: 'Treatment Plans',
          iconName: 'next_plan',
          children: [
            {
              displayName: 'Dental Plan Master(UnLimited)',
            },
            {
              displayName: 'Dental Plan Master(Limited)',
              route: '/dataManager/treatmentPlan'
            },
            {
              displayName: 'Company Treatment Plan',
              route: '/dataManager/treatmentPlanCompany'
            },
            // {
            //   displayName: 'Treatment Plan Category',
            //   route: '/dataManager/treatmentPlanCategory'
            // }
          ]
        },
        {
          displayName: 'Optical Services',
          iconName: 'panorama_fish_eye',
          children: [
            {
              displayName: 'Optician Master',
            },
            {
              displayName: 'Optical Outlets Master',
            },
          ]
        },

      ]
    }]
  }

  ngOnInit(): void {

  }

  initializeForm() {
    this.menuForm = this.fb.group({
      TPA_Menu_ID: 0,
      Menu_Name: new FormControl(),
      TPA_Menu_Status_ID: new FormControl(1),
      User_ID: JSON.parse(this.obj).User_ID
    })
  }

  getStatusList() {
    this.ddlist.getStatusDDList().subscribe(response => {
      if (response.success) {
        this.statusList = response.data;
      } else {
        this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 })
      }
    });
  }

  addMenu() {
    this.menuService.addMenu(JSON.stringify(this.menuForm.value)).subscribe(response => {
      if (response.success) {
        this.menuForm.reset();
        this.snackBar.open('Menu Added Successfully', 'ok', { duration: 3000 })
      } else {
        this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 })
      }
    })
  }

  getdata(data) {
    return new MatTableDataSource<any>(data);
  }

}
