import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MenuService } from './../../components/menu.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { DdlistService } from './../../services/config/ddlist.service';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { Component, Inject, OnInit } from '@angular/core';

@Component({
  templateUrl: './add-menu.component.html',
  styleUrls: ['./add-menu.component.css']
})
export class AddMenuComponent implements OnInit {

  menuForm: FormGroup;
  obj = localStorage.getItem('currentuser');
  statusList = new Array();

  constructor(private fb: FormBuilder, private ddlist: DdlistService, private snackBar: MatSnackBar, private menuService: MenuService,
    @Inject(MAT_DIALOG_DATA) private data: any) {
    this.initializeForm();
    if (this.data != null) {
      this.menuForm.patchValue({
        TPA_Menu_ID: this.data.TPA_Menu_ID,
        Menu_Name: this.data.Menu_Name,
        TPA_Menu_Status_ID: this.data.TPA_Menu_Status_ID,
        User_ID: JSON.parse(this.obj).User_ID
      })
    }
  }

  ngOnInit(): void {
    this.getStatusList();
    console.log(this.menuForm.getRawValue())
  }

  initializeForm() {
    this.menuForm = this.fb.group({
      TPA_Menu_ID: 0,
      Menu_Name: new FormControl(),
      TPA_Menu_Status_ID: new FormControl(1),
      User_ID: JSON.parse(this.obj).User_ID
    })
  }

  getStatusList() {
    this.ddlist.getStatusDDList().subscribe(response => {
      if (response.success) {
        this.statusList = response.data;
      } else {
        this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 })
      }
    });
  }

  addMenu() {
    this.menuService.addMenu(JSON.stringify(this.menuForm.value)).subscribe(response => {
      if (response.success) {
        this.menuForm.reset();
        this.snackBar.open('Menu Added Successfully', 'ok', { duration: 3000 })
      } else {
        this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 })
      }
    })
  }

}
