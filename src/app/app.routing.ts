import { ClaimsComponent } from './claims/claims.component';
import { NgModule } from '@angular/core';
import { CommonModule, } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';

import { AdminLayoutComponent } from './layouts/admin-layout/admin-layout.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full',
  },
  {
    path: '',
    component: AdminLayoutComponent,
    children:
      [
        {
          path: '',
          loadChildren: './layouts/admin-layout/admin-layout.module#AdminLayoutModule'
        }
      ]
  },
  {
    path: 'login',
    loadChildren: './login/login.module#LoginModule'
  },
  {
    path: 'claims',
    component: AdminLayoutComponent,
    children:
      [
        {
          path: '',
          loadChildren: () => import('./claims/claims.module').then(m => m.ClaimsModule)
        }
      ]
  },
  {
    path: 'tpa-users',
    component: AdminLayoutComponent,
    children:
      [
        {
          path: '',
          loadChildren: () => import('./users/tpa-user/tpa-user.module').then(m => m.TpaUserModule)
        }
      ]
  },
  {
    path: 'benefitOption',
    component: AdminLayoutComponent,
    children:
      [
        {
          path: '',
          loadChildren: () => import('./benefitOption/benefit-option/benefit-option.module').then(m => m.BenefitOptionModule)
        }
      ]
  },
  {
    path: 'invoice',
    component: AdminLayoutComponent,
    children:
      [
        {
          path: '',
          loadChildren: () => import('./invoice/invoice.module').then(m => m.InvoiceModule)
        }
      ]
  },
  {
    path: 'city',
    component: AdminLayoutComponent,
    children:
      [
        {
          path: '',
          loadChildren: () => import('./cities/cities.module').then(m => m.CitiesModule)
        }
      ]
  },
  {
    path: 'nationality',
    component: AdminLayoutComponent,
    children:
      [
        {
          path: '',
          loadChildren: () => import('./nationality-master/nationality-master.module').then(m => m.NationalityMasterModule)
        }
      ]
  },
  {
    path: 'permission',
    component: AdminLayoutComponent,
    children:
      [
        {
          path: '',
          loadChildren: () => import('./permission-manager/permission-manager.module').then(m => m.PermissionManagerModule)
        }
      ]
  },
  {
    path: 'support',
    component: AdminLayoutComponent,
    children:
      [
        {
          path: '',
          loadChildren: () => import('./support/support.module').then(m => m.SupportModule)
        }
      ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    RouterModule.forRoot(routes, {
      useHash: true
    })
  ],
  exports: [
  ],
})
export class AppRoutingModule { }
