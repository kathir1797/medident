import { toothCodeSurface } from './../../models/dataManager';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router, ActivatedRoute } from '@angular/router';
import { ToothCodeService } from 'app/services/treatmentManager/tooth-code.service';
import { DdlistService } from 'app/services/config/ddlist.service';

@Component({
  selector: 'app-add-tooth-surface-master',
  templateUrl: './add-tooth-surface-master.component.html',
  styleUrls: ['./add-tooth-surface-master.component.css']
})
export class AddToothSurfaceMasterComponent implements OnInit {

  toothSurfaceCodeForm: FormGroup;
  urlText: any;
  obj = localStorage.getItem('currentuser');
  statusList = new Array();
  toptitle: string = 'Add Tooth Surface Code';
  btntext: string = 'Save';
  isDisabled: boolean = true;

  toothCodeSurReq: toothCodeSurface = new toothCodeSurface();

  constructor(private fb: FormBuilder, private router: Router, private ddlist: DdlistService,
    private snackBar: MatSnackBar, private toothCode: ToothCodeService, private activated: ActivatedRoute) {
    const url = this.router.url;
    this.urlText = url.split('/');
    this.initializeForm();
    this.activated.params.subscribe(params => {
      if (this.urlText[3] != 'add') {
        this.toptitle = 'Update Tooth Surface Code';
        this.btntext = 'Update';
        this.toothCodeSurReq.Tooth_Surface_ID = params.Tooth_Surface_ID
        this.getToothCode(this.toothCodeSurReq)
      }
      if (this.urlText[3] === 'view') {
        this.isDisabled = false;
        this.toptitle = 'View Tooth Surface Code Detail';
      }
    })
  }

  ngOnInit(): void {
    this.getStatusDDLists();
  }

  initializeForm() {
    this.toothSurfaceCodeForm = this.fb.group({
      Tooth_Surface_ID: 0,
      Tooth_Surface_Code: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      Remarks: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      Status_ID: new FormControl({ value: 1, disabled: this.urlText[3] == 'view' ? true : false }),
      User_ID: JSON.parse(this.obj).User_ID
    })
  }

  getStatusDDLists() {
    this.ddlist.getStatusDDList().subscribe(response => {
      if (response.success) {
        this.statusList = response.data;
      } else {
        this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 })
      }
    });
  }

  addToothSurfaceCode() {
    this.toothCode.addToothSurfaceCode(this.toothSurfaceCodeForm.getRawValue()).subscribe(response => {
      if (response.success) {
        this.router.navigate(['/dataManager/toothSurfaceCode']);
        this.snackBar.open('Tooth Surface Code Saved Successfully', 'OK', { duration: 3000 });
        this.toothSurfaceCodeForm.reset();
      } else {
        this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 })
      }
    });
  }

  getToothCode(toothCodeSurReq) {
    this.toothCode.getToothSurfaceCode(toothCodeSurReq).subscribe(response => {
      if (response.success) {
        this.toothSurfaceCodeForm.patchValue({
          Tooth_Surface_ID: response.data[0].Tooth_Surface_ID,
          Tooth_Surface_Code: response.data[0].Tooth_Surface_Code,
          Remarks: response.data[0].Remarks,
          Status_ID: response.data[0].Tooth_Surface_Status_ID,
          User_ID: JSON.parse(this.obj).User_ID
        })
      } else {
        this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 })
      }
    })
  }

}
