import { toothCodeSurface } from './../models/dataManager';
import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { toothCodeRequest } from 'app/models/treatmentManager';
import { ToothCodeService } from 'app/services/treatmentManager/tooth-code.service';

@Component({
  selector: 'app-tooth-surface-master',
  templateUrl: './tooth-surface-master.component.html',
  styleUrls: ['./tooth-surface-master.component.css']
})
export class ToothSurfaceMasterComponent implements OnInit, AfterViewInit {

  @ViewChild(MatPaginator) paginator: MatPaginator;
  dataSource = new MatTableDataSource<any>();
  length: number
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  displayedColumns = ['Tooth_Surface_Code', 'Remarks', 'Status_Name', 'Action'];

  toothCodeSurReq: toothCodeSurface = new toothCodeSurface();

  constructor(private router: Router, private snackBar: MatSnackBar, private toothCode: ToothCodeService) { }

  ngAfterViewInit(): void {
    this.dataSource.paginator = this.paginator
    this.dataSource.sort = this.sort
  }

  ngOnInit(): void {
    this.toothCodeSurReq.Tooth_Surface_ID = 0;
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    this.getToothCode();
  }

  getToothCode() {
    this.toothCode.getToothSurfaceCode(this.toothCodeSurReq).subscribe(response => {
      if (response.success) {
        this.dataSource.data = response.data;
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.length = this.dataSource.data.length;
      } else {
        this.dataSource.data = [];
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 })
      }
    })
  }

}
