import { tap } from 'rxjs/operators';
import { employeeRequest } from './../../models/employeeRequest';
import { EmployeeService } from './../../services/dataManager/employee.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { SubmissionService } from './../../services/claims/submission.service';
import { ConfigService } from './../../services/config/config.service';
import { Component, OnInit } from '@angular/core';
import { CheckPatientEligibility } from 'app/models/patientEligibility';

@Component({
  selector: 'app-patient-eligibility',
  templateUrl: './patient-eligibility.component.html',
  styleUrls: ['./patient-eligibility.component.css']
})
export class PatientEligibilityComponent implements OnInit {

  patEligibility = this.configService.configJson.modules.ClaimManager.PatientEligibility;
  eligibility = this.patEligibility.Eligibility;
  patientEligibility = new CheckPatientEligibility();
  employeeRequest: employeeRequest = new employeeRequest();

  patientList = new Array();

  isBtnClicked: boolean = false;
  isEligible: boolean = false;

  loginDetailsObj = JSON.parse(localStorage.getItem('currentUserDetails'));

  constructor(private configService: ConfigService, private submission: SubmissionService, private employee: EmployeeService,
    private snackBar: MatSnackBar) { }

  ngOnInit(): void {
  }

  getPolicyDetails() {
    this.submission.getPatientDetails(this.patientEligibility.Policy_Number).subscribe(response => {
      if (response.success) {
        this.patientList = response.data;
        console.log(this.patientList)
        this.getPrincipalByID(this.patientList[0].Principal_ID).subscribe(principal => { });
      } else {
        this.snackBar.open(response.errorMessage, 'OK', { duration: 3000 })
      }
    })
  }

  getPrincipalByID(principalID) {
    this.employeeRequest.Employee_ID = principalID
    return this.employee.getEmployeeList(this.employeeRequest).pipe(
      tap(response => {
        if (response.success) {
          this.patientEligibility.Principal_Name = response.data[0].Employee_Name;
          this.patientEligibility.Patient_Name = response.data[0].Employee_Name;
          this.patientEligibility.IC = response.data[0].Employee_NRIC_No;
          this.patientEligibility.Phone_Number = response.data[0].Telephone_No;
          this.isBtnClicked = true
        } else {
          this.snackBar.open(response.errorMessage, 'OK', { duration: 3000 });
        }
      })
    );
  }

  /**Patient Eligibility Check */
  patientEligibleCheck(patID) {
    const data = {
      Patient_ID: patID,
      Dependent_ID: this.patientEligibility.Dependent_ID,
      Clinic_ID: this.loginDetailsObj.Clinic_ID
    };
    this.submission.patientEligibleCheck(data).subscribe(response => {
      if (response.success && response.data == 'Eligible') {
        this.isEligible = true
      } else {
        this.snackBar.open(response.errorMessage, 'OK', { duration: 3000 });
      }
    })
  }

}
