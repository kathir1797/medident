import { PatientEligibilityComponent } from './patient-eligibility/patient-eligibility.component';
import { ClaimsIndexComponent } from './claims-index/claims-index.component';
import { TpaClaimsListComponent } from './claims-submission/tpa-claims-list/tpa-claims-list.component';
import { ClaimsSubmissionComponent } from './claims-submission/claims-submission.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {
    path: 'submission/clinic',
    component: TpaClaimsListComponent
  },
  {
    path: 'submission/index',
    component: ClaimsIndexComponent
  },
  {
    path: 'submission/clinicindex',
    component: ClaimsIndexComponent
  },
  {
    path: 'submission/internal',
    component: TpaClaimsListComponent
  },
  {
    path: 'submission/patientEligibility',
    component: PatientEligibilityComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClaimsRoutingModule { }
