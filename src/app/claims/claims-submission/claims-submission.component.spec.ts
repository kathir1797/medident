import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClaimsSubmissionComponent } from './claims-submission.component';

describe('ClaimsSubmissionComponent', () => {
  let component: ClaimsSubmissionComponent;
  let fixture: ComponentFixture<ClaimsSubmissionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClaimsSubmissionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClaimsSubmissionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
