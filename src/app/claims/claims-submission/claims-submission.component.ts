import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { SubmissionService } from './../../services/claims/submission.service';
import { toothCodeSurface } from './../../models/dataManager';
import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { TreatmentGroupService } from 'app/services/treatmentManager/treatment-group.service';
import { toothCodeRequest, treatmentCodeRequest, treatmentGroup } from 'app/models/treatmentManager';
import { TreatmentCodeService } from 'app/services/treatmentManager/treatment-code.service';
import { ToothCodeService } from 'app/services/treatmentManager/tooth-code.service';
import { DdlistService } from 'app/services/config/ddlist.service';

@Component({
  selector: 'app-claims-submission',
  templateUrl: './claims-submission.component.html',
  styleUrls: ['./claims-submission.component.css']
})
export class ClaimsSubmissionComponent implements OnInit {


  tpaClaims = new Array();



  constructor(private snackBar: MatSnackBar, private submission: SubmissionService,) {

  }

  ngOnInit(): void {

  }



}
