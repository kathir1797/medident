import { MatTable, MatTableDataSource } from '@angular/material/table';
import { DatePipe } from '@angular/common';
import { ClaimsSubmissionComponent } from './../claims-submission.component';
import { MatDialog } from '@angular/material/dialog';
import { tap } from 'rxjs/operators';
import { Router } from '@angular/router';
import { SubmissionService } from './../../../services/claims/submission.service';
import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { clinicsRequest, toothCodeSurface } from 'app/models/dataManager';
import { treatmentGroup, treatmentCodeRequest, toothCodeRequest, treatmentMasterSetup } from 'app/models/treatmentManager';
import { DdlistService } from 'app/services/config/ddlist.service';
import { ToothCodeService } from 'app/services/treatmentManager/tooth-code.service';
import { TreatmentCodeService } from 'app/services/treatmentManager/treatment-code.service';
import { TreatmentGroupService } from 'app/services/treatmentManager/treatment-group.service';
import { ClinicsService } from 'app/services/dataManager/clinics.service';
import { MatHorizontalStepper } from '@angular/material/stepper';
import { MatSelectChange } from '@angular/material/select';
import { TreatmentMasterService } from 'app/services/treatmentManager/treatment-master.service';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { ChangeDetectorRef } from '@angular/core';

@Component({
  selector: 'app-tpa-claims-list',
  templateUrl: './tpa-claims-list.component.html',
  styleUrls: ['./tpa-claims-list.component.css']
})
export class TpaClaimsListComponent implements OnInit, AfterViewInit {

  @ViewChild(MatHorizontalStepper) stepper: MatHorizontalStepper;

  doctorsList = new Array();
  patientList = new Array();
  treatmentGrpList = new Array();
  treatmentCodeList = new Array();
  toothSurfaceCodeList = new Array();
  toothSurfaceSelectionList = new Array();
  toothCodeList = new Array();
  toothCodeSelectionList = new Array();
  statusList = new Array();
  tpaClaims = new Array();
  dataSource = new MatTableDataSource<any>();
  clinicsList = new Array();
  treatmentMasterSetupList = new Array();
  toothCodeByDentition = new Array();
  primaryDentition = new Array();
  adultDentition = new Array();
  treatmentToothMaster = new Array();

  loginDetailsObj = JSON.parse(localStorage.getItem('currentUserDetails'));
  policy_number: any; doctor_id: any
  treatmentDivision: boolean = false;

  isCompletedStepOne: boolean = false

  tpaClaimsForm: FormGroup;
  toothCodeClaimForm: FormGroup;
  toothSurfaceCodeForm: FormGroup;
  tpaClaimDetailsForm: FormGroup;
  policy_id = new FormControl('', Validators.required)

  clinicPerms: boolean = true;
  routerURL: any;

  treatmentgroupReq: treatmentGroup = new treatmentGroup();
  treatmentCodeReq: treatmentCodeRequest = new treatmentCodeRequest();
  toothCodeSurReq: toothCodeSurface = new toothCodeSurface();
  toothCodeReq: toothCodeRequest = new toothCodeRequest();
  clinicsData: clinicsRequest = new clinicsRequest();
  treatmentMasterSetup: treatmentMasterSetup = new treatmentMasterSetup();


  @ViewChild(MatPaginator) paginator: MatPaginator;
  length: number
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatTable) table: MatTable<any>;
  displayedColumns: string[] = [
    'Treatment_Group_Code',
    'Treatment_Group_Description',
    'Treatment_Code',
    'Treatment_Rate',
    'Start_Date',
    'End_Date',]

  constructor(private snackBar: MatSnackBar, private fb: FormBuilder, private submission: SubmissionService,
    private treatmentGrp: TreatmentGroupService, private treatmentCode: TreatmentCodeService, private router: Router,
    private toothCode: ToothCodeService, private ddlist: DdlistService, private clinics: ClinicsService,
    private tmaster: TreatmentMasterService, private dialog: MatDialog, private changeDetectorRefs: ChangeDetectorRef
  ) {
    this.initializeTPAClaimsForm();
    this.initializeSurfaceCodeForm();
    this.initializeToothCodeClaims();
    this.initializeTPAClaimDetailsForm();
    const url = this.router.url;
    this.routerURL = url.split('/');
    // console.log(this.routerURL)
    if (this.routerURL[3] === 'clinic') {
      this.clinicPerms = false;
      // this.getDoctorByClinicID();
    }
  }

  ngOnInit(): void {
    this.getDoctorByClinicID();
    this.treatmentgroupReq.Treatment_Group_ID = 0;
    this.getTreatmentGroup();
    // this.treatmentCodeReq.Treatment_Code_ID = 0;
    // this.getTreatmentCode();
    this.toothCodeSurReq.Tooth_Surface_ID = 0;
    this.getToothSurfaceCode();
    this.getStatusDDLists();
    this.toothCodeReq.Tooth_Code_ID = 0;
    // this.getTPAClaims();
    this.clinicsData.Clinic_ID = 0;
    this.getClinics();
    this.treatmentMasterSetup.Treatment_Master_ID = 0;
    // this.getTPAClaimDetails(0).subscribe(response => { })
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  onClinicSelect(event) {
    this.submission.getDoctorsByClinicID(event.value).subscribe(response => {
      if (response.success) {
        this.doctorsList = response.data;
      } else {
        this.snackBar.open(response.errorMessage, 'OK', { duration: 3000 })
      }
    })
  }

  //Step - 1: For Selecting Doctors 
  getDoctorByClinicID() {
    this.submission.getDoctorsByClinicID(this.loginDetailsObj.Clinic_ID).subscribe(response => {
      if (response.success) {
        this.doctorsList = response.data;
      } else {
        this.snackBar.open(response.errorMessage, 'OK', { duration: 3000 })
      }
    })
  }

  //Step -2 On update policy click 901122-08-9193 - Policy number
  getPatientDetails() {
    if (this.claimFormControls.Clinic_ID.valid && this.claimFormControls.Doctor_ID.valid && this.policy_id.valid) {
      this.submission.getPatientDetails(this.policy_id.value).subscribe(response => {
        if (response.success) {
          this.patientList = response.data;
          this.stepper.next()
          // console.log(this.patientList)
          this.isCompletedStepOne = this.patientList.length != 0 && this.patientList != null ? true : false
        } else {
          this.snackBar.open(response.errorMessage, 'OK', { duration: 3000 })
        }
      })
    } else {
      this.validateAllFormFields(this.tpaClaimsForm);
      this.policy_id.setValidators(Validators.required)
    }

  }

  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach((field) => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({
          onlySelf: true
        });
      } else if (control instanceof FormGroup) { this.validateAllFormFields(control); }
    });
  }

  getTreatmentGroup() {
    this.treatmentGrp.getTreatmentGroup(this.treatmentgroupReq).subscribe(response => {
      if (response.success) {
        this.treatmentGrpList = response.data
      } else {
        this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 })
      }
    });
  }

  getTreatmentCode() {
    this.treatmentCode.getTreatmentCode(this.treatmentCodeReq).subscribe(response => {
      if (response.success) {
        this.treatmentCodeList = response.data;
      } else {
        this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 })
      }
    })
  }

  getToothSurfaceCode() {
    this.toothCode.getToothSurfaceCode(this.toothCodeSurReq).subscribe(response => {
      if (response.success) {
        this.toothSurfaceCodeList = response.data;
      } else {
        this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 })
      }
    })
  }

  getClinics() {
    this.clinics.getClinicsList(this.clinicsData).subscribe(response => {
      if (response.success) {
        this.clinicsList = response.data;
      } else {
        this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 })
      }
    });
  }

  initializeTPAClaimsForm() {
    this.tpaClaimsForm = this.fb.group({
      TPA_Claim_ID: 0,
      Claim_Date: new FormControl(new Date()),
      Clinic_ID: new FormControl('', !this.clinicPerms ? Validators.required : null),
      Doctor_ID: new FormControl('', Validators.required),
      Patient_ID: new FormControl(''),
      Dependent_ID: new FormControl(),
      Company_ID: new FormControl(),

      TPA_Claim_Status_ID: new FormControl(1),
      User_ID: this.loginDetailsObj.Login_User_ID
    })
  }

  initializeTPAClaimDetailsForm() {
    this.tpaClaimDetailsForm = this.fb.group({
      TPA_Claim_Details_ID: 0,
      TPA_Claim_ID: new FormControl(),
      Treatment_Group_ID: new FormControl(),
      Treatment_Code_ID: new FormControl(),
      Tooth_Code_ID: new FormControl(),
      Tooth_Surface_ID: new FormControl(),
      TPA_Claim_Details_Status_ID: 1,
      Comments: new FormControl(),
      User_ID: this.loginDetailsObj.Login_User_ID
    })
  }

  get claimFormControls() {
    return this.tpaClaimsForm.controls;
  }

  initializeToothCodeClaims() {
    this.toothCodeClaimForm = this.fb.group({
      TPA_Claim_Tooth_Code_ID: 0,
      TPA_Claim_ID: new FormControl(),
      Tooth_Code_ID: new FormControl(),
      TPA_Claim_Tooth_Code_Status_ID: 1,
      User_ID: this.loginDetailsObj.Login_User_ID
    })
  }

  initializeSurfaceCodeForm() {
    this.toothSurfaceCodeForm = this.fb.group({
      TPA_Claim_Surface_ID: 0,
      TPA_Claim_ID: new FormControl(),
      Tooth_Surface_ID: new FormControl(),
      TPA_Claim_Surface_Status_ID: 1,
      User_ID: this.loginDetailsObj.Login_User_ID
    })
  }

  getStatusDDLists() {
    this.ddlist.getStatusDDList().subscribe(response => {
      if (response.success) {
        this.statusList = response.data;
      } else {
        this.snackBar.open(response.errorMessage, 'OK', { duration: 3000 }); (response.errorMessage, "Ok");
      }
    });
  }

  onSelectChange(event) {
    // console.log(event)
    this.patientList.filter(value => {
      if (value.Company_ID == event) {
        this.tpaClaimsForm.get('Company_ID').patchValue(value.Company_ID)
        this.tpaClaimsForm.get('Patient_ID').patchValue(value.Principal_ID)
        this.tpaClaimsForm.get('Dependent_ID').patchValue(value.Principal_ID > 0 ? value.Dependent_ID : 0)
      }
    });

    var obj = {
      Patient_ID: this.tpaClaimsForm.get('Patient_ID').value,
      Dependent_ID: this.tpaClaimsForm.get('Dependent_ID').value,
      Claim_Date: null,
      Clinic_ID: this.routerURL[3] == 'clinic' ? this.tpaClaimsForm.get('Clinic_ID').patchValue(this.loginDetailsObj.Clinic_ID) : this.tpaClaimsForm.get('Clinic_ID').value
    }
    this.onPatientSelect().subscribe(response => {
      if (response.success) {
        this.tpaClaims = response.data;
        this.tpaClaims.forEach(x => {
          obj.Claim_Date = x.Claim_Date.split('T')[0];
          this.submission.checkTPAClaimPatient(obj).subscribe(check => {
            if (check.success) {
              if (check.data.TPA_Claim_Status_ID == 1 && check.data.Patient_Check_Result == 1) {
                // this.snackBar.open('Patient has a active claim', 'OK', { duration: 4000 });
                // this.goBack();
                let dialogref = this.dialog.open(ClaimsSubmissionComponent)

                dialogref.afterClosed().subscribe(result => {
                  if (!result) {
                    this.goBack();
                  }
                })
              }
            } else {
              this.snackBar.open(check.errorMessage, 'Patient Check', { duration: 3000 });
            }
          });
        });
      } else {
        this.snackBar.open(response.errorMessage, 'Claim Submission', { duration: 3000 });
      }
    })

    // console.log(this.tpaClaimsForm.get('Patient_ID').value, this.tpaClaimsForm.get('Dependent_ID').value, this.tpaClaimsForm.get('Company_ID').value)
  }

  onToothCodeChanges(event, toothCodeObj) {
    if (event) {
      this.toothCodeSelectionList.push(toothCodeObj)
    } else {
      this.toothCodeSelectionList = this.toothCodeSelectionList.filter(v => v != toothCodeObj);
    }
  }

  onToothSurfaceSelect(event, id) {
    if (event.checked) {
      this.toothSurfaceSelectionList.push(id)
    } else {
      this.toothSurfaceSelectionList = this.toothSurfaceSelectionList.filter((value) => value != id)
    }
  }

  onTreatmentCodeSelect(event: MatSelectChange, based: boolean) {
    if (based) {
      this.submission.getToothCodeByTreatmentCodeID(event).subscribe(response => {
        if (response.success) {
          this.toothCodeList = response.data;
          // console.log(this.toothCodeList, this.treatmentMasterSetupList);
          for (var i = 0; i < this.treatmentMasterSetupList.length; i++) {
            for (var j = 0; j < this.toothCodeList.length; j++) {
              if (j <= i) {
                if (this.treatmentMasterSetupList[i].Min_Age < 18 && this.treatmentMasterSetupList[i].Treatment_Code == this.toothCodeList[j].Treatment_Code) {
                  // console.log(this.toothCodeList[j])
                  if (!this.primaryDentition.some((value) => { return value.Tooth_Code_ID == this.toothCodeList[j].Tooth_Code_ID })) {
                    this.primaryDentition.push(this.toothCodeList[j])
                  }
                }
                if (this.treatmentMasterSetupList[i].Min_Age > 18 && this.treatmentMasterSetupList[i].Treatment_Code == this.toothCodeList[j].Treatment_Code) {
                  // console.log(this.toothCodeList[j])
                  if (!this.adultDentition.some((value) => { return value.Tooth_Code_ID == this.toothCodeList[j].Tooth_Code_ID })) {
                    this.adultDentition.push(this.toothCodeList[j])
                  }
                }
              }

            }
          }
        } else {
          this.snackBar.open(response.errorMessage, 'Tooth Code Claims', { duration: 3000 });
        }
      });
    }

  }

  saveTPAClaims() {
    console.log(this.toothSurfaceSelectionList.length, this.toothCodeSelectionList.length)
    for (var i = 0, j = 0; i < this.toothSurfaceSelectionList.length, j < this.toothCodeSelectionList.length; i++, j++) {
      this.tpaClaimDetailsForm.get('Tooth_Surface_ID').patchValue(this.toothSurfaceSelectionList[i].Tooth_Surface_ID)
      this.tpaClaimDetailsForm.get('Tooth_Code_ID').patchValue(this.toothCodeSelectionList[j].Tooth_Code_ID);
      this.toothCodeClaimForm.get('Tooth_Code_ID').patchValue(this.toothCodeSelectionList[j].Tooth_Code_ID);
      this.toothSurfaceCodeForm.get('Tooth_Surface_ID').patchValue(this.toothSurfaceSelectionList[i].Tooth_Surface_ID)
      this.submission.saveTPAClaims(JSON.stringify(this.tpaClaimsForm.value)).subscribe(response => {
        if (response.success) {
          var data = response.data.TPA_Claim_ID;// Last Inserted ID
          this.toothCodeClaimForm.patchValue({ TPA_Claim_ID: data }); //tooth code form
          this.toothSurfaceCodeForm.patchValue({ TPA_Claim_ID: data });// surface code form
          this.tpaClaimDetailsForm.patchValue({ TPA_Claim_ID: data }); //Claim Details Form
          this.submission.saveToothCodeTPAClaims(JSON.stringify(this.toothCodeClaimForm.value)).subscribe(toothClaims => {
            if (toothClaims.success) {
              this.submission.saveToothSurfaceCodeClaims(JSON.stringify(this.toothSurfaceCodeForm.value)).subscribe(surfaceClaims => {
                if (surfaceClaims.success) {
                  this.submission.saveTPAClaimDetails(JSON.stringify(this.tpaClaimDetailsForm.value)).subscribe(details => {
                    if (details.success) {
                      this.snackBar.open('TPA Claim Updated Successfully', 'Claim Submission', { duration: 3000 });
                      this.getTPAClaimDetails(data).subscribe(response => { this.changeDetectorRefs.detectChanges() });
                      if (i == this.toothSurfaceSelectionList.length && j == this.toothCodeSelectionList.length) {
                        this.initializeTPAClaimDetailsForm();
                        this.primaryDentition = [];
                        this.adultDentition = [];
                      }
                    } else {
                      this.snackBar.open(details.errorMessage, 'ClaimDetails', { duration: 3000 });
                    }
                  })

                  // this.tpaClaimsForm.reset();
                  // this.moveStepperToIndex(0);
                  // this.router.navigate(['/claims/submission/internal'])
                } else {
                  this.snackBar.open(surfaceClaims.errorMessage, 'Surface Code Claims', { duration: 3000 });
                }
              });
            } else {
              this.snackBar.open(toothClaims.errorMessage, 'Tooth Claims', { duration: 3000 });
            }
          });


          // this.snackBar.open('Claim Updated Successfully', 'OK', { duration: 3000 });
          // this.tpaClaimsForm.reset();
          // this.tpaClaimDetailsForm.reset();
          // this.policy_id.reset();
          // this.moveStepperToIndex(0);
          // this.goBack();
          // this.router.navigate(['/claims/submission/' + this.routerURL[3] == 'clinic' ? 'clinicindex' : 'index'])
          // this.getTPAClaims();
        } else {
          this.snackBar.open(response.errorMessage, 'OK', { duration: 3000 });
        }
      });
    }
  }

  getToothCode() {
    this.toothCode.getToothCode(this.toothCodeReq).subscribe(response => {
      if (response.success) {
        this.toothCodeList = response.data
      } else {
        this.snackBar.open(response.errorMessage, 'OK', { duration: 3000 });
      }
    })
  }

  getTPAClaims() {
    this.dialog.open(ClaimsSubmissionComponent, {
      height: '600px',
      width: '600px',
      maxHeight: '500px',
      maxWidth: '800px'
    })
    // this.submission.getTPAClaims(0).subscribe(response => {
    //   if (response.success) {
    //     this.tpaClaims = response.data
    //   } else {
    //     this.snackBar.open(response.errorMessage, 'OK', { duration: 3000 });
    //   }
    // })
  }

  getTreatmentMasterSetup() {
    if (this.claimFormControls.Company_ID.value != null && this.claimFormControls.Company_ID.value != '') {
      this.tmaster.getTreatmentMaster(this.treatmentMasterSetup).subscribe(response => {
        if (response.success) {
          this.treatmentMasterSetupList = response.data;
          this.stepper.next();
        } else {
          this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 })
        }
      });
    } else {
      this.claimFormControls.Company_ID.setValidators(Validators.required);
      this.claimFormControls.Company_ID.markAsTouched({ onlySelf: true })
      this.snackBar.open('Select Patient, Patient Cannot be Empty', 'ok', { duration: 3000 })
    }

  }

  getTreatmentToothMasterSetup() {
    return this.tmaster.getTreatmentMasterToothCode(this.treatmentMasterSetup).pipe(
      tap(response => {
        if (response.success) {
          this.treatmentToothMaster = response.data;
        } else {
          this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 })
        }
      })
    );
  }

  onTreatmentGroupChange(event: MatSelectChange) {
    if (this.primaryDentition.length != 0) this.primaryDentition = []
    if (this.adultDentition.length != 0) this.adultDentition = []
    this.treatmentGrpList.filter(x => {
      if (x.Extraction_YN == 'Y') {
        this.snackBar.open('Treatment Group Should not be Extracted', 'Close')
      }
      if (x.Treatment_Group_ID === event) {
        this.treatmentMasterSetupList.find(y => {
          if (y.Treatment_Group_ID === x.Treatment_Group_ID) {
            this.treatmentCodeReq.Treatment_Code_ID = y.Treatment_Code_ID;
            this.treatmentCode.getTreatmentCode(this.treatmentCodeReq).subscribe(response => {
              if (response.success) {
                this.treatmentCodeList = response.data;
              } else {
                this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 })
              }
            })
          }
        });
      }
    });
  }

  moveStepperToIndex(index: number) {
    this.stepper.selectedIndex = index;
    this.stepper.steps.toArray().forEach(step => { step.interacted = false })
  }

  groupBy(arr, property) {
    return arr.reduce(function (memo, x) {
      if (!memo[x[property]]) { memo[x[property]] = []; }
      memo[x[property]].push(x);
      return memo;
    }, {});
  }

  goBack() {
    if (this.routerURL[3] == 'internal') {
      this.router.navigate(['/claims/submission/index'])
    }
    if (this.routerURL[3] == 'clinic') {
      this.router.navigate(['/claims/submission/clinicindex'])
    }
  }

  onPatientSelect() {
    return this.submission.getTPAClaims(0).pipe(
      tap(response => {
        if (response.success) {
          this.tpaClaims = response.data
        } else {
          this.snackBar.open(response.errorMessage, 'OK', { duration: 3000 });
        }
      })
    );
  }

  getTPAClaimDetails(TPA_Claim_ID) {
    return this.submission.getTPAClaimDetails(TPA_Claim_ID).pipe(
      tap(response => {
        if (response.success) {
          this.dataSource.data = response.data;
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
          this.length = this.dataSource.data.length;

        } else {
          this.snackBar.open(response.errorMessage, 'OK', { duration: 3000 })
          this.dataSource.data = [];
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
          this.length = 0;
        }
      })

    );

  }

}
