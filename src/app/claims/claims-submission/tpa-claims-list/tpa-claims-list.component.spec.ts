import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TpaClaimsListComponent } from './tpa-claims-list.component';

describe('TpaClaimsListComponent', () => {
  let component: TpaClaimsListComponent;
  let fixture: ComponentFixture<TpaClaimsListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TpaClaimsListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TpaClaimsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
