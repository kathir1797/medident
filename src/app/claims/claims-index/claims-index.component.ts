import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { SubmissionService } from './../../services/claims/submission.service';
import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-claims-index',
  templateUrl: './claims-index.component.html',
  styleUrls: ['./claims-index.component.css']
})
export class ClaimsIndexComponent implements OnInit, AfterViewInit {

  tpaClaims = new Array();
  url: any;
  userObj = JSON.parse(localStorage.getItem('currentUserDetails')).Clinic_ID;


  //Table Properties
  @ViewChild(MatPaginator) paginator: MatPaginator;
  dataSource = new MatTableDataSource<any>();
  length: number
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  displayedColumns = ["Clinic_Code", "Clinic_Name", "Doctor_Name", "Company_Name", "Status_Name"]

  constructor(private submission: SubmissionService, private snackBar: MatSnackBar, private router: Router) {
    this.url = this.router.url.split('/');
  }

  ngOnInit(): void {
    this.getTPAClaims();
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  getTPAClaims() {
    this.submission.getTPAClaims(0).subscribe(response => {
      if (response.success) {
        this.tpaClaims = response.data
        this.dataSource.data = response.data;
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      } else {
        this.snackBar.open(response.errorMessage, 'OK', { duration: 3000 });
      }
    })
  }

}
