import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router, ActivatedRoute } from '@angular/router';
import { DdlistService } from 'app/services/config/ddlist.service';
import { EmployeeService } from 'app/services/dataManager/employee.service';

@Component({
  selector: 'app-employee-profile',
  templateUrl: './employee-profile.component.html',
  styleUrls: ['./employee-profile.component.css']
})
export class EmployeeProfileComponent implements OnInit {
  picker: any;
  employeeForm: FormGroup;
  stateList = new Array();
  cityList = new Array();
  UserList = new Array();
  RaceList = new Array();
  GenderList = new Array();
  NationalityList = new Array();
  MaritialStatusList = new Array();
  toptitle: string = 'Add Principal';
  btntext: string = 'Save';
  urlText: any;
  isDisabled: boolean = true;
  obj = localStorage.getItem('currentuser');

  constructor(private fb: FormBuilder, private ddlist: DdlistService, private employee: EmployeeService, private snackBar: MatSnackBar, private router: Router, private activatedRoute: ActivatedRoute) {
    const url = this.router.url;
    this.urlText = url.split('/');
    console.log(this.urlText);

    this.employeeForm = this.fb.group({
      Employee_ID: 0,
      Employee_Code: new FormControl({ value: '', disabled: this.urlText[2] == 'view' ? true : false }),
      Employee_Name: new FormControl({ value: '', disabled: this.urlText[2] == 'view' ? true : false }),
      Employee_NRIC_No: new FormControl({ value: '', disabled: this.urlText[2] == 'view' ? true : false }),
      Employee_DOB: new FormControl({ value: '', disabled: this.urlText[2] == 'view' ? true : false }),
      Employee_POB: new FormControl({ value: '', disabled: this.urlText[2] == 'view' ? true : false }),
      Nationality_ID: new FormControl({ value: '', disabled: this.urlText[2] == 'view' ? true : false }),
      Race_ID: new FormControl({ value: '', disabled: this.urlText[2] == 'view' ? true : false }),
      Gender_ID: new FormControl({ value: '', disabled: this.urlText[2] == 'view' ? true : false }),
      Maritial_Status_ID: new FormControl({ value: '', disabled: this.urlText[2] == 'view' ? true : false }),
      Employee_Address: new FormControl({ value: '', disabled: this.urlText[2] == 'view' ? true : false }),
      State_ID: new FormControl({ value: '', disabled: this.urlText[2] == 'view' ? true : false }),
      City_ID: new FormControl({ value: '', disabled: this.urlText[2] == 'view' ? true : false }),
      Pincode: new FormControl({ value: '', disabled: this.urlText[2] == 'view' ? true : false }),
      Employee_Email: new FormControl({ value: '', disabled: this.urlText[2] == 'view' ? true : false }),
      Telephone_No: new FormControl({ value: '', disabled: this.urlText[2] == 'view' ? true : false }),
      Handphone_No: new FormControl({ value: '', disabled: this.urlText[2] == 'view' ? true : false }),
      User_ID: JSON.parse(this.obj).User_ID,
      Dependent_YN: true,
      Employee_Status_ID: 1,
    });
    this.activatedRoute.params.subscribe(params => {
      console.log(params)
      if (params['Employee_ID'] != 'create') {
        this.toptitle = 'Edit Principal';
        this.btntext = 'Update';
        this.employee.EditEmployee(params.Employee_ID).subscribe(response => {
          if (response.success) {
            this.employeeForm.patchValue({
              Employee_ID: response.data[0].Employee_ID,
              Employee_Code: response.data[0].Employee_Code,
              Employee_Name: response.data[0].Employee_Name,
              Employee_NRIC_No: response.data[0].Employee_NRIC_No,
              Employee_DOB: response.data[0].Employee_DOB,
              Employee_POB: response.data[0].Employee_POB,
              Nationality_ID: response.data[0].Nationality_ID,
              Race_ID: response.data[0].Race_ID,
              Gender_ID: response.data[0].Gender_ID,
              Maritial_Status_ID: response.data[0].Maritial_Status_ID,
              Employee_Address: response.data[0].Employee_Address,
              State_ID: response.data[0].State_ID,
              City_ID: response.data[0].City_ID,
              Pincode: response.data[0].Pincode,
              Employee_Email: response.data[0].Employee_Email,
              Telephone_No: response.data[0].Telephone_No,
              Handphone_No: response.data[0].Handphone_No,
              User_ID: JSON.parse(this.obj).User_ID,
            });
          } else {
            this.snackBar.open(response.errorMessage, 'OK', { duration: 3000 }); (response.errorMessage);
          }
        });

      }
    });
  }

  ngOnInit() {
    this.getStateDDList();
    this.getCityDDList();
    //this.getUserDDList();
    this.getNationalityDDList();
    this.getRaceDDList();
    this.getGenderDDList();
    this.getMaritialStatusDDList();
    if (this.urlText[2] == 'view') {
      this.toptitle = "Principal Detail";
      this.isDisabled = false;
    }
  }

  getStateDDList() {
    this.ddlist.getStateList().subscribe(response => {
      if (response.success) {
        this.stateList = response.data;
      } else {
        this.snackBar.open(response.errorMessage, 'OK', { duration: 3000 }); (response.errorMessage, "Ok");
      }
    });
  }

  getCityDDList() {
    this.ddlist.getCityList().subscribe(response => {
      if (response.success) {
        this.cityList = response.data;
      } else {
        this.snackBar.open(response.errorMessage, 'OK', { duration: 3000 }); (response.errorMessage, "Ok");
      }
    });
  }

  getUserDDList() {
    this.ddlist.getUserList().subscribe(response => {
      if (response.success) {
        this.UserList = response.data;
      } else {
        this.snackBar.open(response.errorMessage, 'OK', { duration: 3000 }); (response.errorMessage, "Ok");
      }
    });
  }

  getNationalityDDList() {
    this.ddlist.getNationalityDDList().subscribe(response => {
      if (response.success) {
        this.NationalityList = response.data;
      } else {
        this.snackBar.open(response.errorMessage, 'OK', { duration: 3000 }); (response.errorMessage, "Ok");
      }
    });
  }

  getRaceDDList() {
    this.ddlist.getRaceDDList().subscribe(response => {
      if (response.success) {
        this.RaceList = response.data;
      } else {
        this.snackBar.open(response.errorMessage, 'OK', { duration: 3000 }); (response.errorMessage, "Ok");
      }
    });
  }

  getGenderDDList() {
    this.ddlist.getGenderDDList().subscribe(response => {
      if (response.success) {
        this.GenderList = response.data;
      } else {
        this.snackBar.open(response.errorMessage, 'OK', { duration: 3000 }); (response.errorMessage, "Ok");
      }
    });
  }

  getMaritialStatusDDList() {
    this.ddlist.getMaritialStatusDDList().subscribe(response => {
      if (response.success) {
        this.MaritialStatusList = response.data;
      } else {
        this.snackBar.open(response.errorMessage, 'OK', { duration: 3000 }); (response.errorMessage, "Ok");
      }
    });
  }

  addEmployee() {
    if (this.employeeForm.valid) {
      this.employee.addEmployee(JSON.stringify(this.employeeForm.value)).subscribe(response => {
        if (response.success) {
          this.snackBar.open('Employee Saved Successfully', 'OK');
          this.employeeForm.reset();
          this.router.navigate(['/employee']);
        } else {
          this.snackBar.open(response.errorMessage, 'OK');
        }
      });
    } else {
      this.validateAllFormFields(this.employeeForm);
      this.snackBar.open('Form cannot Invalid', 'OK');
    }
  }

  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach((field) => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({
          onlySelf: true
        });
      } else if (control instanceof FormGroup) { this.validateAllFormFields(control); }
    });
  }

  getDependentsByPrincipal() {
    console.log(this.urlText[2])
    if (this.urlText[2] === 'view') {
      this.router.navigate(['/dataManager/dependent'], { queryParams: { Principal_ID: this.urlText[3] } })
    } else {
      this.router.navigate(['/dataManager/dependent'], { queryParams: { Principal_ID: this.urlText[2] } })
    }
  }


}