export class msterTPASetupRequest {
    Master_TPA_Setup_ID: Number;
}

export class msterTPARequest {
    MasterTPAID: Number;
}

export class dependentRequest {
    Dependent_ID: Number;
}

export class tpaMemberRequest {
    TPA_Member_ID: Number;
}

export class clinicsRequest {
    Clinic_ID: Number;
}

export class branchRequest {
    Branch_ID: Number;
}


export class productRequest {
    Product_ID: Number
}

export class doctorRequest {
    Doctor_ID: Number
}

export class doctorClinicRequest {
    Doctor_Clinic_ID: Number
}


export class tpaMemberImport {
    TPA_Member_Import_ID: number
    Batch_Number: string
    Serial_No: number
    TPA_Member_ID: string
    Staff_Code: string
    Staff_Name: string
    IC_Number: string
    Relationship: string
    Nationality: string
    Gender: string
    Ref_Date: Date
    Date_Of_Birth: Date
    Race: string
    Effective_Date: Date
    Exp_Date: Date
    TPA_Plan: string
    Individual_Limit: number
    Family_Limit: number
    Shared_limit: number
    Category: string
    TPA_Import_Status_ID: number
    Treatment_Plan_ID: number
    Treatment_Category_ID: number
    User_ID: number
    Reference_Number: number
    Remarks: string
    Action_Status: string
    Mobile_Number: string
    Phone_Number: string
    Email_Address: string
    Marital_Status: string
    Principal_Address: string
    Principal_City: string
    Principal_State: string
    Principal_PostCode: string
}

export class toothCodeSurface {
    Tooth_Surface_ID: number
}

export class specialistEquipment {
    Special_Equipment_ID: number
}

export class treatmentGroupLimit {
    Treatment_Plan_Group_Limit_ID: number
    Treatment_Plan_ID: number
    Treatment_Group_ID: number
    Claim_Limit: number
    Treatment_Plan_Group_Limit_Status_ID: number = 1
    User_ID: number
}

export class companyTreatmentPlanRequest {
    Treatment_Plan_Company_ID: number
}

export class Insurer {
    Insurer_ID: number
}