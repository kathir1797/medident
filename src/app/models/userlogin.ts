export class login {
    User_ID: Number;
    User_Name: string;
    Password: string;
    User_Type_ID: Number;
    Status_ID: Number;
}

export class User {
    usr_id: Number;
    comp_id: Number;
    usr_name: string;
    active: boolean;
    usr_address: string;
    usr_address1: string;
    usr_zipcode: Number;
    state_name: string;
    city_name: string;
    user_type: string;
    pswd_reset: boolean;
}