export class apiResponseObj {
    success: boolean
    errorMessage: string
    data: any
    token?: string
}
