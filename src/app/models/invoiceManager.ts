export class TPA_Invoice_Details_SP {
    TPA_Invoice_ID: number
    Customer_Type_ID: number
    Customer_ID: number
    Invoice_Number: number
    PO_SO_Number: number
    Invoice_Date: Date
    Payment_Term: number
    Invoice_Amount: number
    Tax_Type_ID: number
    Tax_Amount: number
    Invoice_Status_ID: number
    User_ID: number
}

export class TPA_Invoices_SP {
    TPA_Invoice_Detail_ID: number
    TPA_Invoice_ID: number
    Item_Name: string
    Item_Description: string
    Item_Quantity: number
    Item_Price: number
    Invoice_Detail_Status_ID: number
    User_ID: number
}