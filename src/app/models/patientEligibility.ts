export class CheckPatientEligibility {
    Policy_Number: string
    Principal_Name: string
    Phone_Number: string
    Patient_ID: any;
    Patient_Name: string
    Dependent_ID: number
    IC: any;
}