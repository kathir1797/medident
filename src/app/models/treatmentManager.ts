export class toothCodeRequest {
    Tooth_Code_ID: number
}

export class treatmentCodeRequest {
    Treatment_Code_ID: number
}


export class treatmentCodeByClinic {
    Treatment_Code_Clinic_ID: number
}

export class treatmentGroup {
    Treatment_Group_ID: number
}

export class treatmentCategory {
    Treatment_Category_ID: number
}

export class treatmentMasterSetup{
    Treatment_Master_ID: number
}