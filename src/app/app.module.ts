import { MatButtonModule } from '@angular/material/button';
import { AlertDialogComponent } from './dialog/alert-dialog/alert-dialog.component';
import { NavigationItem } from './components/menu';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';


import { AppRoutingModule } from './app.routing';
import { ComponentsModule } from './components/components.module';

import { AppComponent } from './app.component';

import { HttpClientModule } from '@angular/common/http';


import { AdminLayoutComponent } from './layouts/admin-layout/admin-layout.component';
import { LoginComponent } from './login/login.component';
import { NgxUiLoaderModule, NgxUiLoaderHttpModule, NgxUiLoaderRouterModule, NgxUiLoaderConfig } from 'ngx-ui-loader';
import { MAT_DATE_LOCALE } from '@angular/material/core';

const ngxUiLoaderConfig: NgxUiLoaderConfig = {
  bgsColor: "blue",
  bgsOpacity: 0.5,
  bgsPosition: "bottom-right",
  bgsSize: 60,
  bgsType: "three-strings",
  blur: 3,
  delay: 0,
  fastFadeOut: true,
  fgsColor: "#7fe5d2",
  fgsPosition: "center-center",
  fgsSize: 30,
  fgsType: "three-strings",
  gap: 24,
  logoPosition: "center-center",
  logoSize: 100,
  logoUrl: "",
  masterLoaderId: "master",
  overlayBorderRadius: "0",
  overlayColor: "rgba(40, 40, 40, 0.8)",
  pbColor: "#7fe5d2",
  pbDirection: "ltr",
  pbThickness: 3,
  hasProgressBar: true,
  text: "Loading...",
  textColor: "#FFFFFF",
  textPosition: "center-center",
  maxTime: -1,
  minTime: 300
};

@NgModule({
  imports: [
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    HttpClientModule,
    ComponentsModule,
    RouterModule,
    AppRoutingModule,
    // AgmCoreModule.forRoot({
    //   apiKey: 'YOUR_GOOGLE_MAPS_API_KEY'
    // }),
    NgxUiLoaderModule.forRoot(ngxUiLoaderConfig),
    NgxUiLoaderHttpModule.forRoot({ showForeground: true }),
    NgxUiLoaderRouterModule.forRoot({ showForeground: true }),
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule
  ],
  declarations: [
    AppComponent,
    AdminLayoutComponent,
    LoginComponent,
    AlertDialogComponent
  ],
  providers: [NavigationItem, { provide: MAT_DATE_LOCALE, useValue: 'en-GB' }],
  bootstrap: [AppComponent]
})
export class AppModule { }
