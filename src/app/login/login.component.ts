import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { User, login } from 'app/models/userlogin';
import { isNullOrUndefined } from 'util';
import { MatSnackBar } from '@angular/material/snack-bar';
import { LoginService } from 'app/services/config/login.service';
import { duration } from 'moment';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  userData: login = new login();
  constructor(private router: Router, private authlogin: LoginService, private snackBar: MatSnackBar) { }

  ngOnInit(): void {
  }

  onLoggedIn(): Observable<User> {
    if (isNullOrUndefined(this.userData.User_Name) && isNullOrUndefined(this.userData.Password)) {
      console.log('user logg')
      this.snackBar.open('Enter UserName and Password', 'Login');
      // this.router.navigate(['/error']);
      return
    } else if (isNullOrUndefined(this.userData.User_Name) || this.userData.Password.trim() === '') {
      this.snackBar.open('Please enter valid UserName', 'Login');
    } else if (isNullOrUndefined(this.userData.User_Name) || this.userData.Password.trim() === '') {
      this.snackBar.open('Please enter password.', 'Login');
    }
    else {
      this.authlogin.login(this.userData).subscribe((response) => {
        if (response.success) {
          localStorage.setItem('currentuser', JSON.stringify(response.data));
          localStorage.setItem('token', response.token);
          this.authlogin.loginUserDetails(this.userData).subscribe(user => {
            if (user.success) {
              localStorage.setItem('currentUserDetails', JSON.stringify(user.data))
            } else {
              this.snackBar.open(response.errorMessage, 'LoginDetails', { duration: 3000 });
            }
          }, error => {
            this.snackBar.open(error.message, 'LoginDetails', { duration: 3000 });
          })
          if (JSON.parse(localStorage.getItem('currentuser')).User_ID != null) {
            this.router.navigate(['/dashboard']);;
          }
          else {
            this.snackBar.open("Invalid User Credentials", 'Login', { duration: 3000 });
          }
        }
        else {
          this.snackBar.open(response.errorMessage, 'Login', { duration: 3000 });
        }
      }, error => {
        this.snackBar.open(error.message, 'Login', { duration: 3000 });
      });
    }
  }

}
