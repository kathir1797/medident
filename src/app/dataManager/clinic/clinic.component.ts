import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { clinicsRequest } from 'app/models/dataManager';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ClinicsService } from 'app/services/dataManager/clinics.service';

@Component({
  selector: 'app-clinic',
  templateUrl: './clinic.component.html',
  styleUrls: ['./clinic.component.css']
})
export class ClinicComponent implements AfterViewInit,OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  dataSource = new MatTableDataSource<any>();
  length: number
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['Clinic_Code', 'Clinic_Name','Clinic_Email', 'State_Name', 'City_Name',  'Clinic_Address','Action'];

  clinicsData: clinicsRequest = new clinicsRequest();
  constructor(private clinics: ClinicsService, private snackBar: MatSnackBar) { }

  ngOnInit() {
    this.clinicsData.Clinic_ID = 0;
    this.getTPAMemberList();
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  getTPAMemberList() {
    this.clinics.getClinicsList(this.clinicsData).subscribe(response => {
      if (response.success) {
        this.dataSource.data = response.data;
        this.dataSource.sort = this.sort;
        this.length = response.data.length;
      } else {
        this.snackBar.open(response.errorMessage, "Ok", { duration: 3000 });
      }
    });
  }
}

