import { Router } from '@angular/router';
import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { MatSnackBar } from '@angular/material/snack-bar';
import { msterTPARequest } from 'app/models/dataManager';
import { MasterTPAService } from 'app/services/dataManager/master-tpa.service';

@Component({
  selector: 'app-master-tpa',
  templateUrl: './master-tpa.component.html',
  styleUrls: ['./master-tpa.component.css']
})
export class MasterTPAComponent implements AfterViewInit, OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  dataSource = new MatTableDataSource<any>();
  length: number
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['Master_TPA_ID', 'Company_Name', 'Action'];

  msterTPAData: msterTPARequest = new msterTPARequest();
  constructor(private masterTPAsrv: MasterTPAService, private snackBar: MatSnackBar, private router: Router) { }

  ngOnInit() {
    this.msterTPAData.MasterTPAID = 0;
    this.getMasterTPAList();
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  getMasterTPAList() {
    this.masterTPAsrv.getMasterTPA(this.msterTPAData).subscribe(response => {
      if (response.success) {
        this.dataSource.data = response.data;
        this.dataSource.sort = this.sort;
        this.length = response.data.length;
      } else {
        this.snackBar.open(response.errorMessage, "Ok", {
          duration: 3000
        });
      }
    });
  }

  addMasterTPABenefits(tpaID, masterID) {
    this.router.navigate(['/dataManager/master/master-tpa-benefit'], { queryParams: { Master_TPA_ID: tpaID, MasterTPAID: masterID } })
  }

  addSubscription(tpaID, masterID) {
    this.router.navigate(['/dataManager/master/master-tpa-service'], { queryParams: { Master_TPA_ID: tpaID, MasterTPAID: masterID } })
  }
}

