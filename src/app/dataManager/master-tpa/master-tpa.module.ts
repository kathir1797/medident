import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MasterTpaRoutingModule } from './master-tpa-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    MasterTpaRoutingModule
  ]
})
export class MasterTpaModule { }
