import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MasterTPAComponent } from './master-tpa.component';

describe('MasterTPAComponent', () => {
  let component: MasterTPAComponent;
  let fixture: ComponentFixture<MasterTPAComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MasterTPAComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MasterTPAComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
