import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { branchRequest } from 'app/models/dataManager';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { MatSnackBar } from '@angular/material/snack-bar';
import { BranchService } from 'app/services/dataManager/branch.service';

@Component({
  selector: 'app-branch',
  templateUrl: './branch.component.html',
  styleUrls: ['./branch.component.css']
})
export class BranchComponent implements AfterViewInit,OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  dataSource = new MatTableDataSource<any>();
  length: number
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['Branch_Code', 'Branch_Description', 'Status_Name', 'Action'];

  branchData: branchRequest = new branchRequest();
  constructor(private branchser: BranchService, private snackBar: MatSnackBar) { }

  ngOnInit() {
    this.branchData.Branch_ID = 0;
    this.getBranchMasterList();
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  getBranchMasterList() {
    this.branchser.getBranchMaster(this.branchData).subscribe(response => {
      if (response.success) {
        this.dataSource.data = response.data;
        this.dataSource.sort = this.sort;
        this.length = response.data.length;
      } else {
        this.snackBar.open(response.errorMessage, "Ok", { duration: 3000 });
      }
    });
  }
}

