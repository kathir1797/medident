import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MasterTPAsetupProfileRoutingModule } from './master-tpasetup-profile-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    MasterTPAsetupProfileRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class MasterTPAsetupProfileModule { }
