import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MasterTPAsetupProfileComponent } from './master-tpasetup-profile.component';

describe('MasterTPAsetupProfileComponent', () => {
  let component: MasterTPAsetupProfileComponent;
  let fixture: ComponentFixture<MasterTPAsetupProfileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MasterTPAsetupProfileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MasterTPAsetupProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
