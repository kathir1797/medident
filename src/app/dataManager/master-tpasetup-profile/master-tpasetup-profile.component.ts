import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { MatDatepicker } from '@angular/material/datepicker';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router, ActivatedRoute } from '@angular/router';
import { DdlistService } from 'app/services/config/ddlist.service';
import { MasterTPASetupService } from 'app/services/dataManager/master-tpasetup.service';

@Component({
  selector: 'app-master-tpasetup-profile',
  templateUrl: './master-tpasetup-profile.component.html',
  styleUrls: ['./master-tpasetup-profile.component.css']
})
export class MasterTPAsetupProfileComponent implements OnInit {

  masterTPASetupForm: FormGroup;
  toptitle: string = 'Add MasterTPA Setup';
  btntext: string = 'Save';
  urlText: any;
  isDisabled: boolean = true;
  isDisabledGenerate: boolean = true;
  obj = localStorage.getItem('currentuser');
  branchList = new Array();
  productList = new Array();
  startDate = new Date()
  endDate = new Date()
  statusList = new Array();

  constructor(private fb: FormBuilder, private ddlist: DdlistService, private masterTPA: MasterTPASetupService,
    private snackBar: MatSnackBar, private router: Router, private activatedRoute: ActivatedRoute) {
    const url = this.router.url;
    this.urlText = url.split('/');
    console.log(this.urlText);
    this.endDate = new Date(this.startDate.getFullYear() + 2, this.startDate.getMonth(), this.startDate.getDate())
    this.masterTPASetupForm = this.fb.group({
      Master_TPA_Setup_ID: 0,
      Branch_Code: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      Branch_Description: new FormControl({ value: '', disabled: true }),
      Master_TPA_ID: new FormControl({ value: '', disabled: true }),
      Product_Code: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      Product_Description: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      Master_TPA_Setup_Status_ID: 1,
      Start_Date: new FormControl({ value: this.startDate, disabled: this.urlText[3] == 'view' ? true : false }),
      End_Date: new FormControl({ value: this.endDate, disabled: this.urlText[3] == 'view' ? true : false }),
      User_ID: JSON.parse(this.obj).User_ID,
    });
    this.activatedRoute.params.subscribe(params => {
      if (params['Master_TPA_Setup_ID'] != 'create') {
        this.toptitle = 'Edit MasterTPA Setup';
        this.btntext = 'Update';
        this.isDisabledGenerate = false;
        this.masterTPA.editMasterTPASetup(params['Master_TPA_Setup_ID']).subscribe(response => {
          if (response.success) {
            this.masterTPASetupForm.patchValue({
              Master_TPA_Setup_ID: response.data[0].Master_TPA_Setup_ID,
              Branch_Code: response.data[0].Branch_Code,
              Branch_Description: response.data[0].Branch_Description,
              Product_Code: response.data[0].Product_Code,
              Product_Description: response.data[0].Product_Description,
              Master_TPA_ID: response.data[0].Master_TPA_ID,
              Master_TPA_Setup_Status_ID: response.data[0].Master_TPA_Setup_Status_ID,
              Start_Date: response.data[0].Start_Date,
              End_Date: response.data[0].End_Date,
              User_ID: JSON.parse(this.obj).User_ID,
            });
          } else {
            this.snackBar.open(response.errorMessage, 'OK', {
              duration: 3000
            });
          }
        });

      }
    });
  }

  ngOnInit() {
    this.getBranchDDLists();
    this.getProductDDlist();
    this.getStatusDDLists();
    if (this.urlText[3] == 'view') {
      this.toptitle = "MasterTPA Setup Detail";
      this.isDisabled = false;
    }
  }

  BindingValue(event) {
    // console.log(event);
    this.branchList.filter(value => {
      if (value.Branch_Code === event.value) {
        this.masterTPASetupForm.patchValue({
          // Branch_Code: value.Branch_Code,
          Branch_Description: value.Branch_Description,
        });
      }
    });

  }

  BindingProductDescription(event) {
    console.log(event.value, this.productList)
    this.productList.filter(value => {
      if (value.Product_Code === event.value) {
        this.masterTPASetupForm.patchValue({
          Product_Description: value.Product_Description,
        });
      }
    });
  }

  getBranchDDLists() {
    this.ddlist.getBranchDDList().subscribe(response => {
      if (response.success) {
        this.branchList = response.data;
      } else {
        this.snackBar.open(response.errorMessage, 'OK', { duration: 3000 }); (response.errorMessage, "Ok");
      }
    });
  }

  getProductDDlist() {
    this.ddlist.getProductDDList().subscribe(response => {
      if (response.success) {
        this.productList = response.data;
      } else {
        this.snackBar.open(response.errorMessage, 'OK', { duration: 3000 }); (response.errorMessage, "Ok");
      }
    });
  }


  addMasterTPASetup() {
    console.log(this.masterTPASetupForm.getRawValue())
    if (this.masterTPASetupForm.valid) {
      this.masterTPA.addMasterTPASetup(this.masterTPASetupForm.getRawValue()).subscribe(response => {
        if (response.success) {
          this.snackBar.open('MasterTPA Setup Saved Successfully', 'OK', {
            duration: 3000
          });
          this.masterTPASetupForm.reset();
          this.router.navigate(['/dataManager/master-tpasetup']);
        } else {
          this.snackBar.open(response.errorMessage, 'OK');
        }
      });
    } else {
      this.validateAllFormFields(this.masterTPASetupForm);
      this.snackBar.open('Form cannot Invalid', 'OK');
    }
  }

  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach((field) => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({
          onlySelf: true
        });
      } else if (control instanceof FormGroup) { this.validateAllFormFields(control); }
    });
  }

  generateMASTERTPAID() {
    if (this.masterTPASetupForm.get('Product_Code').value != null || this.masterTPASetupForm.get('Branch_Code').value != null) {
      this.masterTPA.generateMASTERTPAID().subscribe(response => {
        if (response.success) {
          this.masterTPASetupForm.patchValue({
            Master_TPA_ID: this.masterTPASetupForm.get('Product_Code').value + '-' + this.masterTPASetupForm.get('Branch_Code').value + '-' + this.startDate.getFullYear() + '-' + response.data
          });
        } else {
          this.snackBar.open(response.errorMessage)
        }
      })
    } else {
      this.snackBar.open('Product_Code and Branch_Code cannot be null')
    }
  }

  onStartDateChange() {
    this.startDate = this.masterTPASetupForm.get('Start_Date').value
    this.endDate = new Date(this.startDate.getFullYear() + 2, this.startDate.getMonth(), this.startDate.getDate())
    this.masterTPASetupForm.get('End_Date').patchValue(this.endDate)
  }

  getStatusDDLists() {
    this.ddlist.getStatusDDList().subscribe(response => {
      if (response.success) {
        this.statusList = response.data;
      } else {
        this.snackBar.open(response.errorMessage, 'OK', { duration: 3000 }); (response.errorMessage, "Ok");
      }
    });
  }


}