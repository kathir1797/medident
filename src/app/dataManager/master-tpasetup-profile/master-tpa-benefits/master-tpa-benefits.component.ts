import { Router, ActivatedRoute } from '@angular/router';
import { DdlistService } from './../../../services/config/ddlist.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ConfigService } from './../../../services/config/config.service';
import { Component, OnInit } from '@angular/core';
import { TpaBenefitsService } from 'app/services/benefitoption/tpa-benefits.service';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'app-master-tpa-benefits',
  templateUrl: './master-tpa-benefits.component.html',
  styleUrls: ['./master-tpa-benefits.component.css']
})
export class MasterTpaBenefitsComponent implements OnInit {

  masterTPABenefits = this.configService.configJson.modules.DataManager.MasterTPAIDSetup.MasterTPABenefits;
  addMasterTPABenefits = this.configService.configJson.modules.DataManager.MasterTPAIDSetup.MasterTPABenefits.Add;
  masterTPABenefitsForm: FormGroup;
  benefitPlanList = new Array();
  statusList = new Array();
  obj = localStorage.getItem('currentuser');
  masterTPABenefitsList = new Array();

  constructor(private configService: ConfigService, private fb: FormBuilder, private snackBar: MatSnackBar,
    private tpaBenefits: TpaBenefitsService, private ddlist: DdlistService, private router: Router, private activatedRoute: ActivatedRoute) {
    this.initializeMasterTPABenefits();
    this.activatedRoute.queryParams.subscribe(queryParam => {
      this.masterTPABenefitsForm.get('MasterTPAID').patchValue(queryParam.MasterTPAID);
      this.getMasterTPABenefits(queryParam.Master_TPA_ID).subscribe(response => {
        if (response.success) {
          if (this.masterTPABenefitsList.length > 0) {
            // this.masterTPABenefits.Title = 'Update Master TPA Float Details';
            this.masterTPABenefitsForm.patchValue({
              Master_TPA_Float_ID: this.masterTPABenefitsList[0].Master_TPA_Float_ID,
              MasterTPAID: this.masterTPABenefitsList[0].MasterTPAID,
              Float_Amount: this.masterTPABenefitsList[0].Float_Amount,
              Threshold_Warning: this.masterTPABenefitsList[0].Threshold_Warning,
              Threshold_Hold: this.masterTPABenefitsList[0].Threshold_Hold,
              Threshold_Stop: this.masterTPABenefitsList[0].Threshold_Stop,
              Master_TPA_Float_Status_ID: this.masterTPABenefitsList[0].Master_TPA_Float_Status_ID,
              User_ID: JSON.parse(this.obj).User_ID
            });
          }
        } else {
          this.initializeMasterTPABenefits();
        }
      })
    })

  }

  ngOnInit(): void {
    this.getTreatmentBenefitPlan();
    this.getStatusDDLists();

  }

  initializeMasterTPABenefits() {
    this.masterTPABenefitsForm = this.fb.group({
      Master_TPA_Float_ID: 0,
      MasterTPAID: '',
      // TPA_Benefit_ID: '',
      Float_Amount: new FormControl('', Validators.required),
      Threshold_Warning: new FormControl('', Validators.required),
      Threshold_Hold: new FormControl('', Validators.required),
      Threshold_Stop: new FormControl('', Validators.required),
      Master_TPA_Float_Status_ID: 1,
      User_ID: JSON.parse(this.obj).User_ID
    }, {
      validators:
        [
          this.tpaBenefits.rangeValidation('Float_Amount'),
          this.tpaBenefits.rangeValidation('Threshold_Warning'),
          this.tpaBenefits.rangeValidation('Threshold_Hold'),
          this.tpaBenefits.rangeValidation('Threshold_Stop')
        ]
    })
  }

  get float() {
    return this.masterTPABenefitsForm.controls;
  }

  getTreatmentBenefitPlan() {
    this.tpaBenefits.getTPABenefits(0).subscribe(response => {
      if (response.success) {
        this.benefitPlanList = response.data
      } else {
        this.snackBar.open(response.errorMessage, 'OK', { duration: 3000 });
      }
    })
  }

  getStatusDDLists() {
    this.ddlist.getStatusDDList().subscribe(response => {
      if (response.success) {
        this.statusList = response.data;
      } else {
        this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 })
      }
    });
  }

  saveMasterTPABenefits() {
    if (this.masterTPABenefitsForm.valid) {
      this.tpaBenefits.addmasterTPAbenefits(JSON.stringify(this.masterTPABenefitsForm.value)).subscribe(response => {
        if (response.success) {
          this.snackBar.open('Master TPA Float Saved Successfully', 'OK', { duration: 3000 });
          this.router.navigate(['/dataManager/master-tpa']);
          this.masterTPABenefitsForm.reset();
        } else {
          this.snackBar.open(response.errorMessage, 'OK', { duration: 3000 });
        }
      })
    } else {
      this.validateAllFormFields(this.masterTPABenefitsForm)
      this.snackBar.open('Form cannot be Invalid or Empty', 'OK', { duration: 3000 });
    }

  }

  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach((field) => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({
          onlySelf: true
        });
      } else if (control instanceof FormGroup) { this.validateAllFormFields(control); }
    });
  }

  getMasterTPABenefits(masterID) {
    return this.tpaBenefits.getMasterTPAbenefits(masterID, 0).pipe(
      tap(response => {
        if (response.success) {
          this.masterTPABenefitsList = response.data
        } else {
          this.snackBar.open(response.errorMessage, 'OK', { duration: 3000 });
        }
      })
    )
  }

}
