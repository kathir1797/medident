import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MasterTPAsetupProfileComponent } from './master-tpasetup-profile.component';


const routes: Routes = [{
  path: '',
  component: MasterTPAsetupProfileComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MasterTPAsetupProfileRoutingModule { }
