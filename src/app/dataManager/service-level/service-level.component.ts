import { MatRadioChange } from '@angular/material/radio';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router, ActivatedRoute } from '@angular/router';
import { TpaBenefitsService } from 'app/services/benefitoption/tpa-benefits.service';
import { ConfigService } from 'app/services/config/config.service';
import { DdlistService } from 'app/services/config/ddlist.service';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'app-service-level',
  templateUrl: './service-level.component.html',
  styleUrls: ['./service-level.component.css']
})
export class ServiceLevelComponent implements OnInit {

  masterTPABenefits = this.configService.configJson.modules.DataManager.MasterTPAIDSetup.MasterTPASubscriptionSetup;
  addMasterTPABenefits = this.configService.configJson.modules.DataManager.MasterTPAIDSetup.MasterTPASubscriptionSetup.Add;
  subscriptionForm: FormGroup;
  statusList = new Array();
  obj = localStorage.getItem('currentuser');
  sub: boolean = true;
  masterTPASubscription = new Array();

  subChecked: boolean = true;
  pycChecked: boolean = false;

  constructor(private configService: ConfigService, private fb: FormBuilder, private snackBar: MatSnackBar,
    private tpaBenefits: TpaBenefitsService, private ddlist: DdlistService, private router: Router, private activatedRoute: ActivatedRoute) {
    this.initializeMasterTPASubscriptionForm();
    this.activatedRoute.queryParams.subscribe(queryParam => {
      this.subscriptionForm.get('MasterTPAID').patchValue(queryParam.MasterTPAID);
      this.getMasterTPABenefits(queryParam.MasterTPAID).subscribe(response => {
        if (response.success) {
          if (this.masterTPASubscription.length > 0) {
            this.subChecked = this.masterTPASubscription[0].Subscription_Type == 1 ? true : false;
            this.pycChecked = this.masterTPASubscription[0].Subscription_Type == 2 ? true : false;

            this.sub = this.subChecked ? true : false;



            // this.masterTPABenefits.Title = 'Update Master TPA Subscription Details';
            this.subscriptionForm.patchValue({
              Master_TPA_Subscription_ID: this.masterTPASubscription[0].Master_TPA_Subscription_ID,
              MasterTPAID: this.masterTPASubscription[0].MasterTPAID,
              Subscription_Type: this.masterTPASubscription[0].Subscription_Type,
              Flat_Fee: this.masterTPASubscription[0].Flat_Fee,
              Service_Fee: this.masterTPASubscription[0].Service_Fee,
              Master_TPA_Subscription_Status_ID: this.masterTPASubscription[0].Master_TPA_Subscription_Status_ID,
              User_ID: JSON.parse(this.obj).User_ID
            })
          }
        } else {
          this.initializeMasterTPASubscriptionForm();
        }
      })
    })

  }

  ngOnInit(): void {
  }

  initializeMasterTPASubscriptionForm() {
    this.subscriptionForm = this.fb.group({
      Master_TPA_Subscription_ID: 0,
      MasterTPAID: new FormControl(''),
      Subscription_Type: 1,
      Flat_Fee: new FormControl(''),
      Service_Fee: new FormControl(0),
      Master_TPA_Subscription_Status_ID: 1,
      User_ID: JSON.parse(this.obj).User_ID
    }, {
      validators: [this.tpaBenefits.rangeValidation('Flat_Fee'), this.tpaBenefits.rangeValidation('Service_Fee')]
    })
  }

  get subscription() {
    return this.subscriptionForm.controls;
  }

  onSubscriptionTypeChange(event: MatRadioChange) {
    if (event.value == 1) {
      this.sub = true;
      this.subscriptionForm.patchValue({ Subscription_Type: parseInt(event.value), Service_Fee: 0 })
    } else {
      this.sub = false;
      this.subscriptionForm.patchValue({ Subscription_Type: parseInt(event.value), Flat_Fee: 0 })
    }
  }

  saveMasterTPASubscription() {
    if (this.subscriptionForm.valid) {
      this.tpaBenefits.saveMasterTPASubscriptionSetup(JSON.stringify(this.subscriptionForm.getRawValue())).subscribe(response => {
        if (response.success) {
          this.subscriptionForm.reset();
          this.router.navigate(['/dataManager/master-tpa']);
          this.snackBar.open('Master TPA Subscription Saved Successfully', 'Ok', { duration: 3000 })
        } else {
          this.snackBar.open(response.errorMessage, 'Ok', { duration: 3000 })
        }
      })
    } else {
      this.snackBar.open('Form Cannot be Invalid or empty', 'Ok', { duration: 3000 })
    }
  }

  getMasterTPABenefits(masterID) {
    return this.tpaBenefits.getMasterTPASubscription(0, masterID).pipe(
      tap(response => {
        if (response.success) {
          this.masterTPASubscription = response.data
        } else {
          this.snackBar.open(response.errorMessage, 'OK', { duration: 3000 });
        }
      })
    )
  }

  flatFeeRangeValidator(control: string) {
    return (formGroup: FormGroup) => {
      const rangeControl = formGroup.controls[control];
      if (!rangeControl) {
        return null;
      }

      if (rangeControl.errors && !rangeControl.errors.flat) {
        return null;
      }

      if (rangeControl.value <= 0) {
        rangeControl.setErrors({ flat: true });
      } else {
        rangeControl.setErrors(null);
      }
    }
  }

  serviceFeeRangeValidator(control: string) {
    return (formGroup: FormGroup) => {
      const rangeControl = formGroup.controls[control];
      console.log(rangeControl.value)
      if (!rangeControl) {
        return null;
      }

      if (rangeControl.errors && !rangeControl.errors.service) {
        return null;
      }

      if (rangeControl.value <= 0 || rangeControl.value > 100) {
        rangeControl.setErrors({ service: true });
      } else {
        rangeControl.setErrors(null);
      }
    }
  }

}
