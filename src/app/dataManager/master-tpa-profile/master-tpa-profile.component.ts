import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router, ActivatedRoute } from '@angular/router';
import { TpaBenefitsService } from 'app/services/benefitoption/tpa-benefits.service';
import { DdlistService } from 'app/services/config/ddlist.service';
import { MasterTPAService } from 'app/services/dataManager/master-tpa.service';
import { TpamemberService } from 'app/services/dataManager/tpamember.service';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'app-master-tpa-profile',
  templateUrl: './master-tpa-profile.component.html',
  styleUrls: ['./master-tpa-profile.component.css']
})
export class MasterTPAProfileComponent implements OnInit {
  companyList = new Array();
  benefitPlanList = new Array();
  masterSetupList = new Array();
  treatmentPlanList = new Array();
  masterTPAForm: FormGroup;
  toptitle: string = 'Add Master TPA Company Setup';
  btntext: string = 'Save';
  urlText: any;
  isDisabled: boolean = true;
  isGenerateDisabled: boolean = true;
  obj = localStorage.getItem('currentuser');

  constructor(private fb: FormBuilder, private ddlist: DdlistService, private masterTPA: MasterTPAService, private snackBar: MatSnackBar, private treatment: TpamemberService,
    private router: Router, private activatedRoute: ActivatedRoute, private tpaBenefits: TpaBenefitsService) {
    const url = this.router.url;
    this.urlText = url.split('/');
    console.log(this.urlText);

    this.masterTPAForm = this.fb.group({
      MasterTPAID: 0,
      Master_TPA_ID: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      New_Master_TPA_ID: new FormControl({ value: '', disabled: true }),
      Company_ID: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      Treatment_Plan_ID: new FormControl({ value: 0, disabled: this.urlText[3] == 'view' ? true : false }),
      Benefit_Plan_ID: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      //Master_TPA_Setup_Status_ID: 1,
      TPA_Service_Level_ID: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      User_ID: JSON.parse(this.obj).User_ID,
    });
    this.activatedRoute.params.subscribe(params => {
      if (params['MasterTPAID'] != 'create') {
        console.log(this.masterSetupList);
        this.toptitle = 'Edit Master TPA Company Setup';
        this.isGenerateDisabled = false;
        this.btntext = 'Update';
        this.masterTPA.editMasterTPA(params['MasterTPAID']).subscribe(response => {
          if (response.success) {
            this.masterTPAForm.patchValue({
              MasterTPAID: response.data[0].MasterTPAID,
              Master_TPA_ID: response.data[0].Master_TPA_ID,
              Company_ID: response.data[0].Company_ID,
              New_Master_TPA_ID: response.data[0].New_Master_TPA_ID,
              TPA_Service_Level_ID: response.data[0].TPA_Service_Level_ID,
              //Company_Code: response.data[0].Company_Code,
              Treatment_Plan_ID: response.data[0].Treatment_Plan_ID,
              // Benefit_Plan_ID: response.data[0].Benefit_Plan_ID,
              //Master_TPA_Setup_Status_ID: response.data[0].Master_TPA_Setup_Status_ID,
              User_ID: JSON.parse(this.obj).User_ID,
            });
          } else {
            this.snackBar.open(response.errorMessage, "Ok", {
              duration: 3000
            });
          }
        });

      }
    });
  }

  ngOnInit() {
    this.getCompnyDDList();
    this.getTreatmentPlan();
    this.getTreatmentBenefitPlan()
    this.getMasterTPASetupDDList().subscribe(response => console.log(response));
    if (this.urlText[3] == 'view') {
      this.toptitle = "Master TPA Company Setup Detail";
      this.isDisabled = false;
    }
  }

  getCompnyDDList() {
    this.ddlist.getCompanyDDList().subscribe(response => {
      if (response.success) {
        this.companyList = response.data;
      } else {
        this.snackBar.open(response.errorMessage, "Ok", {
          duration: 3000
        });
      }
    });
  }

  getMasterTPASetupDDList() {
    return this.ddlist.getMasterTPASetupDDList().pipe(
      tap(response => {
        if (response.success) {
          this.masterSetupList = response.data;
          console.log(this.masterSetupList)
        } else {
          this.snackBar.open(response.errorMessage, "Ok", {
            duration: 3000
          });
        }
      })
    )
    // this.ddlist.getMasterTPASetupDDList().subscribe(response => {
    //   if (response.success) {
    //     this.masterSetupList = response.data;
    //   } else {
    //     this.snackBar.open(response.errorMessage, "Ok", { duration: 3000 });
    //   }
    // });
  }

  generateNewMDTPAID() {
    this.masterTPA.generateNewMASTERTPAID().subscribe(response => {
      if (response.success) {
        this.masterTPAForm.patchValue({
          New_Master_TPA_ID: this.masterTPAForm.get('Master_TPA_ID').value + '-' + response.data
        })
      } else {
        this.snackBar.open(response.errorMessage, "Ok", {
          duration: 3000
        });
      }
    })
  }

  addMasterTPASetup() {
    if (this.masterTPAForm.valid) {
      this.masterTPA.addMasterTPA(this.masterTPAForm.getRawValue()).subscribe(response => {
        if (response.success) {
          this.snackBar.open('Master TPA Company Setup Saved Successfully', 'OK', {
            duration: 3000
          });
          this.masterTPAForm.reset();
          this.router.navigate(['/dataManager/master-tpa']);
        } else {
          this.snackBar.open(response.errorMessage, 'OK', {
            duration: 3000
          });
        }
      });
    } else {
      this.validateAllFormFields(this.masterTPAForm);
      this.snackBar.open('Form cannot Invalid', 'OK', {
        duration: 3000
      });
    }
  }

  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach((field) => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({
          onlySelf: true
        });
      } else if (control instanceof FormGroup) { this.validateAllFormFields(control); }
    });
  }

  getTreatmentPlan() {
    this.treatment.getTreatmentPlan(0).subscribe(response => {
      if (response.success) {
        this.treatmentPlanList = response.data;
      } else {
        this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 })
      }
    })
  }

  onMasterTPASelect(event) {
    this.masterTPAForm.get('New_Master_TPA_ID').patchValue(event.value)
  }

  getTreatmentBenefitPlan() {
    this.tpaBenefits.getTPABenefits(0).subscribe(response => {
      if (response.success) {
        this.benefitPlanList = response.data
      } else {
        this.snackBar.open(response.errorMessage, 'OK', { duration: 3000 });
      }
    })
  }


}