import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MasterTPAProfileRoutingModule } from './master-tpa-profile-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    MasterTPAProfileRoutingModule
  ]
})
export class MasterTPAProfileModule { }
