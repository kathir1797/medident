import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MasterTPAProfileComponent } from './master-tpa-profile.component';

describe('MasterTPAProfileComponent', () => {
  let component: MasterTPAProfileComponent;
  let fixture: ComponentFixture<MasterTPAProfileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MasterTPAProfileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MasterTPAProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
