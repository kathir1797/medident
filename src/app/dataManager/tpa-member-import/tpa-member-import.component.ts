import { tap } from 'rxjs/operators';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Component, OnInit, ViewChild } from '@angular/core';
import * as XLSX from 'xlsx';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { DatePipe } from '@angular/common';
import { tpaMemberImport } from 'app/models/dataManager';
import { deserialize } from "serializer.ts/Serializer";
import { Router } from '@angular/router';
import { DdlistService } from 'app/services/config/ddlist.service';
import { TpamemberService } from 'app/services/dataManager/tpamember.service';

@Component({
  selector: 'app-tpa-member-import',
  templateUrl: './tpa-member-import.component.html',
  styleUrls: ['./tpa-member-import.component.css']
})
export class TpaMemberImportComponent implements OnInit {

  excelHeaders: string[] =
    [
      "No",
      "Action",
      "TPA_MASTER",
      "StaffCode",
      "Name",
      "IC",
      "Relationship",
      "Gender",
      "Ref",
      "DOB",
      "Race",
      "Nationality",
      "EffectiveDate",
      "ExpiryDate",
      "Plan",
      "Category",
      "IndividualLimit",
      "FamilyLimit",
      "SharedLimit",
      "Mobile",
      "Phone",
      "Email",
      "Maritalstatus",
      "PrincipalAddress",
      "City",
      "State",
      "Postcode"
    ]

  toptitle: string = 'TPA Member Import';
  fileName: any;
  data: [][];
  displayedColumns = [];
  dataSource = new MatTableDataSource<any>();
  length: number
  tpaMemberImportRequest = new tpaMemberImport;
  obj = localStorage.getItem('currentuser');
  batchNumber: string;
  statusList = new Array();
  status_id: any = 1;
  referenceNumber: number = 0;

  //Style Bindings
  actionundefined: boolean = false;
  icUndefined: boolean = false;
  tpaMasterUndefined: boolean = false;
  genderOthers: boolean = false;
  relationshipOthers: boolean = false;
  raceOthers: boolean = false;
  Status: string;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;


  constructor(private router: Router, private snackbar: MatSnackBar, private datepipe: DatePipe, private tpa: TpamemberService,
    private ddlist: DdlistService) { }

  ngOnInit(): void {
    this.getStatusDDLists();
  }

  onFileChange(evt: any) {
    this.fileName = evt.target.files[0].name;

    const target: DataTransfer = <DataTransfer>(evt.target);

    console.log(evt.target.files[0].name)

    if (target.files.length !== 1) this.snackbar.open('Cannot use multiple files', 'Cancel', { duration: 3000 });

    const reader: FileReader = new FileReader();

    reader.onload = (e: any) => {
      const bstr: string = e.target.result;

      const wb: XLSX.WorkBook = XLSX.read(bstr, { type: 'binary', cellText: false, cellDates: true });

      const wsname: string = wb.SheetNames[0];

      const ws: XLSX.WorkSheet = wb.Sheets[wsname];

      this.data =
        (
          XLSX.utils.sheet_to_json
            (
              ws,
              {
                header: this.excelHeaders, raw: false, dateNF: 'dd/mm/yyyy'
              }
            )
        );

      for (var i = 1; i < this.data.length; i++) {
        console.log(`MaritalStatus:${this.data[i]['Maritalstatus']}`)
        console.log(i,'->>>>',`Action:${this.data[i]['Action']}`, `ICNumber:${this.data[i]['IC']}`)
        if (this.data[i]['Action'] != undefined || (this.data[i]['Action'] != 'INSERT' && this.data[i]['Action'] != 'UPDATE' && this.data[i]['Action'] != 'DISABLE')) {
          this.data[i]['actionundefined'] = false
          this.data[i]['readStatus'] = 'Pending'
        } else {
          this.data[i]['actionundefined'] = true
        }
        if (this.data[i]['IC'] == '' || this.data[i]['IC'] == null) {
          this.data[i]['icUndefined'] = true
        } else {
          this.data[i]['icUndefined'] = false
        }
        if (this.data[i]['TPA_MASTER'] == '' || this.data[i]['TPA_MASTER'] == null) {
          this.data[i]['tpaMasterUndefined'] = true
        } else {
          this.data[i]['tpaMasterUndefined'] = false
        }
        this.data[i]['referenceNumber'] = this.referenceNumber
        if (this.data[i]['Relationship'] === 'P' && this.data[i]['Action'] != 'UPDATE' && this.data[i]['Action'] != 'DISABLE') {
          this.data[i]['referenceNumber'] = this.data[i]['referenceNumber'] + 1
          this.referenceNumber += 1;
        } else {
          this.data[i]['referenceNumber'] = 1
        }
        this.data[i]['Race'] != 'M' && this.data[i]['Race'] != 'I' && this.data[i]['Race'] != 'C' ? this.data[i]['raceOthers'] = true : this.data[i]['Race'];
        this.data[i]['Gender'] != 'M' && this.data[i]['Gender'] != 'F' ? this.data[i]['genderOthers'] = true : this.data[i]['Gender'];
        this.data[i]['Nationality'] != 'M' ? 'O' : this.data[i]['Nationality'];
        this.data[i]['Relationship'] != 'P' && this.data[i]['Relationship'] != 'S' && this.data[i]['Relationship'] != 'C' ? this.data[i]['relationshipOthers'] = true : this.data[i]['Relationship'];
      }
    };
    reader.readAsBinaryString(target.files[0]);
    this.generateBatchNo();
  }


  saveTPAMemberData() {

    for (var i = 1; i < this.data.length; i++) {
      console.log(`PrincipalAddress:${this.data[i]['PrincipalAddress']}`)
      this.tpaMemberImportRequest.TPA_Member_Import_ID = 0;
      this.tpaMemberImportRequest.Batch_Number = this.batchNumber
      this.tpaMemberImportRequest.User_ID = JSON.parse(this.obj).User_ID;
      this.tpaMemberImportRequest.TPA_Import_Status_ID = this.status_id;
      this.tpaMemberImportRequest.Treatment_Category_ID = 0;
      this.tpaMemberImportRequest.Treatment_Plan_ID = 0;
      this.tpaMemberImportRequest.Reference_Number = this.data[i]['referenceNumber'];
      this.tpaMemberImportRequest.Action_Status = this.data[i]['Action'];
      this.tpaMemberImportRequest.Mobile_Number = this.data[i]['Mobile'];
      this.tpaMemberImportRequest.Phone_Number = this.data[i]['Phone'];
      this.tpaMemberImportRequest.Email_Address = this.data[i]['Email'];
      this.tpaMemberImportRequest.Marital_Status = this.data[i]['Maritalstatus'];
      this.tpaMemberImportRequest.Principal_Address = this.data[i]['PrincipalAddress'];
      this.tpaMemberImportRequest.Principal_City = this.data[i]['City'];
      this.tpaMemberImportRequest.Principal_State = this.data[i]['State'];
      this.tpaMemberImportRequest.Principal_PostCode = this.data[i]['Postcode']
      this.tpaMemberImportRequest.Remarks = '';
      console.log(this.tpaMemberImportRequest)
      this.tpa.saveTPAMemberImport(Object.assign(this.tpaMemberImportRequest, this.data[i])).subscribe(response => {
        if (response.success) {
          this.snackbar.open('TPA Member Imported Successfully', 'OK', { duration: 3000 })
          console.log(`Index:${i}`, `Length:${this.data.length}`)
          if (i == this.data.length) {
            this.updateBatchNumber(this.batchNumber).subscribe(response => { })
           
          }
        } else {
          this.snackbar.open(response.errorMessage, 'OK', { duration: 3000 })
        }
      });
    }
  }

  // Batch Number Generation
  generateBatchNo() {
    this.tpa.generateBatchTPAMemberImport().subscribe(response => {
      if (response.success) {
        this.batchNumber = response.data;
        localStorage.setItem('TPAImportBatch', JSON.stringify({ Batch_Number: this.batchNumber }))
      } else {
        this.snackbar.open(response.errorMessage, 'OK', { duration: 3000 })
      }
    });
  }

  //Status DropDown
  getStatusDDLists() {
    this.ddlist.getStatusDDList().subscribe(response => {
      if (response.success) {
        this.statusList = response.data;
      } else {
        this.snackbar.open(response.errorMessage, 'ok', { duration: 3000 })
      }
    });
  }

  //dummy Function
  replaceKeys(object) {
    Object.keys(object).forEach(function (key) {
      var newKey = key.replace(/\s+/g, '');
      if (object[key] && typeof object[key] === 'object') {
        this.replaceKeys(object[key]);
      }
      if (key !== newKey) {
        object[newKey] = object[key];
        delete object[key];
      }
    });
  }

  public trimObj(obj) {
    if (!Array.isArray(obj) && typeof obj != 'object') return obj;
    return Object.keys(obj).reduce(function (acc, key) {
      acc[key.trim()] = typeof obj[key] == 'string' ? obj[key].trim() : this.trimObj(obj[key]);
      return acc;
    }, Array.isArray(obj) ? [] : {});
  }

  updateBatchNumber(batchNumber) {
    return this.tpa.updateTPAMemberImportBatchNumber(batchNumber).pipe(
      tap(response => {
        if (response.success) {
          this.snackbar.open('Batch Number Updated Successfully', 'OK', { duration: 3000 });
          this.router.navigate(['/dataManager/tpamember/import/list']);
        } else {
          this.snackbar.open(response.errorMessage, 'ok', { duration: 3000 })
        }
      })
    );
  }

}
