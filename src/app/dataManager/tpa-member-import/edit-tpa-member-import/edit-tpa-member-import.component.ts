import { AlertDialogComponent } from './../../../dialog/alert-dialog/alert-dialog.component';
import { MatCheckboxChange } from '@angular/material/checkbox';
import { ActivatedRoute, Router } from '@angular/router';
import { tpaMemberImport } from './../../../models/dataManager';
import { Component, OnInit } from '@angular/core';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { treatmentCategory } from 'app/models/treatmentManager';
import { DdlistService } from 'app/services/config/ddlist.service';
import { TpamemberService } from 'app/services/dataManager/tpamember.service';
import { TreatmentCategoryService } from 'app/services/treatmentManager/treatment-category.service';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-edit-tpa-member-import',
  templateUrl: './edit-tpa-member-import.component.html',
  styleUrls: ['./edit-tpa-member-import.component.css']
})
export class EditTpaMemberImportComponent implements OnInit {

  tpaMemberImportRequest: tpaMemberImport = new tpaMemberImport();
  tpamemberData = new Array();
  statusList = new Array();
  treatmentPlanList = new Array();
  treatmentCategoryList = new Array();
  treatmentCategoryRequest: treatmentCategory = new treatmentCategory();
  horizontalPosition: MatSnackBarHorizontalPosition = 'end';
  verticalPosition: MatSnackBarVerticalPosition = 'top';
  confirm_Checked: boolean = false

  constructor(private snackbar: MatSnackBar, private router: Router, private tpa: TpamemberService,
    private ddlist: DdlistService, private cat: TreatmentCategoryService, public dialog: MatDialog,
    private activated: ActivatedRoute) {
    this.tpaMemberImportRequest.User_ID = JSON.parse(localStorage.getItem('currentuser')).User_ID;
  }

  ngOnInit(): void {
    this.getTPAMemberImportList();
    this.getStatusDDLists();
    this.treatmentCategoryRequest.Treatment_Category_ID = 0;
    this.getTreatmentCategory();
    this.getTreatmentPlan();
  }

  saveTPAMemberData(data) {
    this.tpa.getTPAMemberImportList(data.Batch_Number).subscribe(response => {
      if (response.success) {
        this.tpamemberData = response.data;
        console.log('Confirm', this.confirm_Checked)
        const apiData = this.tpamemberData
          .filter(value => value.IC === data.IC_Number)
          .map(() => ({
            Batch_Number: data.Batch_Number,
            Category: this.tpaMemberImportRequest.Category,
            DOB: data.DOB,
            EffectiveDate: data.EffectiveDate,
            ExpiryDate: data.ExpiryDate,
            FamilyLimit: data.FamilyLimit,
            Gender: data.Gender,
            IC: data.IC_Number,
            IndividualLimit: data.IndividualLimit,
            Name: data.Staff_Name,
            Nationality: data.Nationality,
            No: data.No,
            Plan: this.tpaMemberImportRequest.TPA_Plan,
            Race: data.Race,
            Ref: data.Ref_Date,
            Relationship: data.Relationship,
            SharedLimit: data.SharedLimit,
            StaffCode: data.StaffCode,
            TPA_Import_Status_ID: !this.confirm_Checked ? data.TPA_Import_Status_ID : 2,
            TPA_MASTER: data.TPA_MASTER,
            TPA_Member_Import_ID: data.TPA_Member_Import_ID,
            Treatment_Category: this.tpaMemberImportRequest.Category,
            Treatment_Plan_ID: this.tpaMemberImportRequest.Treatment_Plan_ID,
            Treatment_Category_ID: this.tpaMemberImportRequest.Treatment_Category_ID,
            Reference_Number: this.tpaMemberImportRequest.Reference_Number,
            Remarks: data.Remarks,
            Action_Status: data.Action_Status,
            Mobile_Number: data.Mobile_Number,
            Phone_Number: data.Phone_Number,
            Email_Address: data.Email_Address,
            Marital_Status: data.Marital_Status,
            Principal_Address: data.Principal_Address,
            Principal_City: data.Principal_City,
            Principal_State: data.Principal_State,
            Principal_PostCode: data.Principal_PostCode,
          }));
        console.log(typeof apiData[0].Treatment_Plan_ID, apiData[0].Treatment_Category_ID)
        if (apiData[0].Treatment_Plan_ID == 0 || apiData[0].Treatment_Category_ID == 0) {
          this.dialog.open(AlertDialogComponent)
        }
        this.tpa.saveTPAMemberImport(apiData[0]).subscribe(response1 => {
          if (response1.success) {
            this.router.navigate(['/dataManager/tpamember/import/list'])
            this.snackbar.open('TPA Member Imported Successfully', 'OK', { duration: 3000 })
          } else {
            this.snackbar.open(response1.errorMessage, 'OK', { duration: 3000 })
          }
        });
      } else {
        this.snackbar.open(response.errorMessage, 'ok', { duration: 3000 })
      }
    });
  }

  getTPAMemberImportList() {
    this.activated.queryParams.subscribe(params => {
      this.tpa.getTPAMemberImportList(params.Batch_Number).subscribe(response => {
        if (response.success) {
          this.tpamemberData = response.data;
          this.tpamemberData = this.tpamemberData.filter(value => {
            if (value.IC === params.IC_Number) {
              console.log(value)
              this.confirm_Checked = value.TPA_Import_Status_ID === 2 ? true : false;
              this.tpaMemberImportRequest.Treatment_Plan_ID = value.Treatment_Plan_ID
              this.tpaMemberImportRequest.Treatment_Category_ID = value.Treatment_Category_ID
              this.tpaMemberImportRequest.IC_Number = value.IC
              this.tpaMemberImportRequest.Staff_Code = value.StaffCode
              this.tpaMemberImportRequest.Staff_Name = value.Name
              this.tpaMemberImportRequest.Ref_Date = value.Ref;
              this.tpaMemberImportRequest.Date_Of_Birth = value.DOB
              this.tpaMemberImportRequest.Effective_Date = value.EffectiveDate
              this.tpaMemberImportRequest.Exp_Date = value.ExpiryDate
              this.tpaMemberImportRequest.Individual_Limit = value.IndividualLimit
              this.tpaMemberImportRequest.Family_Limit = value.FamilyLimit
              this.tpaMemberImportRequest.Shared_limit = value.SharedLimit
              this.tpaMemberImportRequest.Category = value.Treatment_Category
              this.tpaMemberImportRequest.TPA_Plan = value.Plan
              this.tpaMemberImportRequest.TPA_Import_Status_ID = value.TPA_Import_Status_ID
              this.tpaMemberImportRequest.Relationship = value.Relationship
              this.tpaMemberImportRequest.Nationality = value.Nationality
              this.tpaMemberImportRequest.Gender = value.Gender
              this.tpaMemberImportRequest.Reference_Number = value.Reference_Number
              this.tpaMemberImportRequest.Remarks = value.Remarks
              this.tpaMemberImportRequest.Action_Status = value.Action_Status
              this.tpaMemberImportRequest.Email_Address = value.Email_Address
              this.tpaMemberImportRequest.Marital_Status = value.Marital_Status
              this.tpaMemberImportRequest.Phone_Number = value.Phone_Number
              this.tpaMemberImportRequest.Principal_Address = value.Principal_Address
              this.tpaMemberImportRequest.Principal_PostCode = value.Principal_PostCode
              this.tpaMemberImportRequest.Mobile_Number = value.Mobile_Number
              this.tpaMemberImportRequest.Principal_State = value.Principal_State
              this.tpaMemberImportRequest.Principal_City = value.Principal_City
              Object.assign(this.tpaMemberImportRequest, value)
            } else {
              return
            }
          });
        } else {
          this.snackbar.open(response.errorMessage, 'ok', { duration: 3000 })
        }
      });
    });

  }

  getStatusDDLists() {
    this.ddlist.getStatusDDList().subscribe(response => {
      if (response.success) {
        this.statusList = response.data;
        this.statusList = this.statusList.filter(() => {
          return this.statusList[0].Status_ID === this.tpaMemberImportRequest.TPA_Import_Status_ID
        })
      } else {
        this.snackbar.open(response.errorMessage, 'ok', { duration: 3000 })
      }
    });
  }

  getTreatmentPlan() {
    this.tpa.getTreatmentPlan(0).subscribe(response => {
      if (response.success) {
        this.treatmentPlanList = response.data
      } else {
        this.snackbar.open(response.errorMessage, 'ok', { duration: 3000 })
      }
    })
  }

  getTreatmentCategory() {
    this.cat.getTreatmentCategory(this.treatmentCategoryRequest).subscribe(response => {
      if (response.success) {
        this.treatmentCategoryList = response.data
      } else {
        this.snackbar.open(response.errorMessage, 'ok', { duration: 3000 })
      }
    })
  }

  onPlanSelect(event) {
    // this.tpaMemberImportRequest.Treatment_Plan_ID = event.value.Treatment_Plan_ID
    this.treatmentPlanList.filter(value => {
      if (value.Treatment_Plan_ID === event.value) {
        this.tpaMemberImportRequest.TPA_Plan = value.Treatment_Plan_Name
      }
    })
  }

  onCategorySelect(event) {
    console.log(event)
    // this.tpaMemberImportRequest.Treatment_Category_ID = event.value.Treatment_Category_ID
    this.treatmentCategoryList.filter(value => {
      if (value.Treatment_Category_ID === event.value) {
        this.tpaMemberImportRequest.Category = value.Treatment_Category_Code
      }
    })
  }

  OnConfirmation(e: MatCheckboxChange) {
    console.log(e)
    this.confirm_Checked = e.checked ? true : false
  }

}
