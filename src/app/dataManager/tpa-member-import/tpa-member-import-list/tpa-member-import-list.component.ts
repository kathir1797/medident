import { MatCheckboxChange } from '@angular/material/checkbox';

import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { Component, OnInit, ViewChild, AfterViewInit, Input } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { TpamemberService } from 'app/services/dataManager/tpamember.service';
import { SelectionModel } from '@angular/cdk/collections';
import { tap } from 'rxjs/operators';
import { tpaMemberImport } from 'app/models/dataManager';

@Component({
  selector: 'app-tpa-member-import-list',
  templateUrl: './tpa-member-import-list.component.html',
  styleUrls: ['./tpa-member-import-list.component.css']
})
export class TpaMemberImportListComponent implements OnInit, AfterViewInit {

  @ViewChild(MatPaginator) paginator: MatPaginator;
  dataSource = new MatTableDataSource<any>();
  length: number;
  statusName: string;
  batchlist = new Array();
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  selection = new SelectionModel<any>(true, []);

  tpaMemberImportRequest: tpaMemberImport = new tpaMemberImport();

  tpaMemberImportColumn =
    [
      "select",
      "Batch_Number",
      "TPA_MASTER",
      "Relationship",
      "Plan",
      "Name",
      "IC",
      "Remarks",
      // "EffectiveDate",
      "statusName",
      "Action"
    ]

  constructor(private snackBar: MatSnackBar, private tpa: TpamemberService) { }

  ngOnInit(): void {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    this.getTPAMemberImportList('').subscribe(response => { });
    this.getBatchnumberDD();
  }

  // ngAfterViewInit() {
  //   this.dataSource.paginator = this.paginator;
  //   this.dataSource.sort = this.sort;
  // }

  getTPAMemberImportList(batchNumber) {
    return this.tpa.getTPAMemberImportList(batchNumber)
      .pipe
      (
        tap
          (response => {
            if (response.success) {
              this.dataSource.data = response.data;
              for (var i = 0; i < this.dataSource.data.length; i++) {
                if (this.dataSource.data[i].TPA_Import_Status_ID == 1) {
                  this.dataSource.data[i].statusName = 'Pending'
                } else if (this.dataSource.data[i].TPA_Import_Status_ID == 2) {
                  this.dataSource.data[i].statusName = 'Confirmed'
                } else {
                  this.dataSource.data[i].statusName = 'Uploaded'
                }
              }
              this.dataSource.paginator = this.paginator;
              this.dataSource.sort = this.sort;
            } else {
              this.dataSource.data = []
              this.dataSource.paginator = this.paginator;
              this.dataSource.sort = this.sort;
              this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 })
            }
          }
          )
      )
  }

  inserTPAMemberImport() {
    this.tpa.inserTPAMemberImport().subscribe(response => {
      if (response.success) {
        this.getTPAMemberImportList('').subscribe(response => { });
        this.snackBar.open('TPA Member Inserted Successfully', 'ok', { duration: 3000 })
      } else {
        this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 })
      }
    })
  }

  applyFilter(filterString: string) {
    // console.log(`FilterString:${filterString}`);
    if (filterString === 'All') {
      this.dataSource.filter = ''
    } else {
      this.dataSource.filter = filterString.trim().toLowerCase()
    }
  }

  getBatchnumberDD() {
    this.tpa.getBatchnumberTPAMemberImport().subscribe(response => {
      if (response.success) {
        this.batchlist = response.data;
      } else {
        this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 })
      }
    });
  }



  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => {
        this.selection.select(row)
      });
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: any): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.position + 1}`;
  }

  toggleCheckbox(event: MatCheckboxChange, row: any) {
    if (event.checked) {
      this.selection.toggle(row)
      console.log(this.selection.hasValue(), row)
    } else {
      this.selection.deselect(row)
      console.log(this.selection.hasValue(), row)
    }
  }

  updateAll(value) {
    console.log(`Length of APIDATA:${value.length}`)
    for (var i = 0; i < value.length; i++) {
      this.tpaMemberImportRequest.Category = value[i].Treatment_Category
      value[i].TPA_Import_Status_ID = 2
      console.log(Object.assign(this.tpaMemberImportRequest, value[i]))
      this.tpa.saveTPAMemberImport(Object.assign(this.tpaMemberImportRequest, value[i])).subscribe(response => {
        if (response.success) {
          this.snackBar.open('TPA Member Confirmed Successfully', 'OK', { duration: 3000 })
          if (i == value.length - 1) {
            this.getTPAMemberImportList('').subscribe(response => { });
          }
        } else {
          this.snackBar.open(response.errorMessage, 'OK', { duration: 3000 })
        }
      });
    }

  }


  updateBatchNumber(batchNumber) {
    return this.tpa.updateTPAMemberImportBatchNumber(batchNumber).pipe(
      tap(response => {
        if (response.success) {
          this.snackBar.open('Batch Number Updated Successfully', 'OK', { duration: 3000 })
        } else {
          this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 })
        }
      })
    );
  }


  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

}
