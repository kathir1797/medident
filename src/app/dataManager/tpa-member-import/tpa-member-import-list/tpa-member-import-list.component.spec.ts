import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TpaMemberImportListComponent } from './tpa-member-import-list.component';

describe('TpaMemberImportListComponent', () => {
  let component: TpaMemberImportListComponent;
  let fixture: ComponentFixture<TpaMemberImportListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TpaMemberImportListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TpaMemberImportListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
