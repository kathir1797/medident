import { doctorRequest } from './../../../models/dataManager';
import { Subscription } from 'rxjs/Subscription';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router, ActivatedRoute } from '@angular/router';
import { DdlistService } from 'app/services/config/ddlist.service';
import { DoctorService } from 'app/services/dataManager/doctor.service';

@Component({
  selector: 'app-add-doctor',
  templateUrl: './add-doctor.component.html',
  styleUrls: ['./add-doctor.component.css']
})
export class AddDoctorComponent implements OnInit {

  toptitle: string = 'Add Doctor';
  doctorForm: FormGroup;
  btntext: string = 'Save';
  urlText: any;
  isDisabled: boolean = true;
  obj = localStorage.getItem('currentuser');
  statusList = new Array();
  NationalityList = new Array();
  stateList = new Array();
  cityList = new Array();
  doctorData = new doctorRequest();

  constructor(private fb: FormBuilder, private ddlist: DdlistService, private snackBar: MatSnackBar, private router: Router,
    private activatedRoute: ActivatedRoute, private doctorService: DoctorService) {
    const url = this.router.url;
    this.urlText = url.split('/');
    this.initializeDoctorForm();
    this.activatedRoute.params.subscribe((param) => {
      if (this.urlText[3] != 'add') {
        this.toptitle = 'Edit Doctor';
        this.btntext = 'Update';
        this.doctorData.Doctor_ID = param['Doctor_ID']
        this.getDoctorById(this.doctorData);
      }
      if (this.urlText[3] === 'view') {
        this.isDisabled = false;
        this.toptitle = 'View Doctor Detail';
      }
    });
  }

  ngOnInit(): void {
    this.getNationalityDDList();
    this.getStateDDList();
    this.getCityDDList();
    this.getStatusDDLists();
  }

  initializeDoctorForm() {
    this.doctorForm = this.fb.group({
      Doctor_ID: new FormControl({ value: 0, disabled: this.urlText[3] == 'view' ? true : false }),
      Doctor_MDC_Code: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      Doctor_Name: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      Doctor_IC_Number: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      Doctor_Nationality_ID: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      Doctor_Address: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      Doctor_City: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      Doctor_State_ID: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      Doctor_TelephoneNo: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      Doctor_HandphoneNo: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      Specialist: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      Doctor_Status_ID: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      Doctor_APC_Code: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      Expiry_Date: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      User_ID: JSON.parse(this.obj).User_ID
    })
  }

  getNationalityDDList() {
    this.ddlist.getNationalityDDList().subscribe(response => {
      if (response.success) {
        this.NationalityList = response.data;
      } else {
        this.snackBar.open(response.errorMessage, 'OK', { duration: 3000 }); (response.errorMessage, "Ok");
      }
    });
  }

  getStateDDList() {
    this.ddlist.getStateList().subscribe(response => {
      if (response.success) {
        this.stateList = response.data;
      } else {
        this.snackBar.open(response.errorMessage, 'OK', { duration: 3000 }); (response.errorMessage, "Ok");
      }
    });
  }

  getCityDDList() {
    this.ddlist.getCityList().subscribe(response => {
      if (response.success) {
        this.cityList = response.data;
      } else {
        this.snackBar.open(response.errorMessage, 'OK', { duration: 3000 }); (response.errorMessage, "Ok");
      }
    });
  }

  getStatusDDLists() {
    this.ddlist.getStatusDDList().subscribe(response => {
      if (response.success) {
        this.statusList = response.data;
      } else {
        this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 })
      }
    });
  }

  addDoctor() {
    if (this.doctorForm.valid) {
      this.doctorService.addDoctor(this.doctorForm.getRawValue()).subscribe(response => {
        if (response.success) {
          this.doctorForm.reset();
          this.snackBar.open('Doctor Saved Successfully', 'OK', { duration: 3000 });
          this.router.navigate(['/dataManager/doctor']);
        } else {
          this.snackBar.open(response.errorMessage, 'OK', { duration: 3000 });
        }
      });
    } else {
      this.snackBar.open('Form cannot be Invalid', 'OK', { duration: 3000 });
    }
  }


  getDoctorById(doctorId) {
    this.doctorService.getDoctor(doctorId).subscribe(response => {
      if (response.success) {
        this.doctorForm.patchValue({
          Doctor_ID: response.data[0].Doctor_ID,
          Doctor_MDC_Code: response.data[0].Doctor_MDC_Code,
          Doctor_Name: response.data[0].Doctor_Name,
          Doctor_IC_Number: response.data[0].Doctor_IC_Number,
          Doctor_Nationality_ID: response.data[0].Doctor_Nationality_ID,
          Doctor_Address: response.data[0].Doctor_Address,
          Doctor_City: response.data[0].Doctor_City,
          Doctor_State_ID: response.data[0].Doctor_State_ID,
          Doctor_TelephoneNo: response.data[0].Doctor_TelephoneNo,
          Doctor_HandphoneNo: response.data[0].Doctor_HandphoneNo,
          Specialist: response.data[0].Specialist,
          Doctor_Status_ID: response.data[0].Doctor_Status_ID,
          Doctor_APC_Code: response.data[0].Doctor_APC_Code,
          Expiry_Date: response.data[0].Expiry_Date,
          User_ID: JSON.parse(this.obj).User_ID
        });
      } else {
        this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 });
      }
    });
  }

}
