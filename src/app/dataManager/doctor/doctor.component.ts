
import { doctorRequest } from './../../models/dataManager';
import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { MatSnackBar } from '@angular/material/snack-bar';
import { DoctorService } from 'app/services/dataManager/doctor.service';

@Component({
  selector: 'app-doctor',
  templateUrl: './doctor.component.html',
  styleUrls: ['./doctor.component.css']
})
export class DoctorComponent implements OnInit, AfterViewInit {


  dataSource = new MatTableDataSource<any>();
  length: number;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  displayedColumns = ['Doctor_Name', 'State_Name', 'Specialist', 'Status_Name', 'Action'];

  doctorData: doctorRequest = new doctorRequest();

  constructor(private doctorService: DoctorService, private snackBar: MatSnackBar) { }

  ngOnInit(): void {
    this.doctorData.Doctor_ID = 0;
    this.getDoctor();
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  getDoctor() {
    this.doctorService.getDoctor(this.doctorData).subscribe(response => {
      if (response.success) {
        this.dataSource.data = response.data;
      } else {
        this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 });
      }
    });
  }

}
