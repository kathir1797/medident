import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router, ActivatedRoute } from '@angular/router';
import { specialistEquipment } from 'app/models/dataManager';
import { DdlistService } from 'app/services/config/ddlist.service';
import { SpecialistEquipmentService } from 'app/services/treatmentManager/specialist-equipment.service';

@Component({
  selector: 'app-add-specialist-equipment',
  templateUrl: './add-specialist-equipment.component.html',
  styleUrls: ['./add-specialist-equipment.component.css']
})
export class AddSpecialistEquipmentComponent implements OnInit {

  specialistEquipmentForm: FormGroup;
  urlText: any;
  obj = localStorage.getItem('currentuser');
  statusList = new Array();
  toptitle: string = 'Add Specialist Equipment';
  btntext: string = 'Save';
  isDisabled: boolean = true;

  specialistEquipmentRequest: specialistEquipment = new specialistEquipment();

  constructor(private fb: FormBuilder, private router: Router, private ddlist: DdlistService,
    private snackBar: MatSnackBar, private activated: ActivatedRoute, private sp: SpecialistEquipmentService) {
    const url = this.router.url
    this.urlText = url.split("/");
    this.initializeForm();
    this.activated.params.subscribe(param => {
      if (this.urlText[3] != 'add') {
        this.toptitle = 'Update Specialist Equipment';
        this.btntext = 'Update'
        this.specialistEquipmentRequest.Special_Equipment_ID = param.Special_Equipment_ID
        this.getSpecialistEquipment(this.specialistEquipmentRequest);
      }
      if (this.urlText[3] === 'view') {
        this.toptitle = 'View Specialist Equipment'
        this.isDisabled = false;
      }
    })
  }

  ngOnInit(): void {
    this.getStatusDDLists()
  }

  initializeForm() {
    this.specialistEquipmentForm = this.fb.group({
      Special_Equipment_ID: 0,
      Equipment_Code: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      Service_Code: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      Remarks: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      Equipment_Status_ID: new FormControl({ value: 1, disabled: this.urlText[3] == 'view' ? true : false }),
      User_ID: JSON.parse(this.obj).User_ID
    })
  }

  getStatusDDLists() {
    this.ddlist.getStatusDDList().subscribe(response => {
      if (response.success) {
        this.statusList = response.data;
      } else {
        this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 })
      }
    });
  }

  addSpecialistEquipmentCode() {
    this.sp.addSpecialistEquipmentCode(this.specialistEquipmentForm.getRawValue()).subscribe(response => {
      if (response.success) {
        this.specialistEquipmentForm.reset();
        this.router.navigate(['/dataManager/specialistEquipment']);
        this.snackBar.open('Specialist Equipment Saved Successfully', 'ok', { duration: 3000 })
      } else {
        this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 })
      }
    });
  }

  getSpecialistEquipment(spRequest) {
    this.sp.getSpecialistEquipementCode(spRequest).subscribe(response => {
      if (response.success) {
        this.specialistEquipmentForm.patchValue({
          Special_Equipment_ID: response.data[0].Special_Equipment_ID,
          Equipment_Code: response.data[0].Equipment_Code,
          Service_Code: response.data[0].Service_Code,
          Remarks: response.data[0].Remarks,
          Equipment_Status_ID: response.data[0].Equipment_Status_ID,
          User_ID: JSON.parse(this.obj).User_ID
        })
      } else {
        this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 })
      }
    })
  }

}
