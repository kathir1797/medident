import { SpecialistEquipmentService } from './../../services/treatmentManager/specialist-equipment.service';
import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute } from '@angular/router';
import { specialistEquipment } from 'app/models/dataManager';

@Component({
  selector: 'app-specialist-equipment',
  templateUrl: './specialist-equipment.component.html',
  styleUrls: ['./specialist-equipment.component.css']
})
export class SpecialistEquipmentComponent implements OnInit, AfterViewInit {

  @ViewChild(MatPaginator) paginator: MatPaginator;
  dataSource = new MatTableDataSource<any>();
  length: number
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  displayedColumns = ['Equipment_Code', 'Remarks', 'Service_Code', 'Status_Name', 'Action'];
  specialistEquipmentRequest: specialistEquipment = new specialistEquipment();

  constructor(private snackBar: MatSnackBar, private sp: SpecialistEquipmentService) { }

  ngOnInit(): void {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    this.specialistEquipmentRequest.Special_Equipment_ID = 0;
    this.getSpecialistEquipment();
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  getSpecialistEquipment() {
    this.sp.getSpecialistEquipementCode(this.specialistEquipmentRequest).subscribe(response => {
      if (response.success) {
        this.dataSource.data = response.data;
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      } else {
        this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 })
      }
    });
  }

}
