import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router, ActivatedRoute } from '@angular/router';
import { DdlistService } from 'app/services/config/ddlist.service';
import { ServiceTypeService } from 'app/services/dataManager/service-type.service';

@Component({
  selector: 'app-add-service-type',
  templateUrl: './add-service-type.component.html',
  styleUrls: ['./add-service-type.component.css']
})
export class AddServiceTypeComponent implements OnInit {

  urlText: any;
  obj = localStorage.getItem('currentuser');
  statusList = new Array();
  toptitle: string = 'Add Service Type';
  btntext: string = 'Save';
  isDisabled: boolean = true;
  serviceTypeForm: FormGroup

  constructor(private fb: FormBuilder, private router: Router, private ddlist: DdlistService,
    private snackBar: MatSnackBar, private activated: ActivatedRoute, private serviceTypeService: ServiceTypeService) {
    const url = this.router.url
    this.urlText = url.split("/");
    this.initializeForm();
    this.activated.params.subscribe(param => {
      if (this.urlText[3] != 'add') {
        this.toptitle = 'Update Service Type';
        this.btntext = 'Update'
        this.serviceTypeService.getServiceType(param.Service_Type_ID).subscribe(response => {
          if (response.success) {
            this.serviceTypeForm.patchValue(response.data[0]);
          } else {
            this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 })
          }
        })
      }
      if (this.urlText[3] == 'view') {
        this.toptitle = 'View Service Type'
        this.isDisabled = false;
      }
    })
  }

  ngOnInit(): void {
    this.getStatusDDLists();
  }

  initializeForm() {
    this.serviceTypeForm = this.fb.group({
      Service_Type_ID: 0,
      Service_Type_Name: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      Service_Type_Status_ID: new FormControl({ value: 1, disabled: this.urlText[3] == 'view' ? true : false }),
      User_ID: JSON.parse(this.obj).User_ID
    })
  }

  getStatusDDLists() {
    this.ddlist.getStatusDDList().subscribe(response => {
      if (response.success) {
        this.statusList = response.data;
      } else {
        this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 })
      }
    });
  }

  saveServiceType() {
    this.serviceTypeService.addServiceType(JSON.stringify(this.serviceTypeForm.value)).subscribe(response => {
      if (response.success) {
        this.serviceTypeForm.reset();
        this.router.navigate(['/dataManager/servicetype']);
        this.snackBar.open('Service Type Saved Successfully', 'ok', { duration: 3000 })
      } else {
        this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 })
      }
    })
  }

}
