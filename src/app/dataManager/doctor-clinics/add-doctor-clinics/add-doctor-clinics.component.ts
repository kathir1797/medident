import { DdlistService } from './../../../services/config/ddlist.service';
import { DoctorService } from './../../../services/dataManager/doctor.service';
import { ClinicsService } from './../../../services/dataManager/clinics.service';
import { tap } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { clinicsRequest, doctorClinicRequest, doctorRequest } from 'app/models/dataManager';
import { runInThisContext } from 'vm';

@Component({
  selector: 'app-add-doctor-clinics',
  templateUrl: './add-doctor-clinics.component.html',
  styleUrls: ['./add-doctor-clinics.component.css']
})
export class AddDoctorClinicsComponent implements OnInit {

  toptitle: string = 'Add Doctor Clinics';
  btntext: string = 'Save';
  isDisabled: boolean = true;
  doctorClinicsForm: FormGroup;
  obj = localStorage.getItem('currentuser');
  urlText: any;
  statusList = new Array();
  clinicsList = new Array();
  doctorClinicListData = new Array();
  doctorsList = new Array();
  selectionArray = new Array();
  clinicId: boolean = false;

  clinicsData: clinicsRequest = new clinicsRequest();
  doctorData: doctorRequest = new doctorRequest();
  doctorClinicRequest: doctorClinicRequest = new doctorClinicRequest();



  constructor(private fb: FormBuilder, private router: Router, private activatedRoute: ActivatedRoute, private ddlist: DdlistService,
    private snackBar: MatSnackBar, private clinics: ClinicsService, private doctorService: DoctorService) {
    const url = this.router.url;
    this.urlText = url.split('/');
    this.activatedRoute.params.subscribe((param) => {
      if (this.urlText[4] != 'add') {
        this.toptitle = 'Edit Doctor Clinic Data';
        this.btntext = 'Update';
        this.doctorClinicRequest.Doctor_Clinic_ID = param['Doctor_Clinic_ID'];
        this.doctorService.getDoctorClinicList(this.doctorClinicRequest).subscribe(response => {
          if (response.success) {
            this.doctorClinicListData = response.data;
            this.getClinics().subscribe(clinics => {
              if (clinics.success) {
                this.clinicsList = clinics.data;
                console.log(this.selectionArray)
                for (var i = 0; i < this.clinicsList.length; i++) {
                  if (this.clinicsList[i].Clinic_ID == this.doctorClinicListData[0].Clinic_ID) {
                    this.clinicsList[i].clinicId = true;
                    this.selectionArray.push(this.clinicsList[i]);
                    console.log(this.selectionArray)
                  } else {
                    this.clinicsList[i].clinicId = false
                  }
                }
                this.doctorClinicsForm.patchValue({
                  Doctor_Clinic_ID: response.data[0].Doctor_Clinic_ID,
                  Doctor_ID: response.data[0].Doctor_ID,
                  Effective_Date: response.data[0].Effective_Date,
                  Doctor_Clinic_Status_ID: response.data[0].Doctor_Clinic_Status_ID,
                  User_ID: JSON.parse(this.obj).User_ID
                });
              } else {
                this.snackBar.open(clinics.errorMessage, 'ok', { duration: 3000 });
              }
            });
          } else {
            this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 });
          }
        });
      }
      if (this.urlText[3] === 'view') {
        this.isDisabled = false;
        this.toptitle = 'View Doctor Clinic Detail';
      }
    });
    this.initializeDoctorClinicsForm();
  }

  ngOnInit(): void {
    this.clinicsData.Clinic_ID = 0;
    this.doctorData.Doctor_ID = 0;
    this.getStatusDDLists();
    this.getClinics().subscribe(response => { });
    this.getDoctors();
  }

  initializeDoctorClinicsForm() {
    this.doctorClinicsForm = this.fb.group({
      Doctor_Clinic_ID: 0,
      Doctor_ID: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      Clinic_ID: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      Effective_Date: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      Doctor_Clinic_Status_ID: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      User_ID: JSON.parse(this.obj).User_ID
    })
  }

  get formControls() {
    return this.doctorClinicsForm.controls;
  }

  getStatusDDLists() {
    this.ddlist.getStatusDDList().subscribe(response => {
      if (response.success) {
        this.statusList = response.data;
      } else {
        this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 })
      }
    });
  }

  getClinics() {
    return this.clinics.getClinicsList(this.clinicsData).pipe(
      tap(response => {
        if (response.success) {
          this.clinicsList = response.data;
          this.clinicsList = this.clinicsList.filter(value => {
            if (value.Status_ID == 1) {
              return value
            }
          })
        } else {
          this.snackBar.open(response.errorMessage, "Ok", { duration: 3000 });
        }
      })
    );
  }

  getDoctors() {
    this.doctorService.getDoctor(this.doctorData).subscribe(response => {
      if (response.success) {
        this.doctorsList = response.data;
        this.doctorsList = this.doctorsList.filter(value => {
          if (value.Doctor_Status_ID == 1) {
            return value
          }
        })
      } else {
        this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 });
      }
    });
  }

  onSelectionChange(event, clinicId) {
    if (event.checked) {
      this.selectionArray.push(clinicId);
      console.log(this.selectionArray)
    } else {
      this.selectionArray = this.selectionArray.filter((value) => value != clinicId)
    }
  }

  addDoctorClinic() {
    var eff_date = new Date(this.doctorClinicsForm.get('Effective_Date').value).toUTCString();
    for (var i = 0; i < this.selectionArray.length; i++) {
      this.doctorClinicsForm.get('Clinic_ID').patchValue(this.selectionArray[i].Clinic_ID != undefined ? this.selectionArray[i].Clinic_ID : null);
      this.doctorClinicsForm.get('Effective_Date').patchValue(eff_date);
      if (this.doctorClinicsForm.get('Doctor_ID').value != "" && this.doctorClinicsForm.get('Clinic_ID').value != "") {
        this.doctorService.addDoctorClinic(JSON.stringify(this.doctorClinicsForm.value)).subscribe(response => {
          if (response.success) {
            this.router.navigate(['/dataManager/doctor/clinics']);
            this.doctorClinicsForm.reset();
            this.snackBar.open('Doctor Clinic Saved Successfully', 'OK', { duration: 3000 });
          } else {
            this.snackBar.open(response.errorMessage, 'OK', { duration: 3000 })
          }
        });
      } else {
        this.snackBar.open('Select Doctor Name and Clinic_ID', 'OK', { duration: 3000 })
      }
    }
  }

  getDoctorClinicById(doctorClinic) {
    this.doctorService.getDoctorClinicList(doctorClinic).subscribe(response => {
      if (response.success) {
        this.clinicsData.Clinic_ID = 0
        this.doctorClinicsForm.patchValue({
          Doctor_Clinic_ID: response.data[0].Doctor_Clinic_ID,
          Doctor_ID: response.data[0].Doctor_ID,
          Effective_Date: response.data[0].Effective_Date,
          Doctor_Clinic_Status_ID: response.data[0].Doctor_Clinic_Status_ID,
          User_ID: JSON.parse(this.obj).User_ID
        });
        this.clinics.getClinicsList(this.clinicsData).subscribe(response1 => {
          if (response1.success) {
            this.clinicsList = response1.data;
            this.clinicId = this.clinicsList.every(t => {
              return t.Clinic_ID == response.data[0].Clinic_ID
            });
          }
        });
      } else {
        this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 });
      }
    });
  }
}