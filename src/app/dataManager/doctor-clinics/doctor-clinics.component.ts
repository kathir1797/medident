
import { MatSnackBar } from '@angular/material/snack-bar';
import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { doctorClinicRequest } from 'app/models/dataManager';
import { DoctorService } from 'app/services/dataManager/doctor.service';

@Component({
  selector: 'app-doctor-clinics',
  templateUrl: './doctor-clinics.component.html',
  styleUrls: ['./doctor-clinics.component.css']
})
export class DoctorClinicsComponent implements OnInit, AfterViewInit {

  dataSource = new MatTableDataSource<any>();
  length: number;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  displayedColumns = ['Doctor_Name', 'Clinic_Name', 'Effective_Date', 'Status_Name', 'Action'];

  doctorClinicRequest: doctorClinicRequest = new doctorClinicRequest();

  constructor(private snackBar: MatSnackBar, private doctorService: DoctorService) { }

  ngOnInit(): void {
    this.doctorClinicRequest.Doctor_Clinic_ID = 0;
    this.getClinicDoctorList();
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  getClinicDoctorList() {
    this.doctorService.getDoctorClinicList(this.doctorClinicRequest).subscribe(response => {
      if (response.success) {
        this.dataSource.data = response.data;
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      } else {
        this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 });
      }
    })
  }

}
