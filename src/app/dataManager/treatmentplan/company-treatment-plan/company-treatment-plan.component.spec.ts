import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompanyTreatmentPlanComponent } from './company-treatment-plan.component';

describe('CompanyTreatmentPlanComponent', () => {
  let component: CompanyTreatmentPlanComponent;
  let fixture: ComponentFixture<CompanyTreatmentPlanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompanyTreatmentPlanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompanyTreatmentPlanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
