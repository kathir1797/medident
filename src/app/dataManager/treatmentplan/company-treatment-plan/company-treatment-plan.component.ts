import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { companyTreatmentPlanRequest } from 'app/models/dataManager';
import { CompanyTreatmentPlanService } from 'app/services/treatmentplan/company-treatment-plan.service';

@Component({
  selector: 'app-company-treatment-plan',
  templateUrl: './company-treatment-plan.component.html',
  styleUrls: ['./company-treatment-plan.component.css']
})
export class CompanyTreatmentPlanComponent implements OnInit, AfterViewInit {

  @ViewChild(MatPaginator) paginator: MatPaginator;
  dataSource = new MatTableDataSource<any>();
  length: number
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  companyTreatmentPlanRequest: companyTreatmentPlanRequest = new companyTreatmentPlanRequest()

  displayedColumns = ['Company_Name', 'Insurer_Name', 'Principal_Plan_Name', 'Treatment_Plan_Company_Status_Name', 'Action'];

  constructor(private companyTreatmentPlan: CompanyTreatmentPlanService, private snackBar: MatSnackBar) { }

  ngOnInit(): void {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort
    this.companyTreatmentPlanRequest.Treatment_Plan_Company_ID = 0
    this.getCompanyTreatmentPlan();
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort
  }

  getCompanyTreatmentPlan() {
    this.companyTreatmentPlan.getCompanyTreatmentPlan(this.companyTreatmentPlanRequest).subscribe(response => {
      if (response.success) {
        this.dataSource.data = response.data
      } else {
        this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 })
      }
    })
  }

}
