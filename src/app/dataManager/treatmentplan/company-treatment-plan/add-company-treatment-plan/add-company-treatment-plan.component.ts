import { companyTreatmentPlanRequest } from './../../../../models/dataManager';
import { ServiceTypeService } from './../../../../services/dataManager/service-type.service';
import { InsurerService } from './../../../../services/dataManager/insurer.service';
import { CompanyTreatmentPlanService } from './../../../../services/treatmentplan/company-treatment-plan.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router, ActivatedRoute } from '@angular/router';
import { dependentRequest, Insurer } from 'app/models/dataManager';
import { DdlistService } from 'app/services/config/ddlist.service';
import { DependentService } from 'app/services/dataManager/dependent.service';
import { TpamemberService } from 'app/services/dataManager/tpamember.service';
import { CompanyService } from 'app/services/dataManager/company.service';
import { getCompanyRequest } from 'app/models/getCompanyRequest';

@Component({
  selector: 'app-add-company-treatment-plan',
  templateUrl: './add-company-treatment-plan.component.html',
  styleUrls: ['./add-company-treatment-plan.component.css']
})
export class AddCompanyTreatmentPlanComponent implements OnInit {

  toptitle: string = 'Add Company Treatment Plan';
  btntext: string = 'Save';
  isDisabled: boolean = true;
  companyTreatmentPlanForm: FormGroup;
  obj = localStorage.getItem('currentuser');
  urlText: any;


  statusList = new Array();
  companyList = new Array();
  dependantsList = new Array();
  insurer = new Array();
  serviceType = new Array();
  principalList = new Array();

  companyData: getCompanyRequest = new getCompanyRequest();
  dependentData: dependentRequest = new dependentRequest();
  insurerData: Insurer = new Insurer();

  companyTreatmentPlanRequest: companyTreatmentPlanRequest = new companyTreatmentPlanRequest()

  constructor(private fb: FormBuilder, private snackBar: MatSnackBar, private router: Router, private company: CompanyService,
    private dependentsrv: DependentService, private ddlist: DdlistService, private activate: ActivatedRoute,
    private treatment: TpamemberService, private companyTreatmentPlan: CompanyTreatmentPlanService, private insurerService: InsurerService,
    private serviceTypeService: ServiceTypeService) {
    const url = this.router.url;
    this.urlText = url.split('/');
    this.initializeCompanyTreatmentPlanForm();
    this.activate.params.subscribe(param => {
      if (this.urlText[3] != 'add') {
        this.toptitle = 'Update Company Treatment Plan';
        this.btntext = 'Update';
        console.log(`Parameter:${param.Treatment_Plan_Company_ID}`)
        this.companyTreatmentPlanRequest.Treatment_Plan_Company_ID = param.Treatment_Plan_Company_ID;
        this.getCompanyTreatmentPlanById(this.companyTreatmentPlanRequest);
      }
      if (this.urlText[3] == 'view') {
        this.toptitle = 'Company Treatment Plan Detail';
        this.isDisabled = false;
      }
    })
  }

  ngOnInit(): void {
    this.getStatusDDLists();
    this.companyData.Company_ID = 0;
    this.getCompany();
    this.dependentData.Dependent_ID = 0;
    this.getDependentsList();
    this.getTreatmentPlan();
    this.insurerData.Insurer_ID = 0;
    this.getInsurer();
    this.getServiceType();
  }

  getStatusDDLists() {
    this.ddlist.getStatusDDList().subscribe(response => {
      if (response.success) {
        this.statusList = response.data;
      } else {
        this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 })
      }
    });
  }

  getCompany() {
    this.company.getCompany(this.companyData).subscribe(response => {
      if (response.success) {
        this.companyList = response.data;
        this.companyList = this.companyList.filter(value => {
          if (value.Company_Status_ID == 1) {
            return value
          }
        })
      } else {
        this.snackBar.open(response.errorMessage, "Ok", { duration: 3000 });
      }
    });
  }

  onCompanySelect(event) {
    // this.companyTreatmentPlanForm.get('Master_TPA_ID').patchValue(event.value)
    console.log(this.companyList)
    this.companyList.filter(value => {
      if (value.Company_ID === event.value) {
        console.log(value.Company_ID, event.value, value)
        this.companyTreatmentPlanForm.get('Master_TPA_ID').patchValue(value.Master_TPA_ID)
      }
    })
  }

  getDependentsList() {
    this.dependentsrv.getDependentList(this.dependentData).subscribe(response => {
      if (response.success) {
        this.dependantsList = response.data;
        this.dependantsList = this.dependantsList.filter(value => {
          if (value.Dependent_Status_ID == 1) {
            return value;
          }
        })
      } else {
        this.snackBar.open(response.errorMessage, "Ok", { duration: 3000 });
      }
    });
  }

  getTreatmentPlan() {
    this.treatment.getTreatmentPlan(0).subscribe(response => {
      if (response.success) {
        this.principalList = response.data;
        this.principalList = this.principalList.filter(value => {
          if(value.Treatment_Plan_Status_ID == 1){
            return value;
          }
        })
      } else {
        this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 })
      }
    })
  }

  getInsurer() {
    this.insurerService.getInsurer(this.insurerData).subscribe(response => {
      if (response.success) {
        this.insurer = response.data;
        this.insurer = this.insurer.filter(value => {
          if (value.Insurer_Status_ID == 1) {
            return value
          }
        })
      } else {
        this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 })
      }
    })
  }

  getServiceType() {
    this.serviceTypeService.getServiceType(0).subscribe(response => {
      if (response.success) {
        this.serviceType = response.data;
        this.serviceType = this.serviceType.filter(value => {
          if (value.Service_Type_Status_ID == 1) {
            return value;
          }
        })
      } else {
        this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 })
      }
    })
  }

  initializeCompanyTreatmentPlanForm() {
    this.companyTreatmentPlanForm = this.fb.group({
      Treatment_Plan_Company_ID: 0,
      Company_ID: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      Insurer_ID: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      Master_TPA_ID: new FormControl({ value: '', disabled: true }),
      Start_Date: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      End_Date: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      Principal_Plan_ID: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      Dependent_Plan_ID: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      Service_Type_ID: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      Max_Child_Age: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      Max_Scaling_Polishing: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      Remarks: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      Treatment_Plan_Company_Status_ID: new FormControl({ value: 1, disabled: this.urlText[3] == 'view' ? true : false }),
      User_ID: JSON.parse(this.obj).User_ID
    })
  }

  addCompanyTreatmentPlan() {
    this.companyTreatmentPlan.addCompanyTreatmentPlan(this.companyTreatmentPlanForm.getRawValue()).subscribe(response => {
      if (response.success) {
        this.snackBar.open('Company Treatment Plan Saved Successfully', 'ok', { duration: 3000 })
        this.router.navigate(['/dataManager/treatmentPlanCompany']);
        this.companyTreatmentPlanForm.reset()
      } else {
        this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 })
      }
    })
  }

  getCompanyTreatmentPlanById(companyRequest) {
    this.companyTreatmentPlan.getCompanyTreatmentPlan(companyRequest).subscribe(response => {
      if (response.success) {
        this.companyTreatmentPlanForm.patchValue(response.data[0]);
      } else {
        this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 })
      }
    })
  }

}
