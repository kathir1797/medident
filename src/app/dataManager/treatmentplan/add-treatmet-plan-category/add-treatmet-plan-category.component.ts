import { MatSnackBar } from '@angular/material/snack-bar';
import { TreatmentCategoryService } from 'app/services/treatmentManager/treatment-category.service';
import { ActivatedRoute, Router } from '@angular/router';
import { TpamemberService } from 'app/services/dataManager/tpamember.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { treatmentCategory } from 'app/models/treatmentManager';
import { tap } from 'rxjs/operators';
import { element } from 'protractor';

@Component({
  selector: 'app-add-treatmet-plan-category',
  templateUrl: './add-treatmet-plan-category.component.html',
  styleUrls: ['./add-treatmet-plan-category.component.css']
})
export class AddTreatmetPlanCategoryComponent implements OnInit {

  toptitle: string = 'Add Treatment Plan Category';
  btntext: string = 'Save';
  isDisabled: boolean = true;


  treatmentPlanCategoryForm: FormGroup;
  obj = localStorage.getItem('currentuser');
  urlText: any;

  treatmentCategoryReq: treatmentCategory = new treatmentCategory();

  statusList = new Array();
  treatmentCategoryList = new Array();
  treatmentPlanCategoryList = new Array();
  treatmentPlanList = new Array();

  constructor(private fb: FormBuilder, private tpa: TpamemberService, private treatmentCategoryService: TreatmentCategoryService,
    private router: Router, private activatedRoute: ActivatedRoute, private snackBar: MatSnackBar) {
    const url = this.router.url;
    this.urlText = url.split('/');
    this.initializeTreatmentPlanCategoryForm();
    this.activatedRoute.queryParams.subscribe(query => {
      if (query.Treatment_Plan_ID != null) {
        console.log(query.Treatment_Plan_ID)
        this.tpa.getTreatmentPlan(query.Treatment_Plan_ID).subscribe(response => {
          if (response.success) {
            this.treatmentPlanList = response.data;
            this.treatmentPlanCategoryForm.get('Treatment_Plan_ID').patchValue(this.treatmentPlanList[0].Treatment_Plan_ID);
          } else {
            this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 })
          }
        });
        this.treatmentCategoryService.getUnassignedTreatmentCategory(query.Treatment_Plan_ID).subscribe(response => {
          if (response.success) {
            this.treatmentCategoryList = response.data
          } else {
            this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 })
          }
        })

      }
    })
  }

  ngOnInit(): void {
    this.treatmentCategoryReq.Treatment_Category_ID = 0;

    // this.getTreatmentPlan();
    this.activatedRoute.params.subscribe(param => {
      console.log(param)
      if (param.Treatment_Plan_Category_ID != null) {
        this.toptitle = 'Update Treatment Plan Category';
        this.btntext = 'Update';
        this.getTreatmentPlan();
        this.getTreatmentCategory();
        this.treatmentCategoryService.getTreatmentPlanCategory(parseInt(param.Treatment_Plan_Category_ID)).subscribe(response => {
          if (response.success) {
            console.log(response)
            this.treatmentPlanCategoryForm.patchValue({
              Treatment_Plan_Category_ID: response.data[0].Treatment_Plan_Category_ID,
              Treatment_Plan_ID: response.data[0].Treatment_Plan_ID,
              Treatment_Category_ID: response.data[0].Treatment_Category_ID,
              // Percentage: response.data[0].Percentage,
              Rate_Limit: response.data[0].Rate_Limit,
              Treatment_Plan_Category_Status_ID: 1,
              User_ID: JSON.parse(this.obj).User_ID
            })
          } else {
            this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 })
          }
        })
      }
      if (this.urlText[3] == 'view') {
        this.toptitle = 'View Treatment Plan Category';
        this.isDisabled = false
      }
    })
  }

  initializeTreatmentPlanCategoryForm() {
    this.treatmentPlanCategoryForm = this.fb.group({
      Treatment_Plan_Category_ID: 0,
      Treatment_Plan_ID: new FormControl({ value: '1', disabled: this.urlText[3] == 'view' ? true : false }),
      Treatment_Category_ID: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      Percentage: new FormControl({ value: 0, disabled: this.urlText[3] == 'view' ? true : false }),
      Rate_Limit: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      Treatment_Plan_Category_Status_ID: 1,
      User_ID: JSON.parse(this.obj).User_ID
    }, {
      validators: this.rangeValidator('Rate_Limit')
    })
  }

  get formControls() {
    return this.treatmentPlanCategoryForm.controls;
  }

  getTreatmentCategory() {
    this.treatmentCategoryService.getTreatmentCategory(this.treatmentCategoryReq).subscribe(response => {
      if (response.success) {
        this.treatmentCategoryList = response.data
      } else {
        this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 })
      }
    })
  }

  getTreatmentPlanCategory() {
    return this.treatmentCategoryService.getTreatmentPlanCategory(0)
      .pipe
      (
        tap(response => {
          if (response.success) {
            this.treatmentPlanCategoryList = response.data
          } else {
            this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 })
          }
        })
      )
  }

  getTreatmentPlan() {
    this.tpa.getTreatmentPlan(0).subscribe(response => {
      if (response.success) {
        this.treatmentPlanList = response.data;
      } else {
        this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 })
      }
    })
  }

  saveTreatmentPlanCategory() {
    if (this.treatmentPlanCategoryForm.valid) {
      this.treatmentCategoryService.saveTreatmentPlanCategory(JSON.stringify(this.treatmentPlanCategoryForm.value)).subscribe(response => {
        if (response.success) {
          this.treatmentPlanCategoryForm.reset();
          this.router.navigate(['/dataManager/treatmentPlan'])
          this.snackBar.open('Treatment Plan Category Saved Successfully', 'ok', { duration: 3000 })
        } else {
          this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 })
        }
      })
    } else {
      this.snackBar.open('Please Make Sure the Claim Limit is Valid', 'OK', { duration: 3000 })
    }

  }

  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach((field) => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({
          onlySelf: true
        });
      } else if (control instanceof FormGroup) { this.validateAllFormFields(control); }
    });
  }

  rangeValidator(control: string) {
    return (formGroup: FormGroup) => {
      const rangeControl = formGroup.controls[control];
      if (!rangeControl) {
        return null;
      }

      if (rangeControl.errors && !rangeControl.errors.range) {
        return null;
      }

      if (rangeControl.value >= -1 && rangeControl.value != 1) {
        rangeControl.setErrors({ range: true });
      } else {
        rangeControl.setErrors(null);
      }
    }
  }

}
