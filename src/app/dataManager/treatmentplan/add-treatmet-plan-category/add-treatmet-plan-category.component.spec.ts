import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddTreatmetPlanCategoryComponent } from './add-treatmet-plan-category.component';

describe('AddTreatmetPlanCategoryComponent', () => {
  let component: AddTreatmetPlanCategoryComponent;
  let fixture: ComponentFixture<AddTreatmetPlanCategoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddTreatmetPlanCategoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddTreatmetPlanCategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
