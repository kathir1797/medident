import { treatmentCodeRequest, treatmentGroup } from './../../../models/treatmentManager';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router, ActivatedRoute } from '@angular/router';
import { treatmentCategory } from 'app/models/treatmentManager';
import { MatRadioChange } from '@angular/material/radio';
import { TreatmentCodeService } from 'app/services/treatmentManager/treatment-code.service';
import { TreatmentGroupService } from 'app/services/treatmentManager/treatment-group.service';
import { treatmentGroupLimit } from 'app/models/dataManager';
import { DdlistService } from 'app/services/config/ddlist.service';
import { TpamemberService } from 'app/services/dataManager/tpamember.service';
import { TreatmentCategoryService } from 'app/services/treatmentManager/treatment-category.service';

@Component({
  selector: 'app-addtreatmentplan',
  templateUrl: './addtreatmentplan.component.html',
  styleUrls: ['./addtreatmentplan.component.css']
})
export class AddtreatmentplanComponent implements OnInit {

  toptitle: string = 'Add Treatment Plan';
  btntext: string = 'Save';
  isDisabled: boolean = true;
  treatmentPlanForm: FormGroup;
  obj = localStorage.getItem('currentuser');
  urlText: any;
  statusList = new Array();
  treatmentCategoryList = new Array();
  option1: string = 'limited'
  option2: string = 'unlimited'
  lchecked: boolean;
  uchecked: boolean;
  treatmentPlan: boolean = false;
  Claim_Limit: any;
  step = 0;
  treatmentCategoryRequest: treatmentCategory = new treatmentCategory();

  //Group Limit
  treatmentgroupReq: treatmentGroup = new treatmentGroup();
  treatmentGrpList = new Array();
  treatmentGrpListLimit = new Array();
  treatmentGroupLimitRequest: treatmentGroupLimit[]

  //treatment Limit
  treatmentCodeReq: treatmentCodeRequest = new treatmentCodeRequest();
  treatmentLimitList = new Array();
  public Claim_Limit_Treatment_Limit: any;
  treatmentCodeLimitList = new Array();
  public Rate_Limit: any;
  public statusID: any = 1;

  constructor(private fb: FormBuilder, private snackBar: MatSnackBar, private ddlist: DdlistService, private router: Router,
    private activatedRoute: ActivatedRoute, private treatment: TpamemberService, private cat: TreatmentCategoryService,
    private treatmentGrp: TreatmentGroupService, private treatmentCode: TreatmentCodeService) {
    const url = this.router.url;
    this.urlText = url.split('/');
    console.log(this.urlText)
    this.initializePlanForm();
    this.treatmentCategoryRequest.Treatment_Category_ID = 0;
    this.activatedRoute.params.subscribe(response => {
      if (this.urlText[3] != 'add') {
        this.toptitle = 'Update Treatment Plan';
        this.btntext = 'Update';
        this.getTreatmentPlan(response.Treatment_Plan_ID);
        this.treatmentgroupReq.Treatment_Group_ID = 0
        this.treatmentCodeReq.Treatment_Code_ID = 0
        this.getTreatmentGroupLimit();
        this.getTreatmentPlanGroupLimit();
        this.getTreatmentCode();
        this.getTreatmentCodeLimit();
        this.treatmentPlan = true
      }
      if (this.urlText[3] == 'view') {
        this.isDisabled = false;
        this.toptitle = 'Treatment Plan Detail'
      }
    })
  }

  ngOnInit(): void {
    this.getStatusDDLists();
    this.getTreatmentCategory();
    this.treatmentCategoryRequest.Treatment_Category_ID = 0;
  }

  getStatusDDLists() {
    this.ddlist.getStatusDDList().subscribe(response => {
      if (response.success) {
        this.statusList = response.data;
      } else {
        this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 })
      }
    });
  }

  initializePlanForm() {
    this.treatmentPlanForm = this.fb.group({
      /**@Treatment_Plan_ID INT,
  @Treatment_Plan_Type_ID INT,	
  @Treatment_Plan_Code varchar(10),
  @Treatment_Plan_Name varchar(50),
  @Treatment_Plan_Status_ID int,
  @User_ID int */
      Treatment_Plan_ID: 0,
      Treatment_Plan_Type_ID: new FormControl({ value: '1', disabled: this.urlText[3] == 'view' ? true : false }),
      Treatment_Plan_Code: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      Treatment_Plan_Name: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      // Treatment_Category_ID: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      // Percentage: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      // Rate_Limit: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      Treatment_Plan_Status_ID: new FormControl({ value: 1, disabled: this.urlText[3] == 'view' ? true : false }),
      User_ID: JSON.parse(this.obj).User_ID
    })
  }

  getTreatmentCategory() {
    this.cat.getTreatmentCategory(this.treatmentCategoryRequest).subscribe(response => {
      if (response.success) {
        this.treatmentCategoryList = response.data
        this.treatmentCategoryList = this.treatmentCategoryList.filter(value => {
          if (value.Treatment_Category_Status_ID == 1) {
            this.statusID = value.Treatment_Category_Status_ID
            return value
          }
        })
      } else {
        this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 })
      }
    })
  }

  onTreatmentPnaTypeChange(event: MatRadioChange) {
    console.log(event.source.checked)
    this.treatmentPlanForm.get('Treatment_Plan_Type_ID').patchValue(parseInt(event.value))
  }

  saveTreatmentPlan() {
    this.treatment.addTreatmentPlan(JSON.stringify(this.treatmentPlanForm.value)).subscribe(response => {
      if (response.success) {
        this.treatmentPlanForm.reset();
        this.router.navigate(['/dataManager/treatmentPlan']);
        this.snackBar.open('Treatment Plan Saved Successfully', 'OK', { duration: 3000 })
      } else {
        this.snackBar.open(response.errorMessage, 'OK', { duration: 3000 })
      }
    })
  }

  getTreatmentPlan(id) {
    this.treatment.getTreatmentPlan(id).subscribe(response => {
      if (response.success) {
        this.lchecked = response.data[0].Treatment_Plan_Type_ID == 1 ? true : false;
        this.uchecked = response.data[0].Treatment_Plan_Type_ID == 2 ? true : false;
        this.treatmentPlanForm.patchValue({
          Treatment_Plan_ID: response.data[0].Treatment_Plan_ID,
          Treatment_Plan_Type_ID: parseInt(response.data[0].Treatment_Plan_Type_ID),
          Treatment_Plan_Code: response.data[0].Treatment_Plan_Code,
          Treatment_Plan_Name: response.data[0].Treatment_Plan_Name,
          // Treatment_Category_ID: response.data[0].Treatment_Category_ID,
          // Percentage: response.data[0].Percentage,
          // Rate_Limit: response.data[0].Rate_Limit,
          Treatment_Plan_Status_ID: response.data[0].Treatment_Plan_Status_ID,
          User_ID: JSON.parse(this.obj).User_ID
        })
      } else {
        this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 })
      }
    })
  }

  getTreatmentGroupLimit() {
    this.treatmentGrp.getTreatmentGroup(this.treatmentgroupReq).subscribe(response => {
      if (response.success) {
        this.treatmentGrpList = response.data;
        this.treatmentGrpList = this.treatmentGrpList.filter(value => {
          if (value.Treatment_Group_Status_ID == 1) {
            return value;
          }
        })
      } else {
        this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 })
      }
    });
  }

  getTreatmentCode() {
    this.treatmentCode.getTreatmentCode(this.treatmentCodeReq).subscribe(response => {
      if (response.success) {
        this.treatmentLimitList = response.data
        this.treatmentLimitList = this.treatmentLimitList.filter(value => {
          if (value.Treatment_Code_Status_ID == 1) {
            return value
          }
        })
      } else {
        this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 })
      }
    })
  }

  //#region TreatmentGroupLimit
  addTreatmentGroupLimit() {
    // console.log(this.treatmentGrpList.map(x => ))
    const data = this.treatmentGrpList.map(x => ({
      Treatment_Plan_Group_Limit_ID: 0,
      Treatment_Plan_ID: parseInt(this.urlText[3]),
      Treatment_Group_ID: x.Treatment_Group_ID,
      Claim_Limit: x.Claim_Limit,
      Treatment_Plan_Group_Limit_Status_ID: 1,
      User_ID: JSON.parse(this.obj).User_ID
    })
    );

    this.treatment.deleteTreatmentPlanGroup(parseInt(this.urlText[3])).subscribe(response => {
      if (response.success) {
        for (var i = 0; i < data.length; i++) {
          if (data[i].Claim_Limit != null) {
            this.treatment.addTreatmentGroupLimit(data[i]).subscribe(grpLimit => {
              if (grpLimit.success) {
                this.snackBar.open('Treatment Group Updated Successfully', 'ok', { duration: 3000 })
              } else {
                this.snackBar.open(grpLimit.errorMessage, 'Treatment Group Limit', { duration: 3000 })
              }
            });
          }

        }
      } else {
        this.snackBar.open(response.errorMessage, 'Treatment Group Limit', { duration: 3000 })
      }

    })

  }

  getTreatmentPlanGroupLimit() {
    this.treatment.getTreatmentPlanGroupLimit(parseInt(this.urlText[3])).subscribe(response => {
      if (response.success) {
        this.treatmentGrpListLimit = response.data;
        const dataFiltered = this.treatmentGrpListLimit.filter(value => {
          if (value.Treatment_Plan_ID === parseInt(this.urlText[3])) {
            return value.Claim_Limit
          } else {
            return
          }
        })
        console.log(dataFiltered)
        for (var i = 0; i < dataFiltered.length; i++)
          this.treatmentGrpList[i].Claim_Limit = dataFiltered[i].Claim_Limit
      } else {
        this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 })
      }
    });
  }
  //#endregion Treatment Group Limit

  //#region Treatment Limit
  addTreatmentPlanCodeLimit() {
    const data = this.treatmentLimitList.map(x =>
      (
        {
          Treatment_Plan_Code_Limit_ID: 0,
          Treatment_Plan_ID: parseInt(this.urlText[3]),
          Treatment_Code_ID: x.Treatment_Code_ID,
          Claim_Limit: x.Claim_Limit_Treatment_Limit,
          Rate_Limit: x.Rate_Limit,
          Treatment_Plan_Code_Limit_Status_ID: x.Treatment_Code_Status_ID,
          User_ID: JSON.parse(this.obj).User_ID
        }
      )
    );

    this.treatment.deleteTreatmentPlanCode(parseInt(this.urlText[3])).subscribe(response => {
      if (response.success) {
        for (var i = 0; i < data.length; i++) {
          this.treatment.addTreatmentCodeLimit(data[i]).subscribe(codeLimit => {
            if (codeLimit.success) {
              this.snackBar.open('Treatment Code Limit Updated Successfully', 'ok', { duration: 3000 })
            } else {
              this.snackBar.open(codeLimit.errorMessage, 'Treatment Code Limit', { duration: 3000 })
            }
          });
        }
      } else {
        this.snackBar.open(response.errorMessage, 'Treatment Code Limit', { duration: 3000 })
      }
    })
  }

  getTreatmentCodeLimit() {
    this.treatment.getTreatmentPlanCodeLimit(parseInt(this.urlText[3])).subscribe(response => {
      if (response.success) {
        this.treatmentCodeLimitList = response.data;

        const filteredData = this.treatmentCodeLimitList.filter(value => {
          if (value.Treatment_Plan_ID === parseInt(this.urlText[3])) {
            return value
          } else {
            return
          }
        });
        console.log(filteredData)
        for (var i = 0; i < filteredData.length; i++) {
          this.treatmentLimitList[i].Claim_Limit_Treatment_Limit = filteredData[i].Claim_Limit
          this.treatmentLimitList[i].Rate_Limit = filteredData[i].Rate_Limit
        }
        console.log(this.treatmentLimitList)
      } else {
        this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 })
      }
    });
  }
  //#endregion Treatment Limit

  addTreatmentPlanCategory() {
    console.log(this.urlText)
    if (this.urlText[2] === 'view') {
      this.router.navigate(['/dataManager/treatmentPlanCategory'], { queryParams: { Treatment_Plan_ID: this.urlText[3] } })
    } else {
      this.router.navigate(['/dataManager/treatmentPlanCategory'], { queryParams: { Treatment_Plan_ID: this.urlText[3] } })
    }
  }

  //ExpansionPanel Functions
  setStep(index: number) {
    this.step = index;
  }

  nextStep() {
    this.step++;
  }

  prevStep() {
    this.step--;
  }
}
