import { AfterViewInit } from '@angular/core';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { TpamemberService } from 'app/services/dataManager/tpamember.service';

@Component({
  selector: 'app-treatmentplan',
  templateUrl: './treatmentplan.component.html',
  styleUrls: ['./treatmentplan.component.css']
})
export class TreatmentplanComponent implements OnInit, AfterViewInit {

  @ViewChild(MatPaginator) paginator: MatPaginator;
  dataSource = new MatTableDataSource<any>();
  length: number
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  displayedColumns = ['Treatment_Plan_Code', 'Treatment_Plan_Name', 'Treatment_Plan_Type_Name', 'Status_Name', 'Action'];

  constructor(private treatment: TpamemberService, private snackBar: MatSnackBar) { }

  ngOnInit(): void {
    this.getTreatmentPlan();
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort
  }

  getTreatmentPlan() {
    this.treatment.getTreatmentPlan(0).subscribe(response => {
      if (response.success) {
        this.dataSource.data = response.data;
      } else {
        this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 })
      }
    })
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

}
