import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TreatmetPlanCategoryComponent } from './treatmet-plan-category.component';

describe('TreatmetPlanCategoryComponent', () => {
  let component: TreatmetPlanCategoryComponent;
  let fixture: ComponentFixture<TreatmetPlanCategoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TreatmetPlanCategoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TreatmetPlanCategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
