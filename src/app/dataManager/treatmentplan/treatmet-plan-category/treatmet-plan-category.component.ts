import { TreatmentCategoryService } from './../../../services/treatmentManager/treatment-category.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-treatmet-plan-category',
  templateUrl: './treatmet-plan-category.component.html',
  styleUrls: ['./treatmet-plan-category.component.css']
})
export class TreatmetPlanCategoryComponent implements OnInit {

  @Input() Treatment_Plan_ID: number;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  dataSource = new MatTableDataSource<any>();
  length: number
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  Status_Name: string;
  displayedColumns =
    [
      'Category_Number',
      'Treatment_Plan_Type_Name',
      'Treatment_Plan_Code',
      'Treatment_Plan_Name',
      'Rate_Limit',
      'Status_Name',
      'Action'
    ];

  constructor(private snackBar: MatSnackBar, private treatmentCategory: TreatmentCategoryService) { }

  ngOnInit(): void {
    this.getTreatmentPlanCategory();
  }

  getTreatmentPlanCategory() {
    this.treatmentCategory.getTreatmentPlanCategory(0).subscribe(response => {
      if (response.success) {
        this.dataSource.data = response.data;
        this.dataSource.data = this.dataSource.data.filter(x => x.Treatment_Plan_ID == this.Treatment_Plan_ID)
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        for (var i = 0; i < this.dataSource.data.length; i++) {
          if (this.dataSource.data[i].Treatment_Plan_Category_Status_ID == 1) {
            this.dataSource.data[i].Status_Name = 'Active'
          } else {
            this.dataSource.data[i].Status_Name = 'InActive'
          }
        }
      } else {
        this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 })
      }
    })
  }

}
