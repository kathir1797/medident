import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router, ActivatedRoute } from '@angular/router';
import { DdlistService } from 'app/services/config/ddlist.service';
import { ClinicsService } from 'app/services/dataManager/clinics.service';

@Component({
  selector: 'app-clinic-profile',
  templateUrl: './clinic-profile.component.html',
  styleUrls: ['./clinic-profile.component.css']
})
export class ClinicProfileComponent implements OnInit {
  clinicTypesList = new Array();
  zoneList = new Array();
  stateList = new Array();
  cityList = new Array();
  statusList = new Array();
  operatingHours = new Array();
  clinicsForm: FormGroup;
  toptitle: string = 'Add Clinics';
  btntext: string = 'Save';
  urlText: any;
  isDisabled: boolean = true;
  obj = localStorage.getItem('currentuser');
  public listItems: Array<any> = [
    { text: '12', value: '12' },
    { text: '24', value: '24' },
  ];
  BranchYN: boolean;
  TPAPanelYN: boolean;
  InsurancePanelYN: boolean;
  PublishedYN: boolean;

  constructor(private fb: FormBuilder, private ddlist: DdlistService, private clinicSer: ClinicsService, private snackBar: MatSnackBar, private router: Router, private activatedRoute: ActivatedRoute) {
    const url = this.router.url;
    this.urlText = url.split('/');
    this.clinicsForm = this.fb.group({
      Clinic_ID: 0,
      Clinic_Type_ID: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      Clinic_Code: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      Registration_No: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      Clinic_Name: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      Clinic_Address: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      State_ID: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      Zone_ID: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      City_ID: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      Postcode: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      Telephone_No: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      Handphone_No: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      Fax_No: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      Clinic_Email: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      Clinic_Website: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      Social_Media_1: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      Social_Media_2: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      Social_Media_3: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      Contact_Person_1: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      Contact_Person_2: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      Branch_YN: this.BranchYN == true ? "1" : "0",//new FormControl({ value: '', disabled: this.urlText[3] == 'view'?  true: false }),
      TPA_Panel_YN: this.TPAPanelYN == true ? "1" : "0",//new FormControl({ value: '', disabled: this.urlText[3] == 'view'?  true: false }),
      Insurance_Panel_YN: this.InsurancePanelYN == true ? "1" : "0",//new FormControl({ value: '', disabled: this.urlText[3] == 'view'?  true: false }),
      Credit_Terms: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      Bank_Name: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      AC_Name: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      AC_Number: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      Admin_Fee_Per: new FormControl({ value: 0, disabled: this.urlText[3] == 'view' ? true : false }),
      Operating_Hours: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      Latitude: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      Longitude: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      Directions: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      Status_ID: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      Published_YN: this.PublishedYN == true ? "Y" : "N",
      User_ID: JSON.parse(this.obj).User_ID,
    });
    console.log(this.clinicsForm.value);
    this.activatedRoute.params.subscribe(params => {
      if (params['Clinic_ID'] != 'create') {
        this.toptitle = 'Edit TPAMember';
        this.btntext = 'Update';
        this.clinicSer.EditEditClinics(params['Clinic_ID']).subscribe(response => {
          if (response.success) {
            console.log(response.data[0].MasterTPAID);
            this.clinicsForm.patchValue({
              Clinic_ID: response.data[0].Clinic_ID,
              Clinic_Type_ID: response.data[0].Clinic_Type_ID,
              Clinic_Code: response.data[0].Clinic_Code,
              Registration_No: response.data[0].Registration_No,
              Clinic_Name: response.data[0].Clinic_Name,
              Clinic_Address: response.data[0].Clinic_Address,
              State_ID: response.data[0].State_ID,
              Zone_ID: response.data[0].Zone_ID,
              City_ID: response.data[0].City_ID,
              Postcode: response.data[0].Postcode,
              Telephone_No: response.data[0].Telephone_No,
              Handphone_No: response.data[0].Handphone_No,
              Fax_No: response.data[0].Fax_No,
              Clinic_Email: response.data[0].Clinic_Email,
              Clinic_Website: response.data[0].Clinic_Website,
              Social_Media_1: response.data[0].Social_Media_1,
              Social_Media_2: response.data[0].Social_Media_2,
              Social_Media_3: response.data[0].Social_Media_3,
              Contact_Person_1: response.data[0].Contact_Person_1,
              Contact_Person_2: response.data[0].Contact_Person_2,
              Branch_YN: response.data[0].Branch_YN == "1" ? this.BranchYN = true : this.BranchYN = false,
              TPA_Panel_YN: response.data[0].TPA_Panel_YN == "1" ? this.TPAPanelYN = true : this.TPAPanelYN = false,
              Insurance_Panel_YN: response.data[0].Insurance_Panel_YN == "1" ? this.InsurancePanelYN = true : this.InsurancePanelYN = false,
              Published_YN: response.data[0].Published_YN == "Y" ? this.PublishedYN = true : this.PublishedYN = false,
              Credit_Terms: response.data[0].Credit_Terms,
              Bank_Name: response.data[0].Bank_Name,
              AC_Name: response.data[0].AC_Name,
              AC_Number: response.data[0].AC_Number,
              Admin_Fee_Per: 0,// response.data[0].Admin_Fee_Per,
              Operating_Hours: response.data[0].Operating_Hours,
              Latitude: response.data[0].Latitude,
              Longitude: response.data[0].Longitude,
              Directions: response.data[0].Directions,
              Status_ID: response.data[0].Status_ID,
              User_ID: JSON.parse(this.obj).User_ID,
            });
          } else {
            this.snackBar.open(response.errorMessage, 'OK', { duration: 3000 }); (response.errorMessage);
          }
        });

      }
    });
  }

  ngOnInit() {
    this.getClinicTypeDDList();
    this.getZoneDDLists();
    this.getCityDDLists();
    this.getStateDDLists();
    this.getStatusDDLists();
    if (this.urlText[3] == 'view') {
      this.toptitle = "Clinics Detail";
      this.isDisabled = false;
    }
  }

  Branchchekbox(event) {
    console.log(event.checked);
    this.clinicsForm.patchValue({
      Branch_YN: event.checked == true ? "1" : "0",
      TPA_Panel_YN: this.TPAPanelYN == true ? "1" : "0",
      Insurance_Panel_YN: this.InsurancePanelYN == true ? "1" : "0",
    });
  }
  TPAPanelchekbox(event) {
    console.log(event.checked);
    this.clinicsForm.patchValue({
      TPA_Panel_YN: event.checked == true ? "1" : "0",
      Branch_YN: this.BranchYN == true ? "1" : "0",
      Insurance_Panel_YN: this.InsurancePanelYN == true ? "1" : "0",
    });
  }
  InsurancePanelchekbox(event) {
    console.log(event.checked);
    this.clinicsForm.patchValue({
      Insurance_Panel_YN: event.checked == true ? "1" : "0",
      Branch_YN: this.BranchYN == true ? "1" : "0",
      TPA_Panel_YN: this.TPAPanelYN == true ? "1" : "0",
    });
  }

  panelCheckEvent(event) {
    this.clinicsForm.patchValue({
      Published_YN: event.checked == true ? "Y" : "N"
    });
  }

  getClinicTypeDDList() {
    this.ddlist.getClinicTypesDDList().subscribe(response => {
      if (response.success) {
        this.clinicTypesList = response.data;
      } else {
        this.snackBar.open(response.errorMessage, 'OK', { duration: 3000 }); (response.errorMessage, "Ok");
      }
    });
  }

  getZoneDDLists() {
    this.ddlist.getZoneDDList().subscribe(response => {
      if (response.success) {
        this.zoneList = response.data;
      } else {
        this.snackBar.open(response.errorMessage, 'OK', { duration: 3000 }); (response.errorMessage, "Ok");
      }
    });
  }

  getStateDDLists() {
    this.ddlist.getStateList().subscribe(response => {
      if (response.success) {
        this.stateList = response.data;
      } else {
        this.snackBar.open(response.errorMessage, 'OK', { duration: 3000 }); (response.errorMessage, "Ok");
      }
    });
  }

  getCityDDLists() {
    this.ddlist.getCityList().subscribe(response => {
      if (response.success) {
        this.cityList = response.data;
      } else {
        this.snackBar.open(response.errorMessage, 'OK', { duration: 3000 }); (response.errorMessage, "Ok");
      }
    });
  }

  getStatusDDLists() {
    this.ddlist.getStatusDDList().subscribe(response => {
      if (response.success) {
        this.statusList = response.data;
      } else {
        this.snackBar.open(response.errorMessage, 'OK', { duration: 3000 }); (response.errorMessage, "Ok");
      }
    });
  }

  addClinicsData() {
    if (this.clinicsForm.valid) {
      this.clinicSer.addClinics(JSON.stringify(this.clinicsForm.value)).subscribe(response => {
        if (response.success) {
          this.snackBar.open('TPAMember Saved Successfully', 'OK', { duration: 3000 });
          this.clinicsForm.reset();
          this.router.navigate(['/dataManager/clinic']);
        } else {
          this.snackBar.open(response.errorMessage, 'OK', { duration: 3000 });
        }
      });
    } else {
      this.validateAllFormFields(this.clinicsForm);
      this.snackBar.open('Form cannot Invalid', 'OK', { duration: 3000 });
    }
  }

  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach((field) => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({
          onlySelf: true
        });
      } else if (control instanceof FormGroup) { this.validateAllFormFields(control); }
    });
  }


}