import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MasterTPASetupComponent } from './master-tpasetup.component';

describe('MasterTPASetupComponent', () => {
  let component: MasterTPASetupComponent;
  let fixture: ComponentFixture<MasterTPASetupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MasterTPASetupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MasterTPASetupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
