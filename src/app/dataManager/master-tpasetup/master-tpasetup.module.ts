import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MasterTpasetupRoutingModule } from './master-tpasetup-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    MasterTpasetupRoutingModule
  ]
})
export class MasterTpasetupModule { }
