import { Router } from '@angular/router';
import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { msterTPASetupRequest } from 'app/models/dataManager';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MasterTPASetupService } from 'app/services/dataManager/master-tpasetup.service';

@Component({
  selector: 'app-master-tpasetup',
  templateUrl: './master-tpasetup.component.html',
  styleUrls: ['./master-tpasetup.component.css']
})
export class MasterTPASetupComponent implements AfterViewInit, OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  dataSource = new MatTableDataSource<any>();
  length: number
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['Branch_Code', 'Branch_Description', 'Product_Code', 'Product_Description', 'Action'];

  msterTPASetupData: msterTPASetupRequest = new msterTPASetupRequest();
  constructor(private masterTPAsetup: MasterTPASetupService, private snackBar: MatSnackBar, private router: Router) { }

  ngOnInit() {
    this.msterTPASetupData.Master_TPA_Setup_ID = 0;
    this.getMasterTPASetupList();
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  getMasterTPASetupList() {
    this.masterTPAsetup.getMasterTPASetup(this.msterTPASetupData).subscribe(response => {
      if (response.success) {
        this.dataSource.data = response.data;
        this.dataSource.sort = this.sort;
        this.length = response.data.length;
      } else {
        this.snackBar.open(response.errorMessage, "Ok", { duration: 3000 });
      }
    });
  }





}

