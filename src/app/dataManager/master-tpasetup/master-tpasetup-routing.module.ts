import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MasterTPASetupComponent } from './master-tpasetup.component';


const routes: Routes = [{
  path: '',
  component: MasterTPASetupComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MasterTpasetupRoutingModule { }
