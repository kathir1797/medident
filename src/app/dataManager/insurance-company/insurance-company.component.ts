import { MatSnackBar } from '@angular/material/snack-bar';
import { InsurerService } from './../../services/dataManager/insurer.service';
import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Insurer } from 'app/models/dataManager';

@Component({
  selector: 'app-insurance-company',
  templateUrl: './insurance-company.component.html',
  styleUrls: ['./insurance-company.component.css']
})
export class InsuranceCompanyComponent implements OnInit, AfterViewInit {

  insurerData: Insurer = new Insurer();
  dataSource = new MatTableDataSource<any>();
  length: number;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  displayedColumns = ['Insurer_Code', 'Insurer_Name', 'Status_Name', 'Action'];

  constructor(private insurerService: InsurerService, private snackBar: MatSnackBar) { }

  ngOnInit(): void {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    this.insurerData.Insurer_ID = 0;
    this.getInsurer();
  }

  ngAfterViewInit(){
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator
  }

  getInsurer() {
    this.insurerService.getInsurer(this.insurerData).subscribe(response => {
      if (response.success) {
        this.dataSource.data = response.data;
      } else {
        this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 })
      }
    })
  }

}
