import { DdlistService } from 'app/services/config/ddlist.service';
import { Insurer } from 'app/models/dataManager';
import { InsurerService } from './../../../services/dataManager/insurer.service';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-add-insurance-company',
  templateUrl: './add-insurance-company.component.html',
  styleUrls: ['./add-insurance-company.component.css']
})
export class AddInsuranceCompanyComponent implements OnInit {

  toptitle: string = 'Add Insurance Company';
  btntext: string = 'Save';
  isDisabled: boolean = true;
  insuranceCompanyForm: FormGroup;
  obj = localStorage.getItem('currentuser');
  urlText: any;

  insurerData: Insurer = new Insurer();
  statusList = new Array();

  constructor(private fb: FormBuilder, private snackBar: MatSnackBar, private router: Router, private activatedRoute: ActivatedRoute,
    private ddlist: DdlistService, private insurer: InsurerService) {
    const url = this.router.url;
    this.urlText = url.split('/');
    this.initializeInsuranceCompanyForm();
    this.activatedRoute.params.subscribe(param => {
      if (this.urlText[3] != 'add') {
        this.toptitle = 'Update Insurance Company';
        this.btntext = 'Update';
        this.insurerData.Insurer_ID = param.Insurer_ID
        this.getInsurerById(this.insurerData);
      }
      if (this.urlText[3] == 'view') {
        this.toptitle = 'View Insurance Company';
        this.isDisabled = false
      }
    })
  }

  ngOnInit(): void {
    this.getStatusDDLists();
  }

  initializeInsuranceCompanyForm() {
    this.insuranceCompanyForm = this.fb.group({
      Insurer_ID: 0,
      Insurer_Code: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      Insurer_Name: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      Insurer_Type_Code: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      Insurer_Image: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      Insurer_Status_ID: new FormControl({ value: 1, disabled: this.urlText[3] == 'view' ? true : false }),
      User_ID: JSON.parse(this.obj).User_ID
    })
  }

  getStatusDDLists() {
    this.ddlist.getStatusDDList().subscribe(response => {
      if (response.success) {
        this.statusList = response.data;
      } else {
        this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 })
      }
    });
  }

  onFileSelect(input): void {
    console.log(input.target.files[0])
    function formatBytes(bytes: number): string {
      const UNITS = ['Bytes', 'kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
      const factor = 1024;
      let index = 0;
      while (bytes >= factor) {
        bytes /= factor;
        index++;
      }
      return `${parseFloat(bytes.toFixed(2))} ${UNITS[index]}`;
    }
    const file = input.target.files[0];
    const Insurer_Type_Code = this.insuranceCompanyForm.get('Insurer_Type_Code').value
    this.uploadFile(file, Insurer_Type_Code);
  }

  uploadFile(file, Insurer_Type_Code) {
    const fileBlob = file;
    const name = fileBlob.name;
    const bfile: FormData = new FormData();
    bfile.append('files', fileBlob, name);
    bfile.append('Insurer_Type_Code', Insurer_Type_Code)
    this.insurer.uploadInsurerImage(bfile).subscribe(response => {
      if (response.success) {
        this.insuranceCompanyForm.patchValue({
          Insurer_Image: response.data
        })
      } else {
        this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 })
      }
    })
  }

  saveInsurancCompany() {
    this.insurer.addInsuranceCompany(JSON.stringify(this.insuranceCompanyForm.value)).subscribe(response => {
      if (response.success) {
        this.insuranceCompanyForm.reset();
        this.router.navigate(['/dataManager/insuranceCompany'])
        this.snackBar.open('Insurance Company Saved Successfully', 'ok', { duration: 3000 })
      } else {
        this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 })
      }
    })
  }

  getInsurerById(insurerReq) {
    this.insurer.getInsurer(insurerReq).subscribe(response => {
      if (response.success) {
        this.insuranceCompanyForm.patchValue(response.data[0])
      } else {
        this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 })
      }
    })
  }

}
