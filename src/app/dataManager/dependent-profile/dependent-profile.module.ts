import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DependentProfileRoutingModule } from './dependent-profile-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    DependentProfileRoutingModule
  ]
})
export class DependentProfileModule { }
