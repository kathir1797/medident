import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router, ActivatedRoute } from '@angular/router';
import { DdlistService } from 'app/services/config/ddlist.service';
import { DependentService } from 'app/services/dataManager/dependent.service';

@Component({
  selector: 'app-dependent-profile',
  templateUrl: './dependent-profile.component.html',
  styleUrls: ['./dependent-profile.component.css']
})
export class DependentProfileComponent implements OnInit {
  employeeList = new Array();
  relationsList = new Array();
  GenderList = new Array();
  dependentForm: FormGroup;
  toptitle: string ='Add Dependents';
  btntext: string = 'Save';
  urlText : any;
  isDisabled: boolean = true;
  obj = localStorage.getItem('currentuser');

  constructor(private fb: FormBuilder, private ddlist: DdlistService, private dependentSer: DependentService, private snackBar: MatSnackBar, private router: Router, private activatedRoute: ActivatedRoute) {
    const url =this.router.url;
    this.urlText = url.split('/');
    console.log(this.urlText);
    
    this.dependentForm = this.fb.group({
      Dependent_ID: 0,
      Employee_ID: new FormControl({ value: '', disabled: this.urlText[3] == 'view'?  true: false }),
      Dependent_Name: new FormControl({ value: '', disabled: this.urlText[3] == 'view'?  true: false }),
      Dependent_NRIC_No: new FormControl({ value: '', disabled: this.urlText[3] == 'view'?  true: false }),
      Dependent_DOB: new FormControl({ value: '', disabled: this.urlText[3] == 'view'?  true: false }),
      Dependent_Gender_ID: new FormControl({ value: '', disabled: this.urlText[3] == 'view'?  true: false }),
      Relationship_ID: new FormControl({ value: '', disabled: this.urlText[3] == 'view'?  true: false }),
      Dependent_Status_ID: 1,
      User_ID: JSON.parse(this.obj).User_ID,
    });
    this.activatedRoute.params.subscribe(params => {
      if (params['Dependent_ID'] != 'create') {
        this.toptitle = 'Edit Dependents';
        this.btntext ='Update';
        this.dependentSer.editDependentData(params['Dependent_ID']).subscribe(response => {
          if (response.success) {
            this.dependentForm.patchValue({
              Dependent_ID: response.data[0].Dependent_ID,
              Employee_ID: response.data[0].Employee_ID,
              Dependent_Name: response.data[0].Dependent_Name,
              Dependent_NRIC_No: response.data[0].Dependent_NRIC_No,
              Dependent_DOB: response.data[0].Dependent_DOB,
              Dependent_Gender_ID: response.data[0].Dependent_Gender_ID,
              Relationship_ID: response.data[0].Relationship_ID,
              Dependent_Status_ID: response.data[0].Dependent_Status_ID,
              User_ID: JSON.parse(this.obj).User_ID,
            });
          } else {
            this.snackBar.open(response.errorMessage, 'OK',{duration:3000});(response.errorMessage);
          }
        });
        
      }
    });
  }

  ngOnInit() {
    this.getEmployeeDDList();
    this.getRelationsDDList();
    this.getGenderDDList();
    if(this.urlText[3] == 'view'){
      this.toptitle ="Dependents Detail";
      this.isDisabled = false;
    }
  }

  getEmployeeDDList() {
    this.ddlist.getEmployeeDDList().subscribe(response => {
      if (response.success) {
        this.employeeList = response.data;
      } else {
        this.snackBar.open(response.errorMessage, 'OK',{duration:3000});(response.errorMessage, "Ok");
      }
    });
  }

  getGenderDDList() {
    this.ddlist.getGenderDDList().subscribe(response => {
      if (response.success) {
        this.GenderList = response.data;
      } else {
        this.snackBar.open(response.errorMessage, 'OK',{duration:3000});(response.errorMessage, "Ok");
      }
    });
  }

  getRelationsDDList() {
    this.ddlist.getRelationsShipDDList().subscribe(response => {
      if (response.success) {
        this.relationsList = response.data;
      } else {
        this.snackBar.open(response.errorMessage, 'OK',{duration:3000});(response.errorMessage, "Ok");
      }
    });
  }

  addMasterTPASetup() {
    if (this.dependentForm.valid) {
      this.dependentSer.addDependentData(JSON.stringify(this.dependentForm.value)).subscribe(response => {
        if (response.success) {
          this.snackBar.open('Dependents Saved Successfully', 'OK');
          this.dependentForm.reset();
          this.router.navigate(['/dataManager/dependent']);
        } else {
          this.snackBar.open(response.errorMessage, 'OK');
        }
      });
    } else {
      this.validateAllFormFields(this.dependentForm);
      this.snackBar.open('Form cannot Invalid', 'OK');
    }
  }

  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach((field) => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({
          onlySelf: true
        });
      } else if (control instanceof FormGroup) { this.validateAllFormFields(control); }
    });
  }


}