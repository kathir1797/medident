import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DependentProfileComponent } from './dependent-profile.component';

describe('DependentProfileComponent', () => {
  let component: DependentProfileComponent;
  let fixture: ComponentFixture<DependentProfileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DependentProfileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DependentProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
