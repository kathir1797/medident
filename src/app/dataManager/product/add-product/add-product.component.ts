import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router, ActivatedRoute } from '@angular/router';
import { DdlistService } from 'app/services/config/ddlist.service';
import { ProductsService } from 'app/services/dataManager/products.service';

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.css']
})
export class AddProductComponent implements OnInit {

  productForm: FormGroup;
  toptitle: string = 'Add Product Master';
  btntext: string = 'Save';
  urlText: any;
  isDisabled: boolean = true;
  obj = localStorage.getItem('currentuser');
  statusList = new Array();

  constructor(private fb: FormBuilder, private ddlist: DdlistService, private snackBar: MatSnackBar, private router: Router,
    private activatedRoute: ActivatedRoute, private productService: ProductsService) {
    const url = this.router.url;
    this.urlText = url.split('/');
    console.log(this.urlText);

    this.productForm = this.fb.group({
      Product_ID: 0,
      Product_Code: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      Product_Description: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      Product_Status_ID: new FormControl({ value: 1, disabled: this.urlText[3] == 'view' ? true : false }),
      User_ID: JSON.parse(this.obj).User_ID,
    });
    this.activatedRoute.params.subscribe(params => {
      if (this.urlText[3] != 'add') {
        this.toptitle = 'Edit Product Master';
        this.btntext = 'Update';
        this.productService.editProductMaster(params['Product_ID']).subscribe(response => {
          if (response.success) {
            this.productForm.patchValue({
              Product_ID: response.data[0].Product_ID,
              Product_Code: response.data[0].Product_Code,
              Product_Description: response.data[0].Product_Description,
              Product_Status_ID: response.data[0].Product_Status_ID,
              User_ID: JSON.parse(this.obj).User_ID,
            });
          } else {
            this.snackBar.open(response.errorMessage, 'OK',{duration:3000});(response.errorMessage);
          }
        });
      }
    });
  }

  ngOnInit(): void {
    this.getStatusDDLists();
    if (this.urlText[3] == 'add') {
      this.toptitle = 'Add Product Master';
      this.btntext = 'Save';
    }
    if (this.urlText[3] == 'view') {
      this.toptitle = "Product Master Detail";
      this.isDisabled = false;
    }
  }


  getStatusDDLists() {
    this.ddlist.getStatusDDList().subscribe(response => {
      if (response.success) {
        this.statusList = response.data;
      } else {
        this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 })
      }
    });
  }

  addProduct() {
    if (this.productForm.valid) {
      this.productService.addProductMaster(JSON.stringify(this.productForm.value)).subscribe(response => {
        if (response.success) {
          this.productForm.reset();
          this.snackBar.open('Product Saved Successfully', 'ok', {
            duration: 3000
          });
          this.router.navigate(['/dataManager/product']);
        } else {
          this.snackBar.open(response.errorMessage, 'Close')
        }
      })
    }
  }

}
