import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { MatSnackBar } from '@angular/material/snack-bar';
import { productRequest } from 'app/models/dataManager';
import { ProductsService } from 'app/services/dataManager/products.service';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {


  @ViewChild(MatPaginator) paginator: MatPaginator;
  dataSource = new MatTableDataSource<any>();
  length: number
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['Product_Code', 'Product_Description', 'Status_Name', 'Action'];

  productData: productRequest = new productRequest();
  constructor(private productService: ProductsService, private snackBar: MatSnackBar) { }

  ngOnInit() {
    this.productData.Product_ID = 0;
    this.getProductMasterList();
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  getProductMasterList() {
    this.productService.getProductMaster(this.productData).subscribe(response => {
      if (response.success) {
        this.dataSource.data = response.data;
        this.dataSource.sort = this.sort;
        this.length = response.data.length;
      } else {
        this.snackBar.open(response.errorMessage, "Ok", { duration: 3000 });
      }
    })
  }

}
