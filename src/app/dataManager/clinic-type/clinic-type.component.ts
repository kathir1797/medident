import { ClinicsService } from 'app/services/dataManager/clinics.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-clinic-type',
  templateUrl: './clinic-type.component.html',
  styleUrls: ['./clinic-type.component.css']
})
export class ClinicTypeComponent implements OnInit {

  @ViewChild(MatPaginator) paginator: MatPaginator;
  dataSource = new MatTableDataSource<any>();
  length: number
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['Clinic_Type_Name', 'Action'];

  constructor(private clinics: ClinicsService, private snackBar: MatSnackBar) { }

  ngOnInit(): void {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    this.getClinicTypes();
  }

  getClinicTypes() {
    this.clinics.getClinictypes(0).subscribe(response => {
      if (response.success) {
        this.dataSource.data = response.data;
      } else {
        this.snackBar.open(response.errorMessage, "Ok", { duration: 3000 });
      }
    })
  }

}
