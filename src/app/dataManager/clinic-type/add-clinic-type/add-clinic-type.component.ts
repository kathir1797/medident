import { ClinicsService } from 'app/services/dataManager/clinics.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router, ActivatedRoute } from '@angular/router';
import { DdlistService } from 'app/services/config/ddlist.service';

@Component({
  selector: 'app-add-clinic-type',
  templateUrl: './add-clinic-type.component.html',
  styleUrls: ['./add-clinic-type.component.css']
})
export class AddClinicTypeComponent implements OnInit {

  clinicsForm: FormGroup;
  toptitle: string = 'Add Clinics';
  btntext: string = 'Save';
  urlText: any;
  isDisabled: boolean = true;
  obj = localStorage.getItem('currentuser');
  statusList = new Array();

  constructor(private fb: FormBuilder, private router: Router, private ddlist: DdlistService,
    private snackBar: MatSnackBar, private activated: ActivatedRoute, private clinics: ClinicsService) {
    const url = this.router.url
    this.urlText = url.split("/");
    this.initializeForm();
    this.activated.params.subscribe(param => {
      if (this.urlText[3] != 'add') {
        this.toptitle = 'Update Clinic Type';
        this.btntext = 'Update'
        this.clinics.getClinictypes(param.Clinic_Type_ID).subscribe(response => {
          if (response.success) {
            this.clinicsForm.patchValue({
              Clinic_Type_ID: response.data[0].Clinic_Type_ID,
              Clinic_Type_Name: response.data[0].Clinic_Type_Name,
              Clinic_Status_ID: response.data[0].Clinic_Status_ID,
              User_ID: JSON.parse(this.obj).User_ID
            });
          } else {
            this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 })
          }
        })
      }
      if (this.urlText[3] == 'view') {
        this.toptitle = 'View Clinic Type'
        this.isDisabled = false;
      }
    })
  }

  ngOnInit(): void {
    this.getStatusDDLists()
  }

  initializeForm() {
    this.clinicsForm = this.fb.group({
      Clinic_Type_ID: 0,
      Clinic_Type_Name: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      Clinic_Status_ID: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      User_ID: JSON.parse(this.obj).User_ID
    })
  }

  addClinicTypes() {
    this.clinics.addClinicTypes(JSON.stringify(this.clinicsForm.value)).subscribe(response => {
      if (response.success) {
        this.clinicsForm.reset();
        this.router.navigate(['/dataManager/clinicType']);
        this.snackBar.open('Clinic Type Saved Successfully', 'ok', { duration: 3000 })
      } else {
        this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 })
      }
    })
  }

  getStatusDDLists() {
    this.ddlist.getStatusDDList().subscribe(response => {
      if (response.success) {
        this.statusList = response.data;
      } else {
        this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 })
      }
    });
  }

}
