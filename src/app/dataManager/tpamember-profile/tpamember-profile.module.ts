import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TpamemberProfileRoutingModule } from './tpamember-profile-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    TpamemberProfileRoutingModule
  ]
})
export class TpamemberProfileModule { }
