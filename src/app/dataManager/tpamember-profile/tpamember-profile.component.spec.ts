import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TPAmemberProfileComponent } from './tpamember-profile.component';

describe('TPAmemberProfileComponent', () => {
  let component: TPAmemberProfileComponent;
  let fixture: ComponentFixture<TPAmemberProfileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TPAmemberProfileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TPAmemberProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
