import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router, ActivatedRoute } from '@angular/router';
import { DdlistService } from 'app/services/config/ddlist.service';
import { TpamemberService } from 'app/services/dataManager/tpamember.service';

@Component({
  selector: 'app-tpamember-profile',
  templateUrl: './tpamember-profile.component.html',
  styleUrls: ['./tpamember-profile.component.css']
})
export class TPAmemberProfileComponent implements OnInit {
  employeeList = new Array();
  masterTPAList = new Array();
  companyList = new Array();
  TPAMemberForm: FormGroup;
  toptitle: string = 'Add TPA';
  btntext: string = 'Save';
  urlText: any;
  isDisabled: boolean = true;
  isDisabledGenerate: boolean = true;
  obj = localStorage.getItem('currentuser');

  constructor(private fb: FormBuilder, private ddlist: DdlistService, private tpaMember: TpamemberService, private snackBar: MatSnackBar, private router: Router, private activatedRoute: ActivatedRoute) {
    const url = this.router.url;
    this.urlText = url.split('/');
    console.log(this.urlText);

    this.TPAMemberForm = this.fb.group({
      TPA_Member_ID: 0,
      MasterTPAID: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      TPA_ID: new FormControl({ value: '', disabled: true }),
      Company_ID: new FormControl({ value: '', disabled: true }),
      New_Master_TPA_ID: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      Principal_ID: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      No_of_Members: new FormControl({ value: 1, disabled: this.urlText[3] == 'view' ? true : false }),
      Individual_Limit: new FormControl({ value: 0, disabled: this.urlText[3] == 'view' ? true : false }),
      Family_Limit: new FormControl({ value: 0, disabled: this.urlText[3] == 'view' ? true : false }),
      Shared: new FormControl({ value: 0, disabled: this.urlText[3] == 'view' ? true : false }),
      User_ID: JSON.parse(this.obj).User_ID,
    });
    this.activatedRoute.params.subscribe(params => {
      if (params['TPA_Member_ID'] != 'create') {
        this.toptitle = 'Edit TPA Policy/Certificcate';
        this.btntext = 'Update';
        this.isDisabledGenerate = false
        this.tpaMember.editTPAMember(params['TPA_Member_ID']).subscribe(response => {
          if (response.success) {
            console.log(response.data[0].MasterTPAID);
            this.TPAMemberForm.patchValue({
              TPA_Member_ID: response.data[0].TPA_Member_ID,
              MasterTPAID: response.data[0].MasterTPAID,
              Company_ID: response.data[0].Company_ID,
              TPA_ID: response.data[0].TPA_ID,
              New_Master_TPA_ID: response.data[0].Master_TPA_ID,
              Principal_ID: response.data[0].Principal_ID,
              No_of_Members: response.data[0].No_of_Members,
              Individual_Limit: response.data[0].Individual_Limit,
              Family_Limit: response.data[0].Family_Limit,
              Shared: response.data[0].Shared,
              User_ID: JSON.parse(this.obj).User_ID,
            });
          } else {
            this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 })
          }
        });

      }
    });
  }

  ngOnInit() {
    this.getEmployeeDDList();
    this.getMasterTPADDList();
    this.getCompnyDDList();
    if (this.urlText[3] == 'view') {
      this.toptitle = "TPA Detail";
      this.isDisabled = false;
    }
  }

  getEmployeeDDList() {
    this.ddlist.getEmployeeDDList().subscribe(response => {
      if (response.success) {
        this.employeeList = response.data;
      } else {
        this.snackBar.open(response.errorMessage, "Ok", { duration: 3000 });
      }
    });
  }

  getMasterTPADDList() {
    this.ddlist.getMasterMPADDList().subscribe(response => {
      if (response.success) {
        this.masterTPAList = response.data;
      } else {
        this.snackBar.open(response.errorMessage, "Ok",{
          duration: 3000
        });
      }
    });
  }

  getCompnyDDList() {
    this.ddlist.getCompanyDDList().subscribe(response => {
      if (response.success) {
        this.companyList = response.data;
      } else {
        this.snackBar.open(response.errorMessage, "Ok",{
          duration: 3000
        });
      }
    });
  }

  BindingValue(event) {
    console.log(event);
    this.masterTPAList.filter(value => {
      if (value.New_Master_TPA_ID == event.value) {
        this.TPAMemberForm.patchValue({
          MasterTPAID: value.MasterTPAID,
          Company_ID: value.Company_ID,
        });
      }
    })
    // this.TPAMemberForm.patchValue({
    //   MasterTPAID: event.value.MasterTPAID,
    //   TPA_ID: event.value.MasterTPAID,
    //   Master_TPA_ID: event.value.Master_TPA_ID,
    // });
  }

  addMasterTPASetup() {
    if (this.TPAMemberForm.valid) {
      this.tpaMember.addTPAMember(this.TPAMemberForm.getRawValue()).subscribe(response => {
        if (response.success) {
          this.snackBar.open('TPA Saved Successfully', 'OK', {
            duration: 3000
          });
          this.TPAMemberForm.reset();
          this.router.navigate(['/dataManager/tpamember']);
        } else {
          this.snackBar.open(response.errorMessage, 'OK',{
            duration: 3000
          });
        }
      });
    } else {
      this.validateAllFormFields(this.TPAMemberForm);
      this.snackBar.open('Form cannot Invalid', 'Cancel',{
        duration: 3000
      });
    }
  }


  generateTPAID() {
    this.tpaMember.generateTPAID().subscribe(response => {
      if (response.success) {
        this.TPAMemberForm.patchValue({
          TPA_ID: this.TPAMemberForm.get('New_Master_TPA_ID').value + '-' + response.data
        });
      } else {
        this.snackBar.open(response.errorMessage, 'ok');
      }
    });
  }

  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach((field) => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({
          onlySelf: true
        });
      } else if (control instanceof FormGroup) { this.validateAllFormFields(control); }
    });
  }


}