import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DependentRoutingModule } from './dependent-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    DependentRoutingModule
  ]
})
export class DependentModule { }
