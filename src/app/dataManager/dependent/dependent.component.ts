import { EmployeeService } from 'app/services/dataManager/employee.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { dependentRequest } from 'app/models/dataManager';
import { MatSnackBar } from '@angular/material/snack-bar';
import { DependentService } from 'app/services/dataManager/dependent.service';

@Component({
  selector: 'app-dependent',
  templateUrl: './dependent.component.html',
  styleUrls: ['./dependent.component.css']
})
export class DependentComponent implements AfterViewInit, OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  dataSource = new MatTableDataSource<any>();
  length: number
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  status_name: string;
  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['Dependent_Name', 'Employee_Name', 'Dependent_DOB', 'Relationship_Name', 'status_name', 'Action'];

  dependentData: dependentRequest = new dependentRequest();
  constructor(private dependentsrv: DependentService, private snackBar: MatSnackBar, private router: ActivatedRoute
    , private employee: EmployeeService) {
    const url = this.router
    url.queryParams.subscribe(param => {
      console.log(param)
      if (param.Principal_ID != null) {
        this.getDependentByPrincipal(param.Principal_ID);
      } else {
        this.dependentData.Dependent_ID = 0;
        this.getDependentsList();
      }
    })
  }

  ngOnInit() {
    this.dependentData.Dependent_ID = 0;
    // this.getDependentsList();
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  getDependentsList() {
    this.dependentsrv.getDependentList(this.dependentData).subscribe(response => {
      if (response.success) {
        this.dataSource.data = response.data;
        this.dataSource.sort = this.sort;
        this.length = response.data.length;
        for (var i = 0; i < this.dataSource.data.length; i++) {
          if (this.dataSource.data[i].Dependent_Status_ID == 1) {
            this.dataSource.data[i].status_name = 'Active'
          } else {
            this.dataSource.data[i].status_name = 'InActive'
          }
        }
      } else {
        this.snackBar.open(response.errorMessage, "Ok", { duration: 3000 });
      }
    });
  }

  getDependentByPrincipal(id) {
    this.employee.getDependentByPrincipal(id).subscribe(response => {
      if (response.success) {
        this.dataSource.data = response.data;
        this.dataSource.sort = this.sort;
        this.length = response.data.length;
        for (var i = 0; i < this.dataSource.data.length; i++) {
          if (this.dataSource.data[i].Dependent_Status_ID == 1) {
            this.dataSource.data[i].status_name = 'Active'
          } else {
            this.dataSource.data[i].status_name = 'InActive'
          }
        }
      } else {
        this.snackBar.open(response.errorMessage, "Ok", { duration: 3000 });
      }
    })
  }



}


