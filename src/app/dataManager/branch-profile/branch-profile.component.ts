import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router, ActivatedRoute } from '@angular/router';
import { DdlistService } from 'app/services/config/ddlist.service';
import { BranchService } from 'app/services/dataManager/branch.service';

@Component({
  selector: 'app-branch-profile',
  templateUrl: './branch-profile.component.html',
  styleUrls: ['./branch-profile.component.css']
})
export class BranchProfileComponent implements OnInit {

  branchForm: FormGroup;
  toptitle: string = 'Add Branch';
  btntext: string = 'Save';
  urlText: any;
  isDisabled: boolean = true;
  obj = localStorage.getItem('currentuser');
  statusList = new Array();

  constructor(private fb: FormBuilder, private ddlist: DdlistService, private branchser: BranchService, private snackBar: MatSnackBar, private router: Router, private activatedRoute: ActivatedRoute) {
    const url = this.router.url;
    this.urlText = url.split('/');
    console.log(this.urlText);

    this.branchForm = this.fb.group({
      Branch_ID: 0,
      Branch_Code: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      Branch_Description: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      Branch_Status_ID: new FormControl({ value: 1, disabled: this.urlText[3] == 'view' ? true : false }),
      User_ID: JSON.parse(this.obj).User_ID,
    });
    this.activatedRoute.params.subscribe(params => {
      if (params['Branch_ID'] != 'create') {
        this.toptitle = 'Edit Branch';
        this.btntext = 'Update';
        this.branchser.editBranchMaster(params['Branch_ID']).subscribe(response => {
          if (response.success) {
            this.branchForm.patchValue({
              Branch_ID: response.data[0].Branch_ID,
              Branch_Code: response.data[0].Branch_Code,
              Branch_Description: response.data[0].Branch_Description,
              Branch_Status_ID: response.data[0].Branch_Status_ID,
              User_ID: JSON.parse(this.obj).User_ID,
            });
          } else {
            this.snackBar.open(response.errorMessage, 'OK');
          }
        });

      }
    });
  }

  ngOnInit() {
    this.getStatusDDLists();
    if (this.urlText[3] == 'view') {
      this.toptitle = "View Branch Detail";
      this.isDisabled = false;
    }
  }

  getStatusDDLists() {
    this.ddlist.getStatusDDList().subscribe(response => {
      if (response.success) {
        this.statusList = response.data;
      } else {
        this.snackBar.open(response.errorMessage, 'OK', { duration: 3000 }); (response.errorMessage, "Ok");
      }
    });
  }

  addMasterTPASetup() {
    if (this.branchForm.valid) {
      this.branchser.addBranchMaster(JSON.stringify(this.branchForm.value)).subscribe(response => {
        if (response.success) {
          this.snackBar.open('Branch Master Saved Successfully', 'OK', {
            duration: 3000
          });
          this.branchForm.reset();
          this.router.navigate(['/dataManager/branch']);
        } else {
          this.snackBar.open(response.errorMessage, 'OK', {
            duration: 3000
          });
        }
      });
    } else {
      this.validateAllFormFields(this.branchForm);
      this.snackBar.open('Form cannot Invalid', 'OK', {
        duration: 3000
      });
    }
  }

  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach((field) => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({
          onlySelf: true
        });
      } else if (control instanceof FormGroup) { this.validateAllFormFields(control); }
    });
  }


}
