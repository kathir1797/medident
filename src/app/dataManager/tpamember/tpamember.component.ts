import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { tpaMemberRequest } from 'app/models/dataManager';
import { MatSnackBar } from '@angular/material/snack-bar';
import { TpamemberService } from 'app/services/dataManager/tpamember.service';

@Component({
  selector: 'app-tpamember',
  templateUrl: './tpamember.component.html',
  styleUrls: ['./tpamember.component.css']
})
export class TPAmemberComponent implements AfterViewInit,OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  dataSource = new MatTableDataSource<any>();
  length: number
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  status_name: string;
  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['TPA_ID', 'Company_Name', 'Employee_Name','No_of_Members','status_name', 'Action'];

  tpaMemberData: tpaMemberRequest = new tpaMemberRequest();
  constructor(private tpaMember: TpamemberService, private snackBar: MatSnackBar) { }

  ngOnInit() {
    this.tpaMemberData.TPA_Member_ID = 0;
    this.getTPAMemberList();
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  getTPAMemberList() {
    this.tpaMember.getTPAMember(this.tpaMemberData).subscribe(response => {
      if (response.success) {
        this.dataSource.data = response.data;
        this.dataSource.sort = this.sort;
        this.length = response.data.length;
        for (var i = 0; i < this.dataSource.data.length; i++) {
          if (this.dataSource.data[i].TPA_Member_Status_ID == 1) {
            this.dataSource.data[i].status_name = 'Active'
          } else {
            this.dataSource.data[i].status_name = 'InActive'
          }
        }
      } else {
        this.snackBar.open(response.errorMessage, "Ok", { duration: 3000 });
      }
    });
  }
}

