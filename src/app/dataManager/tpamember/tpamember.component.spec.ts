import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TPAmemberComponent } from './tpamember.component';

describe('TPAmemberComponent', () => {
  let component: TPAmemberComponent;
  let fixture: ComponentFixture<TPAmemberComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TPAmemberComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TPAmemberComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
