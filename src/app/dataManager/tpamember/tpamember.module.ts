import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TpamemberRoutingModule } from './tpamember-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    TpamemberRoutingModule
  ]
})
export class TpamemberModule { }
