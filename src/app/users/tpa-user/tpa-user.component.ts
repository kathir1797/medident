import { Router } from '@angular/router';
import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { TpaUserService } from 'app/services/users/tpa-user.service';

@Component({
  selector: 'app-tpa-user',
  templateUrl: './tpa-user.component.html',
  styleUrls: ['./tpa-user.component.css']
})
export class TpaUserComponent implements OnInit, AfterViewInit {

  @ViewChild(MatPaginator) paginator: MatPaginator;
  dataSource = new MatTableDataSource<any>();
  length: number
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  Status_Name:string;
  displayedColumns = ['User_Name', 'User_Type_Name', 'Status_Name' ,'Action'];

  constructor(private tpaUser: TpaUserService, private snackbar: MatSnackBar, private router: Router) { }

  ngOnInit(): void {
    this.dataSource.paginator = this.paginator
    this.dataSource.sort = this.sort
    this.getALLTPAUSers();
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator
    this.dataSource.sort = this.sort
  }

  getALLTPAUSers() {
    this.tpaUser.getTPAUsers(0).subscribe(response => {
      if (response.success) {
        this.dataSource.data = response.data
        this.dataSource.paginator = this.paginator
        this.dataSource.sort = this.sort
        for (var i = 0; i < this.dataSource.data.length; i++) {
          if (this.dataSource.data[i].Status_ID == 1) {
            this.dataSource.data[i].Status_Name = 'Active'
          } else {
            this.dataSource.data[i].Status_Name = 'InActive'
          }
        }
      } else {
        this.snackbar.open(response.errorMessage, 'ok', { duration: 3000 });
        this.dataSource.data = []
        this.dataSource.paginator = this.paginator
        this.dataSource.sort = this.sort
      }
    })
  }

  onActionClick(type: string, record: any) {
    if(type === 'edit'){
      this.router.navigate(['/tpa-users/user', record.User_ID])
    }
    if(type === 'view'){
      this.router.navigate(['/tpa-users/user/view', record.User_ID])
    }
    
  }

}
