import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { DdlistService } from 'app/services/config/ddlist.service';
import { TpaUserService } from 'app/services/users/tpa-user.service';
import { MatDialog } from '@angular/material/dialog';
import { AssignToClinicComponent } from '../assign-to-clinic/assign-to-clinic.component';

@Component({
  selector: 'app-add-tpa-user',
  templateUrl: './add-tpa-user.component.html',
  styleUrls: ['./add-tpa-user.component.css']
})
export class AddTpaUserComponent implements OnInit {

  urlText: any;
  obj = localStorage.getItem('currentuser');
  statusList = new Array();
  toptitle: string = 'Add TPA User';
  btntext: string = 'Save';
  isDisabled: boolean = true;
  tpaUserForm: FormGroup;
  userTypeList = new Array();
  assignButton: boolean = false;
  clinicUsersList = new Array();

  showHide: boolean = false

  constructor(private fb: FormBuilder, private router: Router, private snackBar: MatSnackBar, private dialog: MatDialog,
    private ddlist: DdlistService, private tpauser: TpaUserService, private activated: ActivatedRoute) {
    const url = this.router.url;
    this.urlText = url.split('/');
    console.log(this.urlText)
    this.initializeUserForm();
    this.activated.params.subscribe(param => {
      if (this.urlText[3] != undefined) {
        this.toptitle = 'Edit TPA User'
        this.btntext = 'Update';
        this.tpauser.getTPAUsers(param.User_ID).subscribe(response => {
          if (response.success) {
            console.log('TPAUSER', response.data)
            if (response.data[0].User_Type_ID === 2) {
              this.assignButton = true;
            }
            this.tpaUserForm.patchValue({
              User_ID: response.data[0].User_ID,
              User_Name: response.data[0].User_Name,
              User_Password: response.data[0].User_Password,
              User_Type_ID: response.data[0].User_Type_ID,
              User_Status_ID: response.data[0].Status_ID,
            })
          } else {
            this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 })
          }
        })
      }
      if (this.urlText[3] === 'view') {
        this.toptitle = 'TPA User Detail'
        this.isDisabled = false
        this.assignButton = false
      }
    })

  }

  ngOnInit(): void {
    this.getStatusDDLists();
    this.getUserType();
  }

  initializeUserForm() {
    this.tpaUserForm = this.fb.group({
      User_ID: 0,
      User_Name: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      User_Password: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      User_Type_ID: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      User_Status_ID: new FormControl({ value: 1, disabled: this.urlText[3] == 'view' ? true : false }),
    })
  }

  getStatusDDLists() {
    this.ddlist.getStatusDDList().subscribe(response => {
      if (response.success) {
        this.statusList = response.data;
      } else {
        this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 })
      }
    });
  }

  addTPAUser() {
    this.tpauser.addUsers(JSON.stringify(this.tpaUserForm.value)).subscribe(response => {
      if (response.success) {
        this.snackBar.open('User Saved Successfully', 'OK', { duration: 3000 })
        this.tpaUserForm.reset();
        this.router.navigate(['/tpa-users'])
      } else {
        this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 })
      }
    });
  }

  getUserType() {
    this.tpauser.getTPAUserType(0).subscribe(response => {
      if (response.success) {
        this.userTypeList = response.data;
      } else {
        this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 })
      }
    })
  }

  getTPAUSerByID(userID) {
    this.tpauser.getTPAUsers(userID).subscribe(response => {
      if (response.success) {
        this.tpaUserForm.patchValue({
          User_ID: response.data[0].User_ID,
          User_Name: response.data[0].User_Name,
          User_Password: response.data[0].User_Password,
          User_Type_ID: response.data[0].User_Type_ID,
          User_Status_ID: response.data[0].Status_ID,
        })
      } else {
        this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 })
      }
    })
  }

  assignToClinic() {
    this.tpauser.getUserClinic(0).subscribe(response => {
      if (response.success) {
        console.log('TPAUSERCLINIC->', response.data)
        this.clinicUsersList = response.data.filter(x => {
          if (x.User_ID == this.tpaUserForm.get('User_ID').value) {
            return x;
          } else {
            return
          }
        });
        console.log('FilterData:', this.clinicUsersList[0].Clinic_ID)
        this.dialog.open(AssignToClinicComponent, {
          data:
          {
            User_ID: this.tpaUserForm.get('User_ID').value,
            Clinic_ID: this.clinicUsersList[0].Clinic_ID,
            Clinic_Status_ID: this.clinicUsersList[0].User_Clinic_Status_ID
          },
          hasBackdrop: false,
        })
      } else {
        this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 })
      }
    })

  }

  OnUserTypeChange(event) {
    if (event == 2) {
      this.assignButton = true
    } else {
      this.assignButton = false
    }
  }

}
