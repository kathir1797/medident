import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TpaUserComponent } from './tpa-user.component';

describe('TpaUserComponent', () => {
  let component: TpaUserComponent;
  let fixture: ComponentFixture<TpaUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TpaUserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TpaUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
