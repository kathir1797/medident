import {MatDialogModule} from '@angular/material/dialog';
import { AddTpaUserTypeComponent } from './../tpa-user-type/add-tpa-user-type/add-tpa-user-type.component';
import { MatTableModule } from '@angular/material/table';
import { ComponentsModule } from './../../components/components.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TpaUserRoutingModule } from './tpa-user-routing.module';
import { TpaUserComponent } from './tpa-user.component';
import { AddTpaUserComponent } from './add-tpa-user/add-tpa-user.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatRippleModule, MatNativeDateModule } from '@angular/material/core';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatRadioModule } from '@angular/material/radio';
import { MatSelectModule } from '@angular/material/select';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatSortModule } from '@angular/material/sort';
import { MatTooltipModule } from '@angular/material/tooltip';
import { BreadcrumbModule } from 'angular-crumbs';
import { TpaUserTypeComponent } from '../tpa-user-type/tpa-user-type.component';
import { AssignToClinicComponent } from './assign-to-clinic/assign-to-clinic.component';


@NgModule({
  declarations: [TpaUserComponent, AddTpaUserComponent, TpaUserTypeComponent, AddTpaUserTypeComponent, AssignToClinicComponent],
  entryComponents: [AssignToClinicComponent],
  imports: [
    CommonModule,
    TpaUserRoutingModule,
    ComponentsModule,
    FormsModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatRippleModule,
    MatNativeDateModule,
    MatInputModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatTooltipModule,
    MatSnackBarModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatIconModule,
    MatDatepickerModule,
    MatCardModule,
    MatCheckboxModule,
    MatSidenavModule,
    BreadcrumbModule,
    MatRadioModule,
    MatCardModule,
    MatListModule,
    MatDialogModule
  ]
})
export class TpaUserModule { }
