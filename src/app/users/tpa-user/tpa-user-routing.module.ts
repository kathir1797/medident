import { AddTpaUserTypeComponent } from './../tpa-user-type/add-tpa-user-type/add-tpa-user-type.component';
import { TpaUserTypeComponent } from './../tpa-user-type/tpa-user-type.component';
import { AddTpaUserComponent } from './add-tpa-user/add-tpa-user.component';
import { TpaUserComponent } from './tpa-user.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {
    path: '',
    component: TpaUserComponent
  },
  {
    path: 'adduser',
    component: AddTpaUserComponent
  },
  {
    path: 'userTypes',
    component: TpaUserTypeComponent
  },
  {
    path: 'addusertype',
    component: AddTpaUserTypeComponent
  },
  {
    path: 'addusertype/:User_Type_ID',
    component: AddTpaUserTypeComponent
  },
  {
    path: 'user/:User_ID',
    component: AddTpaUserComponent
  },
  {
    path: 'user/view/:User_ID',
    component: AddTpaUserComponent
  },
  {
    path: 'addusertype/view/:User_Type_ID',
    component: AddTpaUserTypeComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TpaUserRoutingModule { }
