import { TpaUserService } from 'app/services/users/tpa-user.service';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { clinicsRequest } from 'app/models/dataManager';
import { ClinicsService } from './../../../services/dataManager/clinics.service';
import { Component, Inject, OnInit } from '@angular/core';
import { DdlistService } from 'app/services/config/ddlist.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-assign-to-clinic',
  templateUrl: './assign-to-clinic.component.html',
  styleUrls: ['./assign-to-clinic.component.css']
})
export class AssignToClinicComponent implements OnInit {

  clinicsData: clinicsRequest = new clinicsRequest();
  clinicsList = new Array();
  statusList = new Array();
  userClinicForm: FormGroup;

  constructor(private tpauser: TpaUserService,
    private ddlist: DdlistService, private clinics: ClinicsService,
    private snackBar: MatSnackBar, private fb: FormBuilder, @Inject(MAT_DIALOG_DATA) public data: any) {
    this.initializeClinicUserForm();
    if (this.data != null) {
      this.userClinicForm.patchValue({
        Clinic_User_ID: this.data.User_ID,
        Clinic_ID: this.data.Clinic_ID,
        User_Clinic_Status_ID: this.data.Clinic_Status_ID,
      })
    }
  }

  ngOnInit(): void {
    this.clinicsData.Clinic_ID = 0
    this.getClinics();
    this.getStatusList();
  }

  getClinics() {
    this.clinics.getClinicsList(this.clinicsData).subscribe(response => {
      if (response.success) {
        this.clinicsList = response.data;
      } else {
        this.snackBar.open(response.errorMessage, "Ok", { duration: 3000 });
      }
    });
  }


  getStatusList() {
    this.ddlist.getStatusDDList().subscribe(response => {
      if (response.success) {
        this.statusList = response.data;
      } else {
        this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 })
      }
    });
  }

  initializeClinicUserForm() {
    this.userClinicForm = this.fb.group({
      User_Clinic_ID: 0,
      Clinic_User_ID: new FormControl(),
      Clinic_ID: new FormControl(),
      User_ID: JSON.parse(localStorage.getItem('currentuser')).User_ID,
      User_Clinic_Status_ID: 1,
    })
  }

  assignClinicUser() {
    this.tpauser.addUserClinic(JSON.stringify(this.userClinicForm.value)).subscribe(response => {
      if (response.success) {
        this.snackBar.open('User Assigned To Clinic Successfully', 'ok', { duration: 3000 })
      } else {
        this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 })
      }
    })
  }

}