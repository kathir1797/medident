import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TpaUserTypeComponent } from './tpa-user-type.component';

describe('TpaUserTypeComponent', () => {
  let component: TpaUserTypeComponent;
  let fixture: ComponentFixture<TpaUserTypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TpaUserTypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TpaUserTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
