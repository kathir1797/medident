import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { TpaUserService } from 'app/services/users/tpa-user.service';

@Component({
  selector: 'app-tpa-user-type',
  templateUrl: './tpa-user-type.component.html',
  styleUrls: ['./tpa-user-type.component.css']
})
export class TpaUserTypeComponent implements OnInit, AfterViewInit {

  @ViewChild(MatPaginator) paginator: MatPaginator;
  dataSource = new MatTableDataSource<any>();
  length: number
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  Status_Name: string;
  displayedColumns = ['User_Type_Name', 'Status_Name', 'Action'];

  constructor(private snackBar: MatSnackBar, private tpauser: TpaUserService) { }

  ngOnInit(): void {
    this.dataSource.paginator = this.paginator
    this.dataSource.sort = this.sort;
    this.getUserType();
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator
    this.dataSource.sort = this.sort;
  }

  getUserType() {
    this.tpauser.getTPAUserType(0).subscribe(response => {
      if (response.success) {
        this.dataSource.data = response.data;
        this.dataSource.paginator = this.paginator
        this.dataSource.sort = this.sort;
      } else {
        this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 })
        this.dataSource.data = [];
        this.dataSource.paginator = this.paginator
        this.dataSource.sort = this.sort;
      }
    })
  }

}
