import { AssignToClinicComponent } from './../../tpa-user/assign-to-clinic/assign-to-clinic.component';
import { MatDialog } from '@angular/material/dialog';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { DdlistService } from 'app/services/config/ddlist.service';
import { TpaUserService } from 'app/services/users/tpa-user.service';

@Component({
  selector: 'app-add-tpa-user-type',
  templateUrl: './add-tpa-user-type.component.html',
  styleUrls: ['./add-tpa-user-type.component.css']
})
export class AddTpaUserTypeComponent implements OnInit {

  urlText: any;
  obj = localStorage.getItem('currentuser');
  statusList = new Array();
  toptitle: string = 'Add TPA UserType';
  btntext: string = 'Save';
  isDisabled: boolean = true;
  tpaUserTypeForm: FormGroup;

  constructor(private fb: FormBuilder, private router: Router, private snackBar: MatSnackBar, 
    private ddlist: DdlistService, private tpauser: TpaUserService, private activated: ActivatedRoute) {
    const url = this.router.url;
    this.urlText = url.split('/');
    this.initializeUserForm();
    this.activated.params.subscribe(param => {
      if (this.urlText[3] != undefined) {
        this.toptitle = 'Edit TPA User Type'
        this.btntext = 'Update'
        this.getUserTypeByID(param.User_Type_ID);
      }
      if (this.urlText[3] === 'view') {
        this.toptitle = 'TPA UserType Detail'
        this.isDisabled = false
      }
    })
  }

  ngOnInit(): void {
    this.getStatusDDLists()
  }

  initializeUserForm() {
    this.tpaUserTypeForm = this.fb.group({
      User_ID: JSON.parse(this.obj).User_ID,
      User_Type_Name: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      User_Type_ID: 0,
      User_Type_Status_ID: new FormControl({ value: 1, disabled: this.urlText[3] == 'view' ? true : false }),
    })
  }

  getStatusDDLists() {
    this.ddlist.getStatusDDList().subscribe(response => {
      if (response.success) {
        this.statusList = response.data;
      } else {
        this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 })
      }
    });
  }

  addUsertype() {
    this.tpauser.addTPAUserType(JSON.stringify(this.tpaUserTypeForm.value)).subscribe(response => {
      if (response.success) {
        this.tpaUserTypeForm.reset();
        this.snackBar.open('UserType Saved Successfully', 'OK', { duration: 3000 })
      } else {
        this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 })
      }
    });
  }

  getUserTypeByID(userTypeID) {
    this.tpauser.getTPAUserType(userTypeID).subscribe(response => {
      if (response.success) {
        this.tpaUserTypeForm.patchValue(response.data[0]);
      } else {
        this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 })
      }
    })
  }

  

}
