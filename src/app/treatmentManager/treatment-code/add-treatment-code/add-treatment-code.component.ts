import { TreatmentGroupService } from './../../../services/treatmentManager/treatment-group.service';
import { TreatmentCodeService } from './../../../services/treatmentManager/treatment-code.service';
import { Router, ActivatedRoute } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { treatmentCodeRequest, treatmentGroup } from 'app/models/treatmentManager';
import { DdlistService } from 'app/services/config/ddlist.service';

@Component({
  selector: 'app-add-treatment-code',
  templateUrl: './add-treatment-code.component.html',
  styleUrls: ['./add-treatment-code.component.css']
})
export class AddTreatmentCodeComponent implements OnInit {

  treatmentCodeForm: FormGroup;
  urlText: any;
  obj = localStorage.getItem('currentuser');
  statusList = new Array();
  toptitle: string = 'Add Treatment Code';
  btntext: string = 'Save';
  isDisabled: boolean = true;
  treatmentCodeReq: treatmentCodeRequest = new treatmentCodeRequest();
  treatmentgroupReq: treatmentGroup = new treatmentGroup();
  treatmentGroupList = new Array();

  constructor(private fb: FormBuilder, private snackBar: MatSnackBar, private router: Router,
    private ddlist: DdlistService, private treatmentCode: TreatmentCodeService,
    private activate: ActivatedRoute, private treatmentGroupService: TreatmentGroupService) {
    const url = this.router.url;
    this.urlText = url.split('/');
    this.initializeTreatmentCodeForm();
    this.activate.params.subscribe(params => {
      if (this.urlText[3] != 'add') {
        this.toptitle = 'Edit Treatment Code';
        this.btntext = 'Update';
        this.treatmentCodeReq.Treatment_Code_ID = params['Treatment_Code_ID'];
        this.getTreatmentCodeByID(this.treatmentCodeReq)
      }
      if (this.urlText[3] === 'view') {
        this.isDisabled = false;
        this.toptitle = 'View Treatment Code Detail';
      }
    })
  }

  ngOnInit(): void {
    this.getStatusDDLists();
    this.treatmentgroupReq.Treatment_Group_ID = 0;
    this.getTreatmentGroup();
  }

  initializeTreatmentCodeForm() {
    this.treatmentCodeForm = this.fb.group({
      Treatment_Code_ID: 0,
      Treatment_Code: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      Treatment_Rate: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      Treatment_Group_ID: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      Start_Date: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      End_Date: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      Status_ID: new FormControl({ value: 1, disabled: this.urlText[3] == 'view' ? true : false }),
      Remarks: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      User_ID: JSON.parse(this.obj).User_ID
    })
  }

  getStatusDDLists() {
    this.ddlist.getStatusDDList().subscribe(response => {
      if (response.success) {
        this.statusList = response.data;
      } else {
        this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 })
      }
    });
  }

  addTreatmentCode() {
    this.treatmentCode.addTreatmentCode(this.treatmentCodeForm.getRawValue()).subscribe(response => {
      if (response.success) {
        this.treatmentCodeForm.reset();
        this.router.navigate(['/treatmentManager/treatmentCode']);
        this.snackBar.open('Treatment Code Saved Successfully', 'ok', { duration: 3000 })
      } else {
        this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 })
      }
    });
  }

  getTreatmentCodeByID(treatmentCode) {
    this.treatmentCode.getTreatmentCode(treatmentCode).subscribe(response => {
      if (response.success) {
        this.treatmentCodeForm.patchValue({
          Treatment_Code_ID: response.data[0].Treatment_Code_ID,
          Treatment_Code: response.data[0].Treatment_Code,
          Treatment_Rate: response.data[0].Treatment_Rate,
          Treatment_Group_ID: response.data[0].Treatment_Group_ID,
          Start_Date: response.data[0].Start_Date,
          End_Date: response.data[0].End_Date,
          Status_ID: response.data[0].Treatment_Code_Status_ID,
          Remarks: response.data[0].Remarks,
          User_ID: JSON.parse(this.obj).User_ID
        })
      } else {
        this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 })
      }
    })
  }

  getTreatmentGroup() {
    this.treatmentGroupService.getTreatmentGroup(this.treatmentgroupReq).subscribe(response => {
      if (response.success) {
        this.treatmentGroupList = response.data
      } else {
        this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 })
      }
    })
  }

}
