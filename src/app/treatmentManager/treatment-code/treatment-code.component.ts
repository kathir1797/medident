import { AfterViewInit } from '@angular/core';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { treatmentCodeRequest } from 'app/models/treatmentManager';
import { TreatmentCodeService } from 'app/services/treatmentManager/treatment-code.service';

@Component({
  selector: 'app-treatment-code',
  templateUrl: './treatment-code.component.html',
  styleUrls: ['./treatment-code.component.css']
})
export class TreatmentCodeComponent implements OnInit, AfterViewInit {

  @ViewChild(MatPaginator) paginator: MatPaginator;
  dataSource = new MatTableDataSource<any>();
  length: number
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  treatmentCodeReq: treatmentCodeRequest = new treatmentCodeRequest();

  displayedColumns = ['Treatment_Code', 'Treatment_Group_Description', 'Treatment_Rate', 'Start_Date', 'End_Date', 'Remarks', 'Status_Name', 'Action'];

  constructor(private router: Router, private snackBar: MatSnackBar, private treatmentCode: TreatmentCodeService) { }

  ngOnInit(): void {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    this.treatmentCodeReq.Treatment_Code_ID = 0;
    this.getTreatmentCode()
  }

  ngAfterViewInit(): void {
    this.dataSource.paginator = this.paginator
    this.dataSource.sort = this.sort
  }

  getTreatmentCode() {
    this.treatmentCode.getTreatmentCode(this.treatmentCodeReq).subscribe(response => {
      if (response.success) {
        this.dataSource.data = response.data
      } else {
        this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 })
      }
    })
  }

}
