import { treatmentCategory, treatmentGroup } from './../../../models/treatmentManager';
import { Router, ActivatedRoute } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { TreatmentGroupService } from 'app/services/treatmentManager/treatment-group.service';
import { DdlistService } from 'app/services/config/ddlist.service';
import { TreatmentCategoryService } from 'app/services/treatmentManager/treatment-category.service';

@Component({
  selector: 'app-add-treatment-group',
  templateUrl: './add-treatment-group.component.html',
  styleUrls: ['./add-treatment-group.component.css']
})
export class AddTreatmentGroupComponent implements OnInit {

  urlText: any;
  obj = localStorage.getItem('currentuser');
  statusList = new Array();
  treatmentCodeList = new Array();
  clinicsList = new Array();
  toptitle: string = 'Add Treatment Group';
  btntext: string = 'Save';
  isDisabled: boolean = true;
  treatmentGroup: FormGroup;
  treatmentCategoryList = new Array()



  treatmentGroupReq: treatmentGroup = new treatmentGroup();
  treatmentCategoryReq: treatmentCategory = new treatmentCategory();

  constructor(private fb: FormBuilder, private snackBar: MatSnackBar, private router: Router,
    private activatedRoute: ActivatedRoute, private ddlist: DdlistService,
    private treatmentGrp: TreatmentGroupService, private category: TreatmentCategoryService) {
    const url = this.router.url;
    this.urlText = url.split('/');
    this.initializeTreatmentGroup();
    this.activatedRoute.params.subscribe(param => {
      if (this.urlText[3] != 'add') {
        this.toptitle = 'Update Treatment Group';
        this.btntext = 'Update';
        this.treatmentGroupReq.Treatment_Group_ID = param.Treatment_Group_ID;
        this.editTreatmentGroup(this.treatmentGroupReq);
      }
      if (this.urlText[3] === 'view') {
        this.isDisabled = false;
        this.toptitle = 'Treatment Group Detail'
      }
    })
  }

  ngOnInit(): void {
    this.getStatusDDLists();
    this.treatmentCategoryReq.Treatment_Category_ID = 0;
    this.getTreatmentCategory();
  }

  initializeTreatmentGroup() {
    this.treatmentGroup = this.fb.group({
      Treatment_Group_ID: 0,
      Treatment_Group_Code: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      Treatment_Group_Description: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      Sort_Order: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      Treatment_Category_ID: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      Treatment_Group_Status_ID: new FormControl({ value: 1, disabled: this.urlText[3] == 'view' ? true : false }),
      User_ID: JSON.parse(this.obj).User_ID,
      Associated_Code: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      Extraction_YN: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
    })
  }

  getStatusDDLists() {
    this.ddlist.getStatusDDList().subscribe(response => {
      if (response.success) {
        this.statusList = response.data;
      } else {
        this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 })
      }
    });
  }

  addTreatmentGroup() {
    this.treatmentGrp.addTreatmentGroup(this.treatmentGroup.getRawValue()).subscribe(response => {
      if (response.success) {
        this.treatmentGroup.reset();
        this.router.navigate(['/treatmentManager/treatmentGroup']);
        this.snackBar.open('TreatmentGroup Saved Successfully', 'OK', { duration: 3000 })
      } else {
        this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 })
      }
    });
  }

  editTreatmentGroup(treatmentgroup) {
    this.treatmentGrp.getTreatmentGroup(treatmentgroup).subscribe(response => {
      if (response.success) {
        this.treatmentGroup.patchValue({
          Treatment_Group_ID: response.data[0].Treatment_Group_ID,
          Treatment_Group_Code: response.data[0].Treatment_Group_Code,
          Treatment_Group_Description: response.data[0].Treatment_Group_Description,
          Sort_Order: response.data[0].Sort_Order,
          Treatment_Category_ID: response.data[0].Treatment_Category_ID,
          Treatment_Group_Status_ID: response.data[0].Treatment_Group_Status_ID,
          Associated_Code: response.data[0].Associated_Code,
          Extraction_YN: response.data[0].Extraction_YN,
          User_ID: JSON.parse(this.obj).User_ID
        })
      } else {
        this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 })
      }
    })
  }

  getTreatmentCategory() {
    this.category.getTreatmentCategory(this.treatmentCategoryReq).subscribe(response => {
      if (response.success) {
        this.treatmentCategoryList = response.data;
        this.treatmentCategoryList = this.treatmentCategoryList.filter(value => {
          if (value.Treatment_Category_Status_ID == 1) {
            return value
          }
        })
      } else {
        this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 })
      }
    })
  }

  onExtractionEvent(event) {
    this.treatmentGroup.get('Extraction_YN').patchValue(
      event.checked ? 'Y' : 'N'
    )
  }

}
