import { treatmentGroup } from './../../models/treatmentManager';
import { MatSort } from '@angular/material/sort';
import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { MatSnackBar } from '@angular/material/snack-bar';
import { TreatmentGroupService } from 'app/services/treatmentManager/treatment-group.service';

@Component({
  selector: 'app-treatment-group',
  templateUrl: './treatment-group.component.html',
  styleUrls: ['./treatment-group.component.css']
})
export class TreatmentGroupComponent implements OnInit, AfterViewInit {

  @ViewChild(MatPaginator) paginator: MatPaginator;
  dataSource = new MatTableDataSource<any>();
  length: number
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  treatmentgroupReq: treatmentGroup = new treatmentGroup();

  displayedColumns = ["Category_Number","Treatment_Group_Code", "Treatment_Group_Description", "Status_Name", "Action"]

  constructor(private snackBar: MatSnackBar, private treatmentGrp: TreatmentGroupService) { }

  ngOnInit(): void {
    this.dataSource.paginator = this.paginator
    this.dataSource.sort = this.sort
    this.treatmentgroupReq.Treatment_Group_ID = 0
    this.getTreatmentGroup();
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator
    this.dataSource.sort = this.sort
  }

  getTreatmentGroup() {
    this.treatmentGrp.getTreatmentGroup(this.treatmentgroupReq).subscribe(response => {
      if (response.success) {
        this.dataSource.data = response.data;
      } else {
        this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 })
      }
    });
  }

}
