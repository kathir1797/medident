import { treatmentCodeByClinic } from './../../models/treatmentManager';
import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { TreatmentCodeService } from 'app/services/treatmentManager/treatment-code.service';

@Component({
  selector: 'app-treatment-code-clinics',
  templateUrl: './treatment-code-clinics.component.html',
  styleUrls: ['./treatment-code-clinics.component.css']
})
export class TreatmentCodeClinicsComponent implements OnInit, AfterViewInit {

  @ViewChild(MatPaginator) paginator: MatPaginator;
  dataSource = new MatTableDataSource<any>();
  length: number
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  treatmentCodeClinicsReq: treatmentCodeByClinic = new treatmentCodeByClinic();

  displayedColumns = ["Treatment_Code", "Clinic_Name", "Treatment_Rate", "Start_Date", "End_Date", "Status_Name", "Action"]

  constructor(private router: Router, private snackBar: MatSnackBar, private treatmentCode: TreatmentCodeService) { }

  ngOnInit(): void {
    this.treatmentCodeClinicsReq.Treatment_Code_Clinic_ID = 0;
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort
    this.getTreatmentCodeClinics();
  }

  getTreatmentCodeClinics() {
    this.treatmentCode.getTreatmentCodeByClinic(this.treatmentCodeClinicsReq).subscribe(response => {
      if (response.success) {
        this.dataSource.data = response.data
      } else {
        this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 })
      }
    })
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort
  }

}
