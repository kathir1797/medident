import { treatmentCodeByClinic } from './../../../models/treatmentManager';
import { clinicsRequest } from 'app/models/dataManager';
import { treatmentCodeRequest } from 'app/models/treatmentManager';
import { TreatmentCodeService } from 'app/services/treatmentManager/treatment-code.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router, Event, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { DdlistService } from 'app/services/config/ddlist.service';
import { ClinicsService } from 'app/services/dataManager/clinics.service';

@Component({
  selector: 'app-add-treatment-code-clinics',
  templateUrl: './add-treatment-code-clinics.component.html',
  styleUrls: ['./add-treatment-code-clinics.component.css']
})
export class AddTreatmentCodeClinicsComponent implements OnInit {

  urlText: any;
  obj = localStorage.getItem('currentuser');
  statusList = new Array();
  treatmentCodeList = new Array();
  clinicsList = new Array();
  toptitle: string = 'Add TPA Code Price';
  btntext: string = 'Save';
  isDisabled: boolean = true;
  treatmentCodeClinics: FormGroup;


  treatmentCodeReq: treatmentCodeRequest = new treatmentCodeRequest();
  treatmentCodeClinicsReq: treatmentCodeByClinic = new treatmentCodeByClinic()
  clinicsData: clinicsRequest = new clinicsRequest();

  constructor(private fb: FormBuilder, private router: Router, private snackBar: MatSnackBar, private activated: ActivatedRoute,
    private ddlist: DdlistService, private treatmentCode: TreatmentCodeService, private clinics: ClinicsService) {
    const url = this.router.url;
    this.urlText = url.split('/');
    this.initializeTreatmentCodeClincsForm();
    this.activated.params.subscribe(param => {
      if (this.urlText[4] != 'add') {
        this.toptitle = 'Update TPA Code Price';
        this.btntext = 'Update';
        this.treatmentCodeClinicsReq.Treatment_Code_Clinic_ID = param.Treatment_Code_Clinic_ID;
        this.editTreatmentCOdeByClinic(this.treatmentCodeClinicsReq);
      }
      if (this.urlText[4] === 'view') {
        this.toptitle = 'TPA Code Price Detail';
        this.isDisabled = false;
      }
    });
  }

  ngOnInit(): void {
    this.getStatusDDLists();
    this.treatmentCodeReq.Treatment_Code_ID = 0
    this.getTreatmentCode();
    this.clinicsData.Clinic_ID = 0;
    this.getClinicDD();
  }

  initializeTreatmentCodeClincsForm() {
    this.treatmentCodeClinics = this.fb.group({
      Treatment_Code_Clinic_ID: 0,
      Treatment_Code_ID: new FormControl({ value: '', disabled: this.urlText[4] == 'view' ? true : false }),
      Clinic_ID: new FormControl({ value: '', disabled: this.urlText[4] == 'view' ? true : false }),
      Treatment_Code: new FormControl({ value: '', disabled: this.urlText[4] == 'view' ? true : false }),
      Treatment_Rate: new FormControl({ value: '', disabled: this.urlText[4] == 'view' ? true : false }),
      Start_Date: new FormControl({ value: '', disabled: this.urlText[4] == 'view' ? true : false }),
      End_Date: new FormControl({ value: '', disabled: this.urlText[4] == 'view' ? true : false }),
      Remarks: new FormControl({ value: '', disabled: this.urlText[4] == 'view' ? true : false }),
      Status_ID: new FormControl({ value: 1, disabled: this.urlText[4] == 'view' ? true : false }),
      User_ID: JSON.parse(this.obj).User_ID
    })
  }

  getStatusDDLists() {
    this.ddlist.getStatusDDList().subscribe(response => {
      if (response.success) {
        this.statusList = response.data;
      } else {
        this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 })
      }
    });
  }

  getTreatmentCode() {
    this.treatmentCode.getTreatmentCode(this.treatmentCodeReq).subscribe(response => {
      if (response.success) {
        this.treatmentCodeList = response.data;
        this.treatmentCodeList = this.treatmentCodeList.filter(value => {
          if (value.Treatment_Code_Status_ID == 1) {
            return value
          }
        })
      } else {
        this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 })
      }
    })
  }

  getClinicDD() {
    this.clinics.getClinicsList(this.clinicsData).subscribe(response => {
      if (response.success) {
        this.clinicsList = response.data;
        this.clinicsList = this.clinicsList.filter(value => {
          if(value.Status_ID == 1){
            return value
          }
        })
      } else {
        this.snackBar.open(response.errorMessage, "Ok", { duration: 3000 });
      }
    });
  }

  onTreatmentCodeSelect(event) {
    this.treatmentCodeList.filter(value => {
      if (value.Treatment_Code_ID == event.value) {
        this.treatmentCodeClinics.patchValue({
          Treatment_Code: value.Treatment_Code
        })
      }
    })
  }

  addTreatmentCodeByClinic() {
    this.treatmentCode.addTreatmentCodeByClinics(this.treatmentCodeClinics.getRawValue()).subscribe(response => {
      if (response.success) {
        this.treatmentCodeClinics.reset();
        this.router.navigate(['/treatmentManager/treatmentcode/clinics']);
        this.snackBar.open('Treatment Code Clinics Saved Successfully', 'ok', { duration: 3000 });
      } else {
        this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 })
      }
    });
  }

  editTreatmentCOdeByClinic(treatmentCodeClinicID) {
    this.treatmentCode.getTreatmentCodeByClinic(treatmentCodeClinicID).subscribe(response => {
      if (response.success) {
        this.treatmentCodeClinics.patchValue({
          Treatment_Code_Clinic_ID: response.data[0].Treatment_Code_Clinic_ID,
          Treatment_Code_ID: response.data[0].Treatment_Code_ID,
          Clinic_ID: response.data[0].Clinic_ID,
          Treatment_Code: response.data[0].Treatment_Code,
          Treatment_Rate: response.data[0].Treatment_Rate,
          Start_Date: response.data[0].Start_Date,
          End_Date: response.data[0].End_Date,
          Remarks: response.data[0].Remarks,
          Status_ID: response.data[0].Treatment_Code_Status_ID
        })
      } else {
        this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 })
      }
    });
  }

}
