import { toothCodeRequest } from './../../../models/treatmentManager';
import { ToothCodeService } from './../../../services/treatmentManager/tooth-code.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { JsonPipe } from '@angular/common';
import { DdlistService } from 'app/services/config/ddlist.service';

@Component({
  selector: 'app-add-tooth-code',
  templateUrl: './add-tooth-code.component.html',
  styleUrls: ['./add-tooth-code.component.css']
})
export class AddToothCodeComponent implements OnInit {

  toothCodeForm: FormGroup;
  urlText: any;
  obj = localStorage.getItem('currentuser');
  statusList = new Array();
  toptitle: string = 'Add Tooth Code';
  btntext: string = 'Save';
  isDisabled: boolean = true;
  toothCodeReq: toothCodeRequest = new toothCodeRequest();

  constructor(private fb: FormBuilder, private router: Router, private ddlist: DdlistService,
    private snackBar: MatSnackBar, private toothCode: ToothCodeService, private activated: ActivatedRoute) {
    const url = this.router.url;
    this.urlText = url.split('/');
    console.log(this.urlText)
    this.initializeForm();
    this.activated.params.subscribe(params => {
      if (this.urlText[3] != 'add') {
        this.toptitle = 'Edit Tooth Code';
        this.btntext = 'Update';
        this.toothCodeReq.Tooth_Code_ID = params['Tooth_Code_ID'];
        this.getToothCodeById(this.toothCodeReq);
      }
      if (this.urlText[3] === 'view') {
        this.isDisabled = false;
        this.toptitle = 'View Tooth Code Detail';
      }
    })
  }

  ngOnInit(): void {
    this.getStatusDDLists()
  }

  initializeForm() {
    this.toothCodeForm = this.fb.group({
      Tooth_Code_ID: 0,
      Tooth_Code: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      Remarks: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      Status_ID: new FormControl({ value: 1, disabled: this.urlText[3] == 'view' ? true : false }),
      User_ID: JSON.parse(this.obj).User_ID
    })
  }

  getStatusDDLists() {
    this.ddlist.getStatusDDList().subscribe(response => {
      if (response.success) {
        this.statusList = response.data;
      } else {
        this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 })
      }
    });
  }

  addToothCode() {
    if (this.toothCodeForm.value != '') {
      this.toothCode.addToothCode(this.toothCodeForm.getRawValue()).subscribe(response => {
        if (response.success) {
          this.router.navigate(['/treatmentManager/toothCode']);
          this.toothCodeForm.reset()
        } else {
          this.snackBar.open(response.errorMessage, 'OK', { duration: 3000 })
        }
      });
    } else {
      this.snackBar.open('Form Cannot be Invalid', 'OK', { duration: 3000 })
    }
  }

  getToothCodeById(toothCodeID) {
    this.toothCode.getToothCode(toothCodeID).subscribe(response => {
      if (response.success) {
        this.toothCodeForm.patchValue({
          Tooth_Code_ID: response.data[0].Tooth_Code_ID,
          Tooth_Code: response.data[0].Tooth_Code,
          Remarks: response.data[0].Remarks,
          Status_ID: response.data[0].Tooth_Code_Status_ID,
          User_ID: JSON.parse(this.obj).User_ID
        })
      } else {
        this.snackBar.open(response.errorMessage, 'OK', { duration: 3000 })
      }
    });
  }

}
