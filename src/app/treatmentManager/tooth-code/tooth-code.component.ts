import { toothCodeRequest } from './../../models/treatmentManager';
import { ToothCodeService } from './../../services/treatmentManager/tooth-code.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-tooth-code',
  templateUrl: './tooth-code.component.html',
  styleUrls: ['./tooth-code.component.css']
})
export class ToothCodeComponent implements OnInit, AfterViewInit {

  @ViewChild(MatPaginator) paginator: MatPaginator;
  dataSource = new MatTableDataSource<any>();
  length: number
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  displayedColumns = ['Tooth Code', 'Remarks', 'Status', 'Action'];

  toothCodeReq: toothCodeRequest = new toothCodeRequest();

  constructor(private router: Router, private snackBar: MatSnackBar, private toothCode: ToothCodeService) { }

  ngAfterViewInit(): void {
    this.dataSource.paginator = this.paginator
    this.dataSource.sort = this.sort
  }

  ngOnInit(): void {
    this.toothCodeReq.Tooth_Code_ID = 0;
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    this.getToothCode();
  }

  getToothCode() {
    this.toothCode.getToothCode(this.toothCodeReq).subscribe(response => {
      if (response.success) {
        this.dataSource.data = response.data;
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      } else {
        this.dataSource.data = [];
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 })
      }
    })
  }

}
