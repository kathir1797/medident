
import { treatmentGroup, treatmentCategory } from './../../../models/treatmentManager';
import { Router, ActivatedRoute } from '@angular/router';
import { TreatmentGroupService } from './../../../services/treatmentManager/treatment-group.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { DdlistService } from 'app/services/config/ddlist.service';
import { TreatmentCategoryService } from 'app/services/treatmentManager/treatment-category.service';

@Component({
  selector: 'app-add-treatment-category',
  templateUrl: './add-treatment-category.component.html',
  styleUrls: ['./add-treatment-category.component.css']
})
export class AddTreatmentCategoryComponent implements OnInit {

  urlText: any;
  obj = localStorage.getItem('currentuser');
  statusList = new Array();
  treatmentGroupList = new Array();
  toptitle: string = 'Add Treatment Category';
  btntext: string = 'Save';
  isDisabled: boolean = true;
  treatmentCategory: FormGroup;

  treatmentgroupReq: treatmentGroup = new treatmentGroup();
  treatmentCategoryReq: treatmentCategory = new treatmentCategory();

  constructor(private fb: FormBuilder, private snackBar: MatSnackBar, private treatmentGrp: TreatmentGroupService, private ddlist: DdlistService,
    private router: Router, private activatedRoute: ActivatedRoute, private treatmentCat: TreatmentCategoryService) {
    const url = this.router.url;
    this.urlText = url.split('/');
    console.log(this.urlText)
    this.initializeTreatmentCategoryForm();
    this.treatmentgroupReq.Treatment_Group_ID = 0
    this.activatedRoute.params.subscribe(params => {
      if (this.urlText[3] != 'add') {
        this.toptitle = 'Update Treatment Category';
        this.btntext = 'Update';
        this.treatmentCategoryReq.Treatment_Category_ID = params.Treatment_Category_ID
        this.treatmentCat.getTreatmentCategory(this.treatmentCategoryReq).subscribe(response => {
          if (response.success) {
            this.treatmentCategory.patchValue({
              Treatment_Category_ID: response.data[0].Treatment_Category_ID,
              Treatment_Category_Code: response.data[0].Treatment_Category_Code,
              Remarks: response.data[0].Remarks,
              Category_Number: response.data[0].Category_Number,
              Treatment_Category_Status_ID: response.data[0].Treatment_Category_Status_ID,
              User_ID: JSON.parse(this.obj).User_ID
            })
          } else {
            this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 })
          }
        });
      }
      if (this.urlText[3] == 'view') {
        this.isDisabled = false;
        this.toptitle = 'Treatment Category Detail'
      }
    })
  }

  ngOnInit(): void {
    this.getStatusDDLists();
    this.getTreatmentGroup();
  }

  getStatusDDLists() {
    this.ddlist.getStatusDDList().subscribe(response => {
      if (response.success) {
        this.statusList = response.data;
      } else {
        this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 })
      }
    });
  }

  initializeTreatmentCategoryForm() {
    this.treatmentCategory = this.fb.group({
      Treatment_Category_ID: 0,
      Treatment_Category_Code: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      Remarks: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      Category_Number: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      Treatment_Category_Status_ID: new FormControl({ value: 1, disabled: this.urlText[3] == 'view' ? true : false }),
      User_ID: JSON.parse(this.obj).User_ID
    })
  }

  getTreatmentGroup() {
    this.treatmentGrp.getTreatmentGroup(this.treatmentgroupReq).subscribe(response => {
      if (response.success) {
        this.treatmentGroupList = response.data;
      } else {
        this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 })
      }
    });
  }

  addTreatmentCategory() {
    this.treatmentCat.addTreatmentCategory(this.treatmentCategory.getRawValue()).subscribe(response => {
      if (response.success) {
        this.router.navigate(['/treatmentManager/treatmentCategory']);
        this.treatmentCategory.reset();
        this.snackBar.open('Treatment Category Saved Successfully', 'OK', { duration: 3000 })
      } else {
        this.snackBar.open(response.errorMessage, 'OK', { duration: 3000 })
      }
    });
  }

  editTreatmentCategory(treatmentCategory) {
    this.treatmentCat.getTreatmentCategory(treatmentCategory).subscribe(response => {
      if (response.success) {
        this.treatmentCategory.patchValue({
          Treatment_Category_ID: response.data[0].Treatment_Category_ID,
          Treatment_Category_Code: response.data[0].Treatment_Category_Code,
          Remarks: response.data[0].Treatment_Group_ID,
          Category_Number: response.data[0].Category_Number,
          Treatment_Category_Status_ID: response.data[0].Treatment_Category_Status_ID,
          User_ID: JSON.parse(this.obj).User_ID
        })
      } else {
        this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 })
      }
    });
  }

}
