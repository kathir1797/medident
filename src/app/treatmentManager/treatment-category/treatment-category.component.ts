
import { MatSnackBar } from '@angular/material/snack-bar';
import { treatmentCategory } from './../../models/treatmentManager';
import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { TreatmentCategoryService } from 'app/services/treatmentManager/treatment-category.service';

@Component({
  selector: 'app-treatment-category',
  templateUrl: './treatment-category.component.html',
  styleUrls: ['./treatment-category.component.css']
})
export class TreatmentCategoryComponent implements OnInit, AfterViewInit {

  @ViewChild(MatPaginator) paginator: MatPaginator;
  dataSource = new MatTableDataSource<any>();
  length: number
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  treatmentCategoryReq: treatmentCategory = new treatmentCategory();

  displayedColumns = ["Category_Number","Remarks", "Status_Name", "Action"]

  constructor(private snackbar: MatSnackBar, private category: TreatmentCategoryService) { }

  ngOnInit(): void {
    this.dataSource.paginator = this.paginator
    this.dataSource.sort = this.sort
    this.treatmentCategoryReq.Treatment_Category_ID = 0;
    this.getTreatmentCategory();
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator
    this.dataSource.sort = this.sort
  }

  getTreatmentCategory() {
    this.category.getTreatmentCategory(this.treatmentCategoryReq).subscribe(response => {
      if (response.success) {
        this.dataSource.data = response.data
      } else {
        this.snackbar.open(response.errorMessage, 'ok', { duration: 3000 })
      }
    });
  }

}
