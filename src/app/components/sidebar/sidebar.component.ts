import { Component, ElementRef, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { NavigationItem, NavItem } from '../menu';
import { MenuService } from '../menu.service';

declare const $: any;
declare interface RouteInfo {
  path: string;
  title: string;
  icon: string;
  class: string;
  type: string;
}
export const ROUTES: RouteInfo[] = [
  { path: '/dashboard', title: 'Dashboard', icon: 'dashboard', class: '', type: 'menu' },

  { path: '/nil', title: 'Insurance Company', icon: 'bubble_chart', class: '', type: 'menu' },
  { path: '/provider', title: 'Providers', icon: 'bubble_chart', class: '', type: 'menu' },

  { path: '/dataManager/clinic', title: 'All Clinics', icon: 'bubble_chart', class: '', type: 'menu' },
  { path: '/nil', title: 'Panel/Non-Panel Clinics', icon: 'bubble_chart', class: '', type: 'menu' },
  { path: '/nil', title: 'Doctors', icon: 'bubble_chart', class: '', type: 'menu' },
  { path: '/nil', title: 'Specialist Equipment', icon: 'bubble_chart', class: '', type: 'menu' },
  { path: '/nil', title: 'Tooth Surface/code Master', icon: 'bubble_chart', class: '', type: 'menu' },

  { path: '/nil', title: 'Optician Master', icon: 'bubble_chart', class: '', type: 'menu' },
  { path: '/nil', title: 'Optical Outlets Master', icon: 'bubble_chart', class: '', type: 'menu' },

  { path: '/nil', title: 'Company Policies', icon: 'bubble_chart', class: '', type: 'menu' },
  { path: '/company', title: 'Company Name', icon: 'bubble_chart', class: '', type: 'submenu' },
  { path: '/dataManager/branch', title: 'Master Policies', icon: 'bubble_chart', class: '', type: 'menu' },

  { path: '/dataManager/tpamember', title: 'TPA/Policy Certificate', icon: 'bubble_chart', class: '', type: 'menu' },

  { path: '/employee', title: 'Principals master ', icon: 'bubble_chart', class: '', type: 'menu' },
  { path: '/dataManager/dependent', title: 'Depandants Master', icon: 'bubble_chart', class: '', type: 'menu' },
  { path: '/nil', title: 'Personal Policies', icon: 'bubble_chart', class: '', type: 'menu' },
  { path: '/nil', title: 'Policy Master(Personal)', icon: 'bubble_chart', class: '', type: 'menu' },

  { path: '/nil', title: 'CMS Internal / External', icon: 'bubble_chart', class: '', type: 'menu' },
  { path: '/nil', title: 'Broker / Agent', icon: 'bubble_chart', class: '', type: 'menu' },
  { path: '/nil', title: 'Clinic Admin', icon: 'bubble_chart', class: '', type: 'menu' },
  { path: '/nil', title: 'TCP Admin', icon: 'bubble_chart', class: '', type: 'menu' },

  { path: '/dataManager/master-tpasetup', title: 'Master TPA Setup ', icon: 'bubble_chart', class: '', type: 'menu' },
  { path: '/dataManager/master-tpa', title: 'Master Policy setup ', icon: 'bubble_chart', class: '', type: 'menu' },
  { path: '/nil', title: 'States Master', icon: 'location_on', class: '', type: 'menu' },
  { path: '/nil', title: 'Zone Master', icon: 'location_on', class: '', type: 'menu' },
  { path: '/nil', title: 'Location', icon: 'location_on', class: '', type: 'menu' },
  { path: '/nil', title: 'Product-Type', icon: 'bubble_chart', class: '', type: 'menu' },
];

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit, AfterViewInit {
  menuItems: any[];

  @ViewChild('appDrawer') appDrawer: ElementRef;
  currentUser = JSON.parse(localStorage.getItem('currentuser'))
  navItems: NavItem[];

  constructor(private menuService: MenuService, private nav: NavigationItem) {
    let user_type = this.currentUser.User_Type_ID
    if (user_type == 2) {
      this.navItems = this.nav.getExternalUser()
    }
    if (user_type == 1) {
      this.navItems = this.nav.getSuperAdmin()
    }
  }

  ngOnInit() {
    this.menuItems = ROUTES.filter(menuItem => menuItem);
  }
  isMobileMenu() {
    if ($(window).width() > 991) {
      return false;
    }
    return true;
  };

  ngAfterViewInit() {
    this.menuService.appDrawer = this.appDrawer;
  }

}
