import { ConfigService } from './../services/config/config.service';
import { EventEmitter, Injectable } from '@angular/core';

import { Event, NavigationEnd, Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class MenuService {

  public appDrawer: any;
  public currentUrl = new BehaviorSubject<string>(undefined);
  apiRoute = `${this.configService.api}`;

  constructor(private router: Router, private configService: ConfigService) {
    this.router.events.subscribe((event: Event) => {
      if (event instanceof NavigationEnd) {
        this.currentUrl.next(event.urlAfterRedirects);
      }
    });
  }

  addMenu(data) {
    return this.configService.contactApiWithoutToken('post', `${this.apiRoute}/addTPAMenus`, data)
  }

  getMenu(id) {
    return this.configService.contactApiWithoutToken('post', `${this.apiRoute}/getTPAMenus`, JSON.stringify({ TPA_menu_ID: id }))
  }

  addMenuLevel(data) {
    return this.configService.contactApiWithoutToken('post', `${this.apiRoute}/addTPAMenuLevel`, data)
  }

  getMenuLevel(id) {
    return this.configService.contactApiWithoutToken('post', `${this.apiRoute}/getTPAMenuLevel`, JSON.stringify({ TPA_Menu_Level_ID: id }))
  }

  public closeNav() {
    this.appDrawer.close();
  }

  public openNav() {
    this.appDrawer.open();
  }
}
