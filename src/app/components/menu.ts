import { Injectable } from '@angular/core';
export interface NavItem {
  displayName: string;
  disabled?: boolean;
  iconName?: string;
  route?: string;
  children?: NavItem[];
}

const superAdmin: NavItem[] = [

  //DashBoard
  {
    displayName: 'DashBoard',
    iconName: 'dashboard',
    route: '/dashboard',
  },

  //Data manager
  {
    displayName: 'Data Manager',
    iconName: 'perm_identity',
    children: [
      {
        displayName: 'TPA Setup',
        iconName: 'code',
        children: [
          {
            displayName: 'Branch',
            route: '/dataManager/branch',
          },
          {
            displayName: 'Products',
            route: '/dataManager/product',
          },
          {
            displayName: 'Master TPA ID Setup',
            route: '/dataManager/master-tpasetup',
          },
          {
            displayName: 'Master TPA Company Setup',
            route: '/dataManager/master-tpa',
          },
          {
            displayName: 'TPA',
            route: '/dataManager/tpamember',
          },
        ]
      },
      {
        displayName: 'Dental Services',
        iconName: 'healing',
        children: [
          {
            displayName: 'All Clinics',
            iconName: 'person',
            children: [
              {
                displayName: 'Panel Clinics',
                route: '/dataManager/clinic',
              },
              {
                displayName: 'Non-Panel Clinics',
                route: '/dataManager/clinic',
              },
            ]
          },

          {
            displayName: 'Doctors',
            iconName: 'person',
            children: [
              {
                displayName: 'Doctors Master',
                route: '/dataManager/doctor'
              },
              {
                displayName: 'Doctors-Clinic',
                route: '/dataManager/doctor/clinics'
              }
            ]
          },
          {
            displayName: 'Specialist Equipment',
            route: 'dataManager/specialistEquipment'
          },
          {
            displayName: 'Tooth Surface Master',
            route: '/dataManager/toothSurfaceCode'
          },
          {
            displayName: 'Tooth Code Master',
            route: '/treatmentManager/toothCode',
          },
        ]
      },
      {
        displayName: 'Membership',
        iconName: 'business',
        children: [
          {
            displayName: 'Company',
            route: '/company',
          },
          {
            displayName: 'Principal Master',
            route: '/employee',
          },
          {
            displayName: 'Dependants Master',
            route: '/dataManager/dependent',
          },
          {
            displayName: 'Individual Policies',
          }
        ]
      },
      {
        displayName: 'Users',
        iconName: 'group',
        children: [
          {
            displayName: 'TPA User',
            route: '/tpa-users'
          },
          {
            displayName: 'TPA User Type',
            route: '/tpa-users/userTypes'
          },
          {
            displayName: 'Broker',
          },
          {
            displayName: 'Clinic Admin',
          },
          {
            displayName: 'TPC Admin',
          },
        ]
      },
      {
        displayName: 'Static',
        iconName: 'business',
        children: [
          {
            displayName: 'Service Type Master',
            route: '/dataManager/servicetype'
          },
          {
            displayName: 'Clinic Type Master',
            route: '/dataManager/clinicType'
          },
          {
            displayName: 'States Master',
          },
          {
            displayName: 'City Master',
            route: '/city'
          },
          {
            displayName: 'Nationality Master',
            route: '/nationality'
          },
          {
            displayName: 'Zone Master',
          },
          {
            displayName: 'Location',
          },
        ]
      },
      {
        displayName: 'Insurer',
        iconName: 'group',
        children: [
          {
            displayName: 'Insurance Company',
            route: '/dataManager/insuranceCompany'
          }
        ]
      },
      {
        displayName: 'Treatment Plans',
        iconName: 'next_plan',
        children: [
          {
            displayName: 'Dental Plan Master(UnLimited)',
          },
          {
            displayName: 'Dental Plan Master(Limited)',
            route: '/dataManager/treatmentPlan'
          },
          {
            displayName: 'Company Treatment Plan',
            route: '/dataManager/treatmentPlanCompany'
          },
          // {
          //   displayName: 'Treatment Plan Category',
          //   route: '/dataManager/treatmentPlanCategory'
          // }
        ]
      },
      {
        displayName: 'Optical Services',
        iconName: 'panorama_fish_eye',
        children: [
          {
            displayName: 'Optician Master',
          },
          {
            displayName: 'Optical Outlets Master',
          },
        ]
      },

    ]
  },

  //Treatment Manager
  {
    displayName: 'Treatment Manager',
    iconName: 'supervisor_account',
    children: [
      {
        displayName: 'Treatment Rates Master TPA',
        children:
          [
            {
              displayName: 'Treatment Category',
              route: '/treatmentManager/treatmentCategory',
            },
            {
              displayName: 'Treatment Groups',
              route: '/treatmentManager/treatmentGroup',
            },
            {
              displayName: 'Treatment Code',
              route: '/treatmentManager/treatmentCode',
            },
            {
              displayName: 'Clinic Code',
            },
            {
              displayName: 'TPA Code Price',
              route: '/treatmentManager/treatmentcode/clinics',

            },
            {
              displayName: 'Treatment Master Setup',
              route: '/treatmentManager/treatmentMasterSetup'
            }
          ]
      },
      {
        displayName: 'Treatment Rates Master Insurance',
        iconName: 'person',
        children:
          [
            {
              displayName: 'Treatment Category',
            },
            {
              displayName: 'Treatment Groups',
            },
            {
              displayName: 'Treatment Code',
            },
            {
              displayName: 'Clinic Code',
            },
            {
              displayName: 'Insurance Code Price',
            },
          ]
      },
      {
        displayName: 'Optical Setup',
        iconName: 'person',
        children: [
          {
            displayName: 'Optical Categories',
          },
          {
            displayName: 'Spectacle Frame Types',
          },
          {
            displayName: 'Spectacle Lense Types',
          },
          {
            displayName: 'Spectacle Lense Coatings',
          },
          {
            displayName: 'Spectacle Lense Material',
          },
          {
            displayName: 'Contact Lense Type of Wear',
          },
          {
            displayName: 'Optical Code',
          },
        ]
      },

    ]
  },

  //Benefit Option master
  {
    displayName: 'BenefitOption Manager',
    iconName: 'supervised_user_circle',
    children: [
      {
        displayName: 'TPA Benefit Master',
        route: '/benefitOption/tpabenefits'
      },
      {
        displayName: 'Option Model',
      },
      {
        displayName: 'Discount Model',
      },
      {
        displayName: 'Admin Fee Model',
      },
      {
        displayName: 'Float Model',
      },
      {
        displayName: 'PYC (Pay as You Claim) Model',
      },
      {
        displayName: 'Auto Approval Model',
      },
    ]
  },

  //Data Import 
  {
    displayName: 'Data Import/Export (CSV IMPORT SCREEN)',
    iconName: 'import_export',
    children: [
      {
        displayName: 'TPA Member Import',
        route: 'dataManager/tpamember/import/list',
      },
      {
        displayName: 'User(Principal Employee and dependent)',
      },
      {
        displayName: 'User(Dependent)',
      },
      {
        displayName: 'Company',
      },
      {
        displayName: 'Clinics',
      },
      {
        displayName: 'Doctors',
      },
      {
        displayName: 'Payment Export(To Bank)',
      },
      {
        displayName: 'Payment Import(From Bank)',
      },
    ]
  },

  //Claim manager
  {
    displayName: 'Claim Manager',
    iconName: 'verified',
    children: [
      {
        displayName: 'Main - Claims Screen',
      },
      {
        displayName: 'Claims(ALL)',
      },
      {
        displayName: 'Claims In Progress',
      },
      {
        displayName: 'Claims(New)',
      },
      {
        displayName: 'Claims(Verifying)',
      },
      {
        displayName: 'Claims(Processing)',
      },
      {
        displayName: 'Claims(Completed)',
      },
      {
        displayName: 'Claims(Cancelled)',
      },
      {
        displayName: 'Claims(KIV)',
      },
      {
        displayName: 'Claims Submission(Clinic)',
        route: '/claims/submission/clinicindex'
      },
      {
        displayName: 'Claims Submission(Internal)',
        route: '/claims/submission/index'
      },
      {
        displayName: 'Claims Submission(Reimbursement)',
      },
      {
        displayName: 'Run Batch Claim',
      },
    ]
  },

  //Report manager
  {
    displayName: 'Report Manager',
    iconName: 'trending_up',
    children: [
      {
        displayName: 'DashBoard',
      },
      {
        displayName: 'Weekly',
      },
      {
        displayName: 'Monthly',
      },
      {
        displayName: 'By Clinic',
      },
    ]
  },

  //Payment Manager
  {
    displayName: 'Payment Manager',
    iconName: 'payment',
    children:
      [
        {
          displayName: 'ALL Payment'
        },
        {
          displayName: 'Generate Payment'
        },
        {
          displayName: 'By Company'
        },
        {
          displayName: 'By Clinic'
        },
        {
          displayName: 'By Broker'
        },
        {
          displayName: 'Statement of Account'
        }
      ]
  },

  //Invoice Manager
  {
    displayName: 'Invoice Manager',
    iconName: 'receipt',
    children:
      [
        {
          displayName: 'All Invoices'
        },
        // {
        //   displayName: 'New Invoice',
        //   route: '/invoice/newInvoice'
        // },
        {
          displayName: 'Company Invoice',
          route: '/invoice/companyInvoice'
        },
        {
          displayName: 'Clinic Invoice',
          route: '/invoice/clinicInvoice'
        },
        {
          displayName: 'By Broker'
        },
        {
          displayName: 'Statement of Account'
        }
      ]
  },



  //Permission Manager
  {
    displayName: 'Permission Manager',
    iconName: 'accessibility',
    children: [
      {
        displayName: 'User Permission',
      },
      {
        displayName: 'User Audit',
      },
      {
        displayName: 'TPA Menu',
        route: '/permission'
      },
    ]
  },

  //Support
  {
    displayName: 'Support',
    iconName: 'help',
    children: [
      {
        displayName: 'Services Request',
        route: '/support'
      },
      {
        displayName: 'Open Queries',
      },
      {
        displayName: 'Pending Queries',
      },
      {
        displayName: 'Closed Queries',
      },
    ]
  }
];

const externalUser = [//Claim manager
  {
    displayName: 'Claim Manager',
    iconName: 'verified',
    children: [
      {
        displayName: 'Main - Claims Screen',
      },
      {
        displayName: 'Claims(ALL)',
      },
      {
        displayName: 'Claims In Progress',
      },
      {
        displayName: 'Claims(New)',
      },
      {
        displayName: 'Claims(Verifying)',
      },
      {
        displayName: 'Claims(Processing)',
      },
      {
        displayName: 'Claims(Completed)',
      },
      {
        displayName: 'Claims(Cancelled)',
      },
      {
        displayName: 'Claims(KIV)',
      },
      {
        displayName: 'Patient Eligibility',
        route: '/claims/submission/patientEligibility'
      },
      {
        displayName: 'Claims Submission(Clinic)',
        route: '/claims/submission/clinicindex'
      },
      {
        displayName: 'Claims Submission(Reimbursement)',
      },
      {
        displayName: 'Run Batch Claim',
      },
    ]
  },
]




@Injectable()
export class NavigationItem {
  getSuperAdmin() {
    return superAdmin
  }

  getExternalUser() {
    return externalUser
  }
}