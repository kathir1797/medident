
import { MatSnackBar } from '@angular/material/snack-bar';
import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { treatmentMasterSetup } from 'app/models/treatmentManager';
import { TreatmentMasterService } from 'app/services/treatmentManager/treatment-master.service';

@Component({
  selector: 'app-treatment-master',
  templateUrl: './treatment-master.component.html',
  styleUrls: ['./treatment-master.component.css']
})
export class TreatmentMasterComponent implements OnInit, AfterViewInit {

  @ViewChild(MatPaginator) paginator: MatPaginator;
  dataSource = new MatTableDataSource<any>();
  length: number
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  displayedColumns = ['Treatment_Code', 'Treatment_Group_Code', 'Treatment_Group_Description', 'Remarks', 'Action'];

  treatmentMasterSetup: treatmentMasterSetup = new treatmentMasterSetup();

  constructor(private snackBar: MatSnackBar, private tmaster: TreatmentMasterService) { }

  ngOnInit(): void {
    this.dataSource.paginator = this.paginator
    this.dataSource.sort = this.sort
    this.treatmentMasterSetup.Treatment_Master_ID = 0;
    this.getTreatmentMasterSetup()
  }

  getTreatmentMasterSetup() {
    this.tmaster.getTreatmentMaster(this.treatmentMasterSetup).subscribe(response => {
      if (response.success) {
        this.dataSource.data = response.data;
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
      } else {
        this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 })
      }
    });
  }

  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

}
