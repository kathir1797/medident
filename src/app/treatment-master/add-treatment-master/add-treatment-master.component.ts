import { SubmissionService } from './../../services/claims/submission.service';
import { tap } from 'rxjs/operators';
import { treatmentMasterSetup } from './../../models/treatmentManager';
import { toothCodeRequest, treatmentCodeRequest, treatmentGroup } from 'app/models/treatmentManager';
import { Router, ActivatedRoute } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { TreatmentCodeService } from 'app/services/treatmentManager/treatment-code.service';
import { TreatmentGroupService } from 'app/services/treatmentManager/treatment-group.service';
import { ToothCodeService } from 'app/services/treatmentManager/tooth-code.service';
import { MatCheckboxChange } from '@angular/material/checkbox';
import { DdlistService } from 'app/services/config/ddlist.service';
import { TreatmentMasterService } from 'app/services/treatmentManager/treatment-master.service';
import { MatSelectChange } from '@angular/material/select';

@Component({
  selector: 'app-add-treatment-master',
  templateUrl: './add-treatment-master.component.html',
  styleUrls: ['./add-treatment-master.component.css']
})
export class AddTreatmentMasterComponent implements OnInit {

  toptitle: string = 'Add TreatmentMaster';
  btntext: string = 'Save';
  isDisabled: boolean = true;
  treatmentMasterForm: FormGroup;
  treatmentMasterToothCodeForm: FormGroup;
  obj = localStorage.getItem('currentuser');
  urlText: any;
  statusList = new Array();
  treatmentToothCodeList = new Array();
  doctorClinicListData = new Array();
  doctorsList = new Array();
  treatmentCodeList = new Array();
  treatmentgrouplist = new Array();

  treatmentCodeReq: treatmentCodeRequest = new treatmentCodeRequest();
  treatmentgroupReq: treatmentGroup = new treatmentGroup();
  toothCodeReq: toothCodeRequest = new toothCodeRequest();
  toothCodeList = new Array();
  xRay: Array<any> = [
    {
      Text: 'No X-Ray',
      Value: 'No X-Ray'
    },
    {
      Text: 'Pre X-Ray Only',
      Value: 'Pre X-Ray only'
    },
    {
      Text: 'Post X-Ray only',
      Value: 'Post X-Ray only'
    },
    {
      Text: 'Both Pre and Post X-Ray',
      Value: 'Both Pre and Post X-Ray'
    },
    {
      Text: 'Either Pre or Post X-Ray',
      Value: 'Either Pre or Post X-Ray'
    },
    {
      Text: 'Diagnostic X-Ray Only',
      Value: 'Diagnostic X-Ray Only'
    },
    {
      Text: 'Diagnostic or Post X-Ray Only',
      Value: 'Diagnostic or Post X-Ray '
    },
  ]

  toothCodeSelected = new Array();
  toothcodeId: boolean = false;

  treatmentMasterSetup: treatmentMasterSetup = new treatmentMasterSetup();



  constructor(private fb: FormBuilder, private snackBar: MatSnackBar, private ddlist: DdlistService, private router: Router,
    private treatmentCode: TreatmentCodeService, private treatmentGrp: TreatmentGroupService, private submission: SubmissionService,
    private toothCode: ToothCodeService, private tmaster: TreatmentMasterService, private activatedRoute: ActivatedRoute) {
    const url = this.router.url;
    this.urlText = url.split('/');
    this.initializeForm();
    this.inintializetoothcodeForm();
    this.activatedRoute.params.subscribe(response => {
      if (this.urlText[3] != 'add') {
        this.toptitle = 'Update Treatment Master';
        this.btntext = 'Update';
        this.treatmentMasterSetup.Treatment_Master_ID = response.Treatment_Master_ID;
        this.tmaster.getTreatmentMaster(this.treatmentMasterSetup).subscribe(response => {
          if (response.success) {
            this.treatmentMasterForm.patchValue({
              Treatment_Master_ID: response.data[0].Treatment_Master_ID,
              Treatment_Code_ID: response.data[0].Treatment_Code_ID,
              Treatment_Group_ID: response.data[0].Treatment_Group_ID,
              Category_Number: response.data[0].Category_Number,
              Min_Age: response.data[0].Min_Age,
              Max_Age: response.data[0].Max_Age,
              X_Ray_Required: response.data[0].X_Ray_Required,
              Remarks: response.data[0].Remarks,
              Specialist_YN: response.data[0].Specialist_YN,
              Treatment_Master_Status_ID: response.data[0].Treatment_Master_Status_ID,
              User_ID: JSON.parse(this.obj).User_ID
            });
            this.tmaster.getTreatmentMasterToothCode(this.treatmentMasterSetup).subscribe(toothMaster => {
              if (toothMaster.success) {
                this.treatmentToothCodeList = toothMaster.data
                this.getToothCode().subscribe(tooth => {
                  if (tooth.success) {
                    for (var i = 0; i < this.toothCodeList.length; i++) {
                      for (var j = 0; j < this.treatmentToothCodeList.length; j++) {
                        if (this.toothCodeList[i].Tooth_Code_ID == this.treatmentToothCodeList[j].Tooth_Code_ID) {
                          this.toothCodeList[i].toothcodeId = true
                          this.toothCodeSelected.push(this.toothCodeList[i].Tooth_Code_ID);
                        }
                      }
                    }
                  } else {
                    this.snackBar.open(toothMaster.errorMessage, 'ok', { duration: 3000 })
                  }
                })
              } else {
                this.snackBar.open(toothMaster.errorMessage, 'ok', { duration: 3000 })
              }
            });

          } else {
            this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 })
          }
        });
      }
      if (this.urlText[3] === 'view') {
        this.isDisabled = false
        this.toptitle = 'View Treatment Master'
      }
    })
  }

  ngOnInit(): void {
    this.getStatusDDLists();
    this.treatmentCodeReq.Treatment_Code_ID = 0;
    this.treatmentgroupReq.Treatment_Group_ID = 0;
    this.toothCodeReq.Tooth_Code_ID = 0;
    // this.getToothCode().subscribe();
    this.getTreatmentGroup();
    this.getTreatmentCode(1);
    if (this.urlText[3] == 'add') {
      this.getToothCode().subscribe();
      this.getTreatmentCode(0);

    }
  }

  getStatusDDLists() {
    this.ddlist.getStatusDDList().subscribe(response => {
      if (response.success) {
        this.statusList = response.data;
      } else {
        this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 })
      }
    });
  }

  initializeForm() {
    this.treatmentMasterForm = this.fb.group({
      Treatment_Master_ID: 0,
      Treatment_Code_ID: new FormControl({ value: '', disabled: this.urlText[3] == 'view' || this.urlText[3] != 'add' ? true : false }),
      Treatment_Group_ID: new FormControl({ value: '', disabled: true }),
      Category_Number: new FormControl({ value: '', disabled: true }),
      Min_Age: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      Max_Age: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      X_Ray_Required: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      Remarks: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      Specialist_YN: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      Treatment_Master_Status_ID: new FormControl({ value: 1, disabled: this.urlText[3] == 'view' ? true : false }),
      User_ID: JSON.parse(this.obj).User_ID
    })
  }

  inintializetoothcodeForm() {
    this.treatmentMasterToothCodeForm = this.fb.group({
      Treatment_Tooth_Master_ID: 0,
      Treatment_Master_ID: new FormControl(),
      Tooth_Code_ID: new FormControl([]),
      Treatment_Tooth_Master_Status_ID: new FormControl(),
      User_ID: JSON.parse(this.obj).User_ID
    })
  }

  getTreatmentCode(index: number) {
    if (index == 0) {
      this.tmaster.getTreatmentMasterUnassignedCode().subscribe(response => {
        if (response.success) {
          this.treatmentCodeList = response.data;
        } else {
          this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 })
        }
      })
    } else {
      this.treatmentCode.getTreatmentCode(this.treatmentCodeReq).subscribe(response => {
        if (response.success) {
          this.treatmentCodeList = response.data;
        } else {
          this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 })
        }
      })
    }


  }

  getTreatmentGroup() {
    this.treatmentGrp.getTreatmentGroup(this.treatmentgroupReq).subscribe(response => {
      if (response.success) {
        this.treatmentgrouplist = response.data;
      } else {
        this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 })
      }
    });
  }

  getToothCode() {
    return this.toothCode.getToothCode(this.toothCodeReq).pipe(
      tap(response => {
        if (response.success) {
          this.toothCodeList = response.data;
          this.toothCodeList = this.toothCodeList.filter(value => {
            if (value.Tooth_Code_Status_ID == 1) {
              return value
            }
          })
        } else {
          this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 })
        }
      })
    )
  }

  onSpecialistChange(e: MatCheckboxChange) {
    if (e.checked) {
      this.treatmentMasterForm.get('Specialist_YN').patchValue('Y');
    } else {
      this.treatmentMasterForm.get('Specialist_YN').patchValue('N');
    }
  }

  onXRayChange(e: MatCheckboxChange) {
    if (e.checked) {
      this.treatmentMasterForm.get('X_Ray_Required').patchValue('Y');
    } else {
      this.treatmentMasterForm.get('X_Ray_Required').patchValue('N');
    }
  }

  onTreatmentCodeChange(event: MatSelectChange) {
    this.treatmentCodeList.filter(val => {
      if (val.Treatment_Code_ID === event.value) {
        this.treatmentMasterForm.get('Category_Number').patchValue(val.Category_Number)
        this.treatmentMasterForm.get('Treatment_Group_ID').patchValue(val.Treatment_Group_ID)
      }
    })
  }

  // fillCategory(treatment_group_id) {
  //   this.treatmentgrouplist.find(x => {
  //     if (x.Treatment_Group_ID == treatment_group_id) {
  //       this.treatmentMasterForm.get('Category_Number').patchValue(x.Category_Number)
  //     }
  //   })
  // }

  onToothSetupChange(e, Tooth_Code_ID) {
    if (e.checked) {
      this.toothCodeSelected.push(Tooth_Code_ID);
    } else {
      this.toothCodeSelected = this.toothCodeSelected.filter((value) => value != Tooth_Code_ID)
    }
  }

  addTreatmentMaster() {
    if (this.treatmentMasterForm.get('X_Ray_Required').value != '' && this.treatmentMasterForm.get('Treatment_Group_ID').value != '' && this.treatmentMasterForm.get('Category_Number').value != '') {
      this.tmaster.addTreatmentMaster(this.treatmentMasterForm.getRawValue()).subscribe(response => {
        if (response.success) {
          var master_ID = response.data.Treatment_Master_ID;
          //Deleting Tooth Master
          this.tmaster.deleteTreatmentToothMaster(master_ID).subscribe(deleteCallBack => {
            if (deleteCallBack.success) {
              //Executing Add ToothMaster
              for (var i = 0; i < this.toothCodeSelected.length; i++) {
                let toothcode = this.toothCodeSelected[i];
                this.treatmentMasterToothCodeForm.patchValue({
                  Treatment_Master_ID: master_ID,
                  Tooth_Code_ID: toothcode,
                  Treatment_Tooth_Master_Status_ID: this.treatmentMasterForm.get('Treatment_Master_Status_ID').value,
                  User_ID: JSON.parse(this.obj).User_ID
                });
                this.tmaster.addTreatmentMasterToothCode(JSON.stringify(this.treatmentMasterToothCodeForm.value)).subscribe(tooth => {
                  if (tooth.success) {
                    this.snackBar.open('Treatment Master Saved Successfully', 'ok', { duration: 3000 });
                    if (i != this.toothCodeList.length) {
                      this.treatmentMasterToothCodeForm.reset();
                      this.treatmentMasterForm.reset();
                    }
                  } else {
                    this.snackBar.open(tooth.errorMessage, 'ok', { duration: 3000 })
                  }
                });
              }
            } else {
              this.snackBar.open(deleteCallBack.errorMessage, 'ok', { duration: 3000 })
            }
          })

          this.router.navigate(['/treatmentManager/treatmentMasterSetup']);
        } else {
          this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 })
        }
      })
    } else {
      this.snackBar.open('Make Sure All Details are Filled', 'OK', { duration: 5000 })
    }

  }

  deleteTreatmentToothMaster(treatmentMasterID) {
    this.tmaster.deleteTreatmentToothMaster(treatmentMasterID).subscribe(response => {
      if (response.success) {
        //Executing Add ToothMaster
      } else {
        this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 })
      }
    })
  }

  // addTreatmentMaster() {
  //   for (var i = 0; i < this.toothCodeSelected.length; i++) {
  //     const toothcode = this.toothCodeSelected[i]
  //     if (this.treatmentMasterForm.get('X_Ray_Required').value != '') {
  //       this.tmaster.addTreatmentMaster(JSON.stringify(this.treatmentMasterForm.value)).subscribe(response => {
  //         if (response.success) {
  //           this.treatmentMasterToothCodeForm.patchValue({
  //             Treatment_Master_ID: response.data.Treatment_Master_ID,
  //             Tooth_Code_ID: toothcode,
  //             Treatment_Tooth_Master_Status_ID: this.treatmentMasterForm.get('Treatment_Master_Status_ID').value,
  //             User_ID: JSON.parse(this.obj).User_ID
  //           });

  //           this.tmaster.addTreatmentMasterToothCode(JSON.stringify(this.treatmentMasterToothCodeForm.value)).subscribe(tooth => {
  //             if (tooth.success) {
  //               this.snackBar.open('Treatment Master Saved Successfully', 'ok', { duration: 3000 });
  //             } else {
  //               this.snackBar.open(tooth.errorMessage, 'ok', { duration: 3000 })
  //             }
  //           });
  //         } else {
  //           this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 })
  //         }
  //       });
  //     } else {
  //       this.snackBar.open('Tooth Code and X-ray is required', 'ok', { duration: 3000 })
  //     }
  //   }
  //   this.router.navigate(['/treatmentManager/treatmentMasterSetup']);
  // }

  getTreatmentMasterSetup(masterId) {
    this.tmaster.getTreatmentMaster(masterId).subscribe(response => {
      if (response.success) {
        this.treatmentMasterForm.patchValue({
          Treatment_Master_ID: response.data[0].Treatment_Master_ID,
          Treatment_Code_ID: response.data[0].Treatment_Code_ID,
          Treatment_Group_ID: response.data[0].Treatment_Group_ID,
          Category_Number: response.data[0].Category_Number,
          Min_Age: response.data[0].Min_Age,
          Max_Age: response.data[0].Max_Age,
          X_Ray_Required: response.data[0].X_Ray_Required,
          Remarks: response.data[0].Remarks,
          Specialist_YN: response.data[0].Specialist_YN,
          Treatment_Master_Status_ID: response.data[0].Treatment_Master_Status_ID,
          User_ID: JSON.parse(this.obj).User_ID
        });
        this.tmaster.getTreatmentMasterToothCode(masterId).subscribe(toothMaster => {
          if (toothMaster.success) {
            this.treatmentToothCodeList = toothMaster.data
            this.treatmentMasterToothCodeForm.patchValue({
              Treatment_Tooth_Master_ID: toothMaster.data[0].Treatment_Tooth_Master_ID,
              Treatment_Master_ID: response.data[0].Treatment_Master_ID,
              Tooth_Code_ID: toothMaster.data[0].Tooth_Code_ID,
              Treatment_Tooth_Master_Status_ID: response.data[0].Treatment_Master_Status_ID,
              User_ID: JSON.parse(this.obj).User_ID
            })
            this.getToothCode().subscribe(toothCode => {
              if (toothCode.success) {
                this.toothCodeList = toothCode.data;
                for (var i = 0; i < this.toothCodeList.length; i++) {
                  if (this.toothCodeList[i].Tooth_Code_ID == this.treatmentToothCodeList[i].Tooth_Code_ID) {
                    this.toothCodeList[i].toothcodeId = true;
                    this.toothCodeSelected.push(this.toothCodeList[i].Tooth_Code_ID)
                  } else {
                    this.toothCodeList[i].toothcodeId = false;
                  }
                }
              }
            });
          } else {
            this.snackBar.open(toothMaster.errorMessage, 'ok', { duration: 3000 })
          }
        });

      } else {
        this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 })
      }
    });
  }

}
