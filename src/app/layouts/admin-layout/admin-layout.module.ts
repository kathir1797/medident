import { ServiceLevelComponent } from './../../dataManager/service-level/service-level.component';
import { MasterTpaBenefitsComponent } from './../../dataManager/master-tpasetup-profile/master-tpa-benefits/master-tpa-benefits.component';
import { TreatmetPlanCategoryComponent } from './../../dataManager/treatmentplan/treatmet-plan-category/treatmet-plan-category.component';
import { AddMenuComponent } from './../../dashboard/add-menu/add-menu.component';
import { AddClinicTypeComponent } from './../../dataManager/clinic-type/add-clinic-type/add-clinic-type.component';
import { ClinicTypeComponent } from './../../dataManager/clinic-type/clinic-type.component';
import { AddServiceTypeComponent } from './../../dataManager/service-type/add-service-type/add-service-type.component';
import { ServiceTypeComponent } from './../../dataManager/service-type/service-type.component';
import { AlertDialogComponent } from './../../dialog/alert-dialog/alert-dialog.component';
import { AddInsuranceCompanyComponent } from './../../dataManager/insurance-company/add-insurance-company/add-insurance-company.component';
import { CompanyTreatmentPlanComponent } from './../../dataManager/treatmentplan/company-treatment-plan/company-treatment-plan.component';
import { AddCompanyTreatmentPlanComponent } from './../../dataManager/treatmentplan/company-treatment-plan/add-company-treatment-plan/add-company-treatment-plan.component';
import { AddtreatmentplanComponent } from './../../dataManager/treatmentplan/addtreatmentplan/addtreatmentplan.component';
import { TreatmentMasterComponent } from './../../treatment-master/treatment-master.component';
import { AddTreatmentMasterComponent } from './../../treatment-master/add-treatment-master/add-treatment-master.component';
import { AddSpecialistEquipmentComponent } from './../../dataManager/specialist-equipment/add-specialist-equipment/add-specialist-equipment.component';
import { SpecialistEquipmentComponent } from './../../dataManager/specialist-equipment/specialist-equipment.component';
import { ToothSurfaceMasterComponent } from './../../tooth-surface-master/tooth-surface-master.component';
import { TreatmentCategoryComponent } from './../../treatmentManager/treatment-category/treatment-category.component';
import { AddTreatmentGroupComponent } from './../../treatmentManager/treatment-group/add-treatment-group/add-treatment-group.component';
import { TreatmentGroupComponent } from './../../treatmentManager/treatment-group/treatment-group.component';
import { AddTreatmentCodeClinicsComponent } from './../../treatmentManager/treatment-code-clinics/add-treatment-code-clinics/add-treatment-code-clinics.component';
import { TpaMemberImportListComponent } from './../../dataManager/tpa-member-import/tpa-member-import-list/tpa-member-import-list.component';
import { AddToothCodeComponent } from './../../treatmentManager/tooth-code/add-tooth-code/add-tooth-code.component';
import { ToothCodeComponent } from './../../treatmentManager/tooth-code/tooth-code.component';
import { PropertiesPipe } from './../../constant/properties.pipe';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule, DatePipe } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AdminLayoutRoutes } from './admin-layout.routing';
import { DashboardComponent } from '../../dashboard/dashboard.component';
import { TypographyComponent } from '../../typography/typography.component';
import { IconsComponent } from '../../icons/icons.component';
import { MapsComponent } from '../../maps/maps.component';
import { NotificationsComponent } from '../../notifications/notifications.component';
import { UpgradeComponent } from '../../upgrade/upgrade.component';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatRippleModule } from '@angular/material/core';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatSelectModule } from '@angular/material/select';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatSidenavModule } from "@angular/material/sidenav";
import { MatIconModule } from '@angular/material/icon';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { MatRadioModule } from '@angular/material/radio';
import { MatMenuModule } from '@angular/material/menu';

import { ProviderComponent } from 'app/provider/provider.component';
import { ProviderProfileComponent } from 'app/provider-profile/provider-profile.component';
import { empty } from 'rxjs';
import { EmployeeComponent } from 'app/employee/employee.component';
import { EmployeeProfileComponent } from 'app/employee-profile/employee-profile.component';
import { CompanyComponent } from 'app/company/company.component';
import { CompanyProfileComponent } from 'app/company-profile/company-profile.component';
import { MatCardModule } from '@angular/material/card';
import { MasterTPASetupComponent } from 'app/dataManager/master-tpasetup/master-tpasetup.component';
import { MasterTPAsetupProfileComponent } from 'app/dataManager/master-tpasetup-profile/master-tpasetup-profile.component';
import { MasterTPAComponent } from 'app/dataManager/master-tpa/master-tpa.component';
import { MasterTPAProfileComponent } from 'app/dataManager/master-tpa-profile/master-tpa-profile.component';
import { DependentComponent } from 'app/dataManager/dependent/dependent.component';
import { DependentProfileComponent } from 'app/dataManager/dependent-profile/dependent-profile.component';
import { TPAmemberComponent } from 'app/dataManager/tpamember/tpamember.component';
import { TPAmemberProfileComponent } from 'app/dataManager/tpamember-profile/tpamember-profile.component';
import { ClinicComponent } from 'app/dataManager/clinic/clinic.component';
import { ClinicProfileComponent } from 'app/dataManager/clinic-profile/clinic-profile.component';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { BranchComponent } from 'app/dataManager/branch/branch.component';
import { BranchProfileComponent } from 'app/dataManager/branch-profile/branch-profile.component';
import { ProductComponent } from 'app/dataManager/product/product.component';
import { AddProductComponent } from 'app/dataManager/product/add-product/add-product.component';
import { DoctorComponent } from 'app/dataManager/doctor/doctor.component';
import { AddDoctorComponent } from 'app/dataManager/doctor/add-doctor/add-doctor.component';
import { AddDoctorClinicsComponent } from 'app/dataManager/doctor-clinics/add-doctor-clinics/add-doctor-clinics.component';
import { DoctorClinicsComponent } from 'app/dataManager/doctor-clinics/doctor-clinics.component';
import { TpaMemberImportComponent } from 'app/dataManager/tpa-member-import/tpa-member-import.component';
import { AddTreatmentCodeComponent } from 'app/treatmentManager/treatment-code/add-treatment-code/add-treatment-code.component';
import { TreatmentCodeComponent } from 'app/treatmentManager/treatment-code/treatment-code.component';
import { BreadcrumbModule } from 'angular-crumbs';
import { EditTpaMemberImportComponent } from 'app/dataManager/tpa-member-import/edit-tpa-member-import/edit-tpa-member-import.component';
import { TreatmentCodeClinicsComponent } from 'app/treatmentManager/treatment-code-clinics/treatment-code-clinics.component';
import { AddTreatmentCategoryComponent } from 'app/treatmentManager/treatment-category/add-treatment-category/add-treatment-category.component';
import { AddToothSurfaceMasterComponent } from 'app/tooth-surface-master/add-tooth-surface-master/add-tooth-surface-master.component';
import { TreatmentplanComponent } from 'app/dataManager/treatmentplan/treatmentplan.component';
import { InsuranceCompanyComponent } from 'app/dataManager/insurance-company/insurance-company.component';
import { MatDialogModule } from '@angular/material/dialog';
import { AddTreatmetPlanCategoryComponent } from 'app/dataManager/treatmentplan/add-treatmet-plan-category/add-treatmet-plan-category.component';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatTabsModule } from '@angular/material/tabs';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AdminLayoutRoutes),
    FormsModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatRippleModule,
    MatNativeDateModule,
    MatInputModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatTooltipModule,
    MatSnackBarModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatIconModule,
    MatDatepickerModule,
    MatCardModule,
    MatCheckboxModule,
    MatSidenavModule,
    BreadcrumbModule,
    MatRadioModule,
    MatMenuModule,
    MatDialogModule,
    MatExpansionModule,
    MatTabsModule
  ],
  declarations: [
    DashboardComponent,
    CompanyProfileComponent,
    CompanyComponent,
    TypographyComponent,
    IconsComponent,
    MapsComponent,
    NotificationsComponent,
    UpgradeComponent,
    ProviderComponent,
    ProviderProfileComponent,
    EmployeeComponent,
    EmployeeProfileComponent,
    MasterTPASetupComponent,
    MasterTPAsetupProfileComponent,
    MasterTPAComponent,
    MasterTPAProfileComponent,
    DependentComponent,
    DependentProfileComponent,
    TPAmemberComponent,
    TPAmemberProfileComponent,
    ClinicComponent,
    ClinicProfileComponent,
    BranchComponent,
    BranchProfileComponent,
    ProductComponent,
    AddProductComponent,
    DoctorComponent,
    AddDoctorComponent,
    DoctorClinicsComponent,
    AddDoctorClinicsComponent,
    TpaMemberImportComponent,
    TpaMemberImportListComponent,
    EditTpaMemberImportComponent,
    ToothCodeComponent,
    AddToothCodeComponent,
    TreatmentCodeComponent,
    AddTreatmentCodeComponent,
    TreatmentCodeClinicsComponent,
    AddTreatmentCodeClinicsComponent,
    TreatmentGroupComponent,
    AddTreatmentGroupComponent,
    TreatmentCategoryComponent,
    AddTreatmentCategoryComponent,
    ToothSurfaceMasterComponent,
    AddToothSurfaceMasterComponent,
    SpecialistEquipmentComponent,
    AddSpecialistEquipmentComponent,
    TreatmentMasterComponent,
    AddTreatmentMasterComponent,
    TreatmentplanComponent,
    AddtreatmentplanComponent,
    CompanyTreatmentPlanComponent,
    AddCompanyTreatmentPlanComponent,
    AddInsuranceCompanyComponent,
    InsuranceCompanyComponent,
    ServiceTypeComponent,
    AddServiceTypeComponent,
    ClinicTypeComponent,
    AddClinicTypeComponent,
    AddMenuComponent,
    AddTreatmetPlanCategoryComponent,
    TreatmetPlanCategoryComponent,
    MasterTpaBenefitsComponent,
    ServiceLevelComponent
  ],
  providers: [DatePipe],
  entryComponents: [AlertDialogComponent, AddMenuComponent]
})

export class AdminLayoutModule { }
