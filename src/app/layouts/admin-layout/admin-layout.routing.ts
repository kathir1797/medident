import { ServiceLevelComponent } from './../../dataManager/service-level/service-level.component';
import { MasterTpaBenefitsComponent } from './../../dataManager/master-tpasetup-profile/master-tpa-benefits/master-tpa-benefits.component';
import { TreatmetPlanCategoryComponent } from './../../dataManager/treatmentplan/treatmet-plan-category/treatmet-plan-category.component';
import { AddClinicTypeComponent } from './../../dataManager/clinic-type/add-clinic-type/add-clinic-type.component';
import { ClinicTypeComponent } from './../../dataManager/clinic-type/clinic-type.component';
import { AddServiceTypeComponent } from './../../dataManager/service-type/add-service-type/add-service-type.component';
import { ServiceTypeComponent } from './../../dataManager/service-type/service-type.component';

import { AddInsuranceCompanyComponent } from '../../dataManager/insurance-company/add-insurance-company/add-insurance-company.component'
import { AddCompanyTreatmentPlanComponent } from './../../dataManager/treatmentplan/company-treatment-plan/add-company-treatment-plan/add-company-treatment-plan.component';
import { CompanyTreatmentPlanComponent } from './../../dataManager/treatmentplan/company-treatment-plan/company-treatment-plan.component';
import { AddtreatmentplanComponent } from './../../dataManager/treatmentplan/addtreatmentplan/addtreatmentplan.component';
import { TreatmentplanComponent } from './../../dataManager/treatmentplan/treatmentplan.component';
import { AddTreatmentMasterComponent } from './../../treatment-master/add-treatment-master/add-treatment-master.component';
import { TreatmentMasterComponent } from './../../treatment-master/treatment-master.component';
import { AddSpecialistEquipmentComponent } from './../../dataManager/specialist-equipment/add-specialist-equipment/add-specialist-equipment.component';
import { SpecialistEquipmentComponent } from './../../dataManager/specialist-equipment/specialist-equipment.component';
import { ToothSurfaceMasterComponent } from './../../tooth-surface-master/tooth-surface-master.component';
import { TreatmentCategoryComponent } from './../../treatmentManager/treatment-category/treatment-category.component';
import { AddTreatmentGroupComponent } from './../../treatmentManager/treatment-group/add-treatment-group/add-treatment-group.component';
import { TreatmentGroupComponent } from './../../treatmentManager/treatment-group/treatment-group.component';
import { AddTreatmentCodeClinicsComponent } from './../../treatmentManager/treatment-code-clinics/add-treatment-code-clinics/add-treatment-code-clinics.component';
import { TpaMemberImportComponent } from 'app/dataManager/tpa-member-import/tpa-member-import.component';
import { TpaMemberImportListComponent } from './../../dataManager/tpa-member-import/tpa-member-import-list/tpa-member-import-list.component';
import { AddTreatmentCodeComponent } from './../../treatmentManager/treatment-code/add-treatment-code/add-treatment-code.component';
import { TreatmentCodeComponent } from './../../treatmentManager/treatment-code/treatment-code.component';
import { AddToothCodeComponent } from './../../treatmentManager/tooth-code/add-tooth-code/add-tooth-code.component';
import { ToothCodeComponent } from './../../treatmentManager/tooth-code/tooth-code.component';
import { AddDoctorClinicsComponent } from './../../dataManager/doctor-clinics/add-doctor-clinics/add-doctor-clinics.component';
import { Routes } from '@angular/router';

import { DashboardComponent } from '../../dashboard/dashboard.component';
import { TypographyComponent } from '../../typography/typography.component';
import { IconsComponent } from '../../icons/icons.component';
import { MapsComponent } from '../../maps/maps.component';
import { NotificationsComponent } from '../../notifications/notifications.component';
import { UpgradeComponent } from '../../upgrade/upgrade.component';
import { ProviderComponent } from 'app/provider/provider.component';
import { ProviderProfileComponent } from 'app/provider-profile/provider-profile.component';
import { EmployeeComponent } from 'app/employee/employee.component';
import { EmployeeProfileComponent } from 'app/employee-profile/employee-profile.component';
import { CompanyComponent } from 'app/company/company.component';
import { CompanyProfileComponent } from 'app/company-profile/company-profile.component';
import { MasterTPASetupComponent } from 'app/dataManager/master-tpasetup/master-tpasetup.component';
import { MasterTPAsetupProfileComponent } from 'app/dataManager/master-tpasetup-profile/master-tpasetup-profile.component';
import { MasterTPAComponent } from 'app/dataManager/master-tpa/master-tpa.component';
import { MasterTPAProfileComponent } from 'app/dataManager/master-tpa-profile/master-tpa-profile.component';
import { DependentComponent } from 'app/dataManager/dependent/dependent.component';
import { DependentProfileComponent } from 'app/dataManager/dependent-profile/dependent-profile.component';
import { TPAmemberComponent } from 'app/dataManager/tpamember/tpamember.component';
import { TPAmemberProfileComponent } from 'app/dataManager/tpamember-profile/tpamember-profile.component';
import { ClinicComponent } from 'app/dataManager/clinic/clinic.component';
import { ClinicProfileComponent } from 'app/dataManager/clinic-profile/clinic-profile.component';
import { BranchComponent } from 'app/dataManager/branch/branch.component';
import { BranchProfileComponent } from 'app/dataManager/branch-profile/branch-profile.component';
import { ProductComponent } from 'app/dataManager/product/product.component';
import { AddProductComponent } from 'app/dataManager/product/add-product/add-product.component';
import { DoctorComponent } from 'app/dataManager/doctor/doctor.component';
import { AddDoctorComponent } from 'app/dataManager/doctor/add-doctor/add-doctor.component';
import { DoctorClinicsComponent } from 'app/dataManager/doctor-clinics/doctor-clinics.component';
import { EditTpaMemberImportComponent } from 'app/dataManager/tpa-member-import/edit-tpa-member-import/edit-tpa-member-import.component';
import { TreatmentCodeClinicsComponent } from 'app/treatmentManager/treatment-code-clinics/treatment-code-clinics.component';
import { AddTreatmentCategoryComponent } from 'app/treatmentManager/treatment-category/add-treatment-category/add-treatment-category.component';
import { AddToothSurfaceMasterComponent } from 'app/tooth-surface-master/add-tooth-surface-master/add-tooth-surface-master.component';
import { InsuranceCompanyComponent } from 'app/dataManager/insurance-company/insurance-company.component';
import { AddTreatmetPlanCategoryComponent } from 'app/dataManager/treatmentplan/add-treatmet-plan-category/add-treatmet-plan-category.component';

export const AdminLayoutRoutes: Routes = [

    { path: 'dashboard', component: DashboardComponent, data: { breadcrumb: 'DashBoard' } },
    { path: 'company', component: CompanyComponent },
    { path: 'company-profile/:Company_ID', component: CompanyProfileComponent },
    { path: 'company-profile/view/:Company_ID', component: CompanyProfileComponent },
    { path: 'provider-profile/:Provider_ID', component: ProviderProfileComponent },
    { path: 'provider-profile/view/:Provider_ID', component: ProviderProfileComponent },
    { path: 'provider', component: ProviderComponent, data: { breadcrumb: 'Provider' } },
    { path: 'employee-profile/:Employee_ID', component: EmployeeProfileComponent },
    { path: 'employee-profile/view/:Employee_ID', component: EmployeeProfileComponent },
    { path: 'employee', component: EmployeeComponent },

    { path: 'dataManager/master-tpasetup', component: MasterTPASetupComponent, data: { breadcrumb: 'DataManager > TPA Setup > Master TPA ID Setup' } },
    { path: 'dataManager/master-tpasetup-profile/:Master_TPA_Setup_ID', component: MasterTPAsetupProfileComponent, data: { breadcrumb: 'DataManager > TPA Setup > Master TPA ID Setup > Save' } },
    { path: 'dataManager/master-tpasetup-profile/view/:Master_TPA_Setup_ID', component: MasterTPAsetupProfileComponent, data: { breadcrumb: 'DataManager > TPA Setup > Master TPA ID Setup > View' } },

    { path: 'dataManager/master/master-tpa-benefit', component: MasterTpaBenefitsComponent, data: { breadcrumb: 'DataManager > TPA Setup > Master TPA ID Setup > Master TPA Float' } },
    { path: 'dataManager/master/master-tpa-service', component: ServiceLevelComponent, data: { breadcrumb: 'DataManager > TPA Setup > Master TPA ID Setup > Master TPA Subscription' } },

    { path: 'dataManager/master-tpa', component: MasterTPAComponent, data: { breadcrumb: 'DataManager > TPA Setup > Master TPA Company Setup' } },
    { path: 'dataManager/master-tpa-profile/:MasterTPAID', component: MasterTPAProfileComponent, data: { breadcrumb: 'DataManager > TPA Setup > Master TPA Company Setup > Save' } },
    { path: 'dataManager/master-tpa-profile/view/:MasterTPAID', component: MasterTPAProfileComponent, data: { breadcrumb: 'DataManager > TPA Setup > Master TPA Company Setup > View' } },

    { path: 'dataManager/dependent', component: DependentComponent, data: { breadcrumb: 'DataManager > MemberShip > Dependants Master' } },
    // { path: 'dataManager/dependent/:Principal_ID', component: DependentComponent },
    { path: 'dataManager/dependent-profile/:Dependent_ID', component: DependentProfileComponent, data: { breadcrumb: 'DataManager > MemberShip > Dependants Master > Update' } },
    { path: 'dataManager/dependent-profile/view/:Dependent_ID', component: DependentProfileComponent, data: { breadcrumb: 'DataManager > MemberShip > Dependants Master > View' } },

    { path: 'dataManager/tpamember', component: TPAmemberComponent, data: { breadcrumb: 'DataManager > TPA Setup > TPA' } },
    { path: 'dataManager/tpamember-profile/:TPA_Member_ID', component: TPAmemberProfileComponent, data: { breadcrumb: 'DataManager > TPA Setup > TPA > Update' } },
    { path: 'dataManager/tpamember-profile/view/:TPA_Member_ID', component: TPAmemberProfileComponent, data: { breadcrumb: 'DataManager > TPA Setup > TPA > View' } },

    { path: 'dataManager/tpamember/import/list', component: TpaMemberImportListComponent },
    { path: 'dataManager/tpamember/import', component: TpaMemberImportComponent },
    { path: 'dataManager/tpamember/import/edit', component: EditTpaMemberImportComponent },

    { path: 'dataManager/clinic', component: ClinicComponent, data: { breadcrumb: 'DataManager > Dental Services > All Clinics' } },
    { path: 'dataManager/clinic-profile/:Clinic_ID', component: ClinicProfileComponent, data: { breadcrumb: 'DataManager > Dental Services > All Clinics > Update' } },
    { path: 'dataManager/clinic-profile/view/:Clinic_ID', component: ClinicProfileComponent, data: { breadcrumb: 'DataManager > Dental Services > All Clinics > View' } },

    { path: 'dataManager/branch', component: BranchComponent, data: { breadcrumb: 'DataManager > TPA Setup > Branch' } },
    { path: 'dataManager/branch-profile/:Branch_ID', component: BranchProfileComponent, data: { breadcrumb: 'DataManager > TPA Setup > Branch > Update' } },
    { path: 'dataManager/branch-profile/view/:Branch_ID', component: BranchProfileComponent, data: { breadcrumb: 'DataManager > TPA Setup > Branch > View' } },

    //Product Routing
    { path: 'dataManager/product', component: ProductComponent, data: { breadcrumb: 'DataManager > TPA Setup > Product' } },
    { path: 'dataManager/product/add', component: AddProductComponent, data: { breadcrumb: 'DataManager > TPA Setup > Product > Add' } },
    { path: 'dataManager/product-profile/:Product_ID', component: AddProductComponent, data: { breadcrumb: 'DataManager > TPA Setup > Product > Update' } },
    { path: 'dataManager/product-profile/view/:Product_ID', component: AddProductComponent, data: { breadcrumb: 'DataManager > TPA Setup > Product > View' } },


    //Doctor Routing
    { path: 'dataManager/doctor', component: DoctorComponent, data: { breadcrumb: 'DataManager > Dental Services > Doctors > Doctors Master' } },
    { path: 'dataManager/doctor/add', component: AddDoctorComponent, data: { breadcrumb: 'DataManager > Dental Services > Doctors > Doctors Master > Add' } },
    { path: 'dataManager/doctor-profile/:Doctor_ID', component: AddDoctorComponent, data: { breadcrumb: 'DataManager > Dental Services > Doctors > Doctors Master > Update' } },
    { path: 'dataManager/doctor-profile/view/:Doctor_ID', component: AddDoctorComponent, data: { breadcrumb: 'DataManager > Dental Services > Doctors > Doctors Master > View' } },

    { path: 'dataManager/doctor/clinics', component: DoctorClinicsComponent, data: { breadcrumb: 'DataManager > Dental Services > Doctors > Doctor-Clinic' } },
    { path: 'dataManager/doctor/clinics/add', component: AddDoctorClinicsComponent, data: { breadcrumb: 'DataManager > Dental Services > Doctors > Doctor-Clinic > Add' } },
    { path: 'dataManager/doctor-clinic-profile/:Doctor_Clinic_ID', component: AddDoctorClinicsComponent, data: { breadcrumb: 'DataManager > Dental Services > Doctors > Doctor-Clinic >  Update' } },
    { path: 'dataManager/doctor-clinic-profile/view/:Doctor_Clinic_ID', component: AddDoctorClinicsComponent, data: { breadcrumb: 'DataManager > Dental Services > Doctors > Doctor-Clinic > View' } },

    { path: 'dataManager/toothSurfaceCode', component: ToothSurfaceMasterComponent, data: { breadcrumb: 'DataManager > Dental Services > Tooth Surface Master' } },
    { path: 'dataManager/toothSurfaceCode/add', component: AddToothSurfaceMasterComponent, data: { breadcrumb: 'DataManager > Dental Services > Tooth Surface Master > Add' } },
    { path: 'dataManager/toothSurfaceCode/:Tooth_Surface_Code_ID', component: AddToothSurfaceMasterComponent, data: { breadcrumb: 'DataManager > Dental Services > Tooth Surface Master > Update' } },
    { path: 'dataManager/toothSurfaceCode/view/:Tooth_Surface_Code_ID', component: AddToothSurfaceMasterComponent, data: { breadcrumb: 'DataManager > Dental Services > Tooth Surface Master >  View' } },

    { path: 'dataManager/specialistEquipment', component: SpecialistEquipmentComponent, data: { breadcrumb: 'DataManager > Dental Services > Specialist Equipment' } },
    { path: 'dataManager/specialistEquipment/add', component: AddSpecialistEquipmentComponent, data: { breadcrumb: 'DataManager > Dental Services > Specialist Equipment > Add' } },
    { path: 'dataManager/specialistEquipment/:Special_Equipment_ID', component: AddSpecialistEquipmentComponent, data: { breadcrumb: 'DataManager > Dental Services > Specialist Equipment > Update' } },
    { path: 'dataManager/specialistEquipment/view/:Special_Equipment_ID', component: AddSpecialistEquipmentComponent, data: { breadcrumb: 'DataManager > Dental Services > Specialist Equipment > View' } },

    { path: 'dataManager/treatmentPlan', component: TreatmentplanComponent },
    { path: 'dataManager/treatmentPlan/add', component: AddtreatmentplanComponent },
    { path: 'dataManager/treatmentPlan/:Treatment_Plan_ID', component: AddtreatmentplanComponent },
    { path: 'dataManager/treatmentPlan/view/:Treatment_Plan_ID', component: AddtreatmentplanComponent },

    // { path: 'dataManager/treatmentPlanCategory', component: TreatmetPlanCategoryComponent },
    { path: 'dataManager/treatmentPlanCategory', component: AddTreatmetPlanCategoryComponent },
    { path: 'dataManager/treatmentPlanCategory/:Treatment_Plan_Category_ID', component: AddTreatmetPlanCategoryComponent },
    { path: 'dataManager/treatmentPlanCategory/view/:Treatment_Plan_Category_ID', component: AddTreatmetPlanCategoryComponent },

    { path: 'dataManager/treatmentPlanCompany', component: CompanyTreatmentPlanComponent },
    { path: 'dataManager/treatmentPlanCompany/add', component: AddCompanyTreatmentPlanComponent },
    { path: 'dataManager/treatmentPlanCompany/:Treatment_Plan_Company_ID', component: AddCompanyTreatmentPlanComponent },
    { path: 'dataManager/treatmentPlanCompany/view/:Treatment_Plan_Company_ID', component: AddCompanyTreatmentPlanComponent },

    { path: 'dataManager/insuranceCompany', component: InsuranceCompanyComponent },
    { path: 'dataManager/insuranceCompany/add', component: AddInsuranceCompanyComponent },
    { path: 'dataManager/insuranceCompany/:Insurer_ID', component: AddInsuranceCompanyComponent },
    { path: 'dataManager/insuranceCompany/view/:Insurer_ID', component: AddInsuranceCompanyComponent },

    { path: 'dataManager/servicetype', component: ServiceTypeComponent },
    { path: 'dataManager/servicetype/add', component: AddServiceTypeComponent },
    { path: 'dataManager/servicetype/:Service_Type_ID', component: AddServiceTypeComponent },
    { path: 'dataManager/servicetype/view/:Service_Type_ID', component: AddServiceTypeComponent },

    { path: 'dataManager/clinicType', component: ClinicTypeComponent },
    { path: 'dataManager/clinicType/add', component: AddClinicTypeComponent },
    { path: 'dataManager/clinicType/:Service_Type_ID', component: AddClinicTypeComponent },
    { path: 'dataManager/clinicType/view/:Service_Type_ID', component: AddClinicTypeComponent },


    //TreatmentManager
    { path: 'treatmentManager/toothCode', component: ToothCodeComponent },
    { path: 'treatmentManager/toothCode/add', component: AddToothCodeComponent },
    { path: 'treatmentManager/toothCode/:Tooth_Code_ID', component: AddToothCodeComponent },
    { path: 'treatmentManager/toothCode/view/:Tooth_Code_ID', component: AddToothCodeComponent },

    { path: 'treatmentManager/treatmentCode', component: TreatmentCodeComponent },
    { path: 'treatmentManager/treatmentCode/add', component: AddTreatmentCodeComponent },
    { path: 'treatmentManager/treatmentCode/:Treatment_Code_ID', component: AddTreatmentCodeComponent },
    { path: 'treatmentManager/treatmentCode/view/:Treatment_Code_ID', component: AddTreatmentCodeComponent },

    { path: 'treatmentManager/treatmentcode/clinics', component: TreatmentCodeClinicsComponent },
    { path: 'treatmentManager/treatmentcode/clinics/add', component: AddTreatmentCodeClinicsComponent },
    { path: 'treatmentManager/treatmentcode/clinics/:Treatment_Code_Clinic_ID', component: AddTreatmentCodeClinicsComponent },
    { path: 'treatmentManager/treatmentcode/clinics/view/:Treatment_Code_Clinic_ID', component: AddTreatmentCodeClinicsComponent },


    { path: 'treatmentManager/treatmentGroup', component: TreatmentGroupComponent },
    { path: 'treatmentManager/treatmentGroup/add', component: AddTreatmentGroupComponent },
    { path: 'treatmentManager/treatmentGroup/:Treatment_Group_ID', component: AddTreatmentGroupComponent },
    { path: 'treatmentManager/treatmentGroup/view/:Treatment_Group_ID', component: AddTreatmentGroupComponent },


    { path: 'treatmentManager/treatmentCategory', component: TreatmentCategoryComponent },
    { path: 'treatmentManager/treatmentCategory/add', component: AddTreatmentCategoryComponent },
    { path: 'treatmentManager/treatmentCategory/:Treatment_Category_ID', component: AddTreatmentCategoryComponent },
    { path: 'treatmentManager/treatmentCategory/view/:Treatment_Category_ID', component: AddTreatmentCategoryComponent },

    { path: 'treatmentManager/treatmentMasterSetup', component: TreatmentMasterComponent },
    { path: 'treatmentManager/treatmentMasterSetup/add', component: AddTreatmentMasterComponent },
    { path: 'treatmentManager/treatmentMasterSetup/:Treatment_Master_ID', component: AddTreatmentMasterComponent },
    { path: 'treatmentManager/treatmentMasterSetup/view/:Treatment_Master_ID', component: AddTreatmentMasterComponent },

    { path: 'typography', component: TypographyComponent },
    { path: 'icons', component: IconsComponent },
    { path: 'maps', component: MapsComponent },
    { path: 'notifications', component: NotificationsComponent },
    { path: 'upgrade', component: UpgradeComponent },
];
