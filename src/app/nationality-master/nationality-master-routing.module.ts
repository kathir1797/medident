import { AddNationalityComponent } from './nationality/add-nationality/add-nationality.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NationalityComponent } from './nationality/nationality.component';


const routes: Routes = [
  {
    path: '',
    component: NationalityComponent
  },
  {
    path: 'addnationality',
    component: AddNationalityComponent
  },
  {
    path:'addnationality/:Nationality_ID',
    component:AddNationalityComponent
  },
  {
    path:'addnationality/view/:Nationality_ID',
    component:AddNationalityComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NationalityMasterRoutingModule { }
