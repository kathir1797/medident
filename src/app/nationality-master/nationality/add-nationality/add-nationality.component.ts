import { NationalityMasterService } from 'app/services/dataManager/nationality-master.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-add-nationality',
  templateUrl: './add-nationality.component.html',
  styleUrls: ['./add-nationality.component.css']
})
export class AddNationalityComponent implements OnInit {

  nationalityMaster: FormGroup;
  toptitle: string = 'Add Nationality';
  btntext: string = 'Save';
  urlText: any;
  isDisabled: boolean = true;
  obj = localStorage.getItem('currentuser');

  constructor(private fb: FormBuilder, private snackBar: MatSnackBar, private router: Router, private activated: ActivatedRoute, private nation: NationalityMasterService) {
    const url = this.router.url;
    this.urlText = url.split('/');
    this.initializeNationalityForm();
    this.activated.params.subscribe(param => {
      if (this.urlText[3] != undefined) {
        this.toptitle = 'Edit Nationality Master'
        this.btntext = 'Update';
        this.nation.getNationality(param.Nationality_ID).subscribe(response => {
          if (response.success) {
            this.nationalityMaster.patchValue({
              Nationality_ID: response.data[0].Nationality_ID,
              Nationality_Name: response.data[0].Nationality_Name,
            })
          } else {
            this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 })
          }
        });
      }
      if (this.urlText[3] == 'view') {
        this.toptitle = 'View Nationality Master';
        this.isDisabled = false
      }
    })
  }

  ngOnInit(): void {
  }

  addNationality() {
    this.nation.addNationality(JSON.stringify(this.nationalityMaster.value)).subscribe(response => {
      if (response.success) {
        this.nationalityMaster.reset();
        this.router.navigate(['/nationality']);
        this.snackBar.open('Nationality Saved Successfully', 'ok', { duration: 3000 })
      } else {
        this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 })
      }
    })
  }

  initializeNationalityForm() {
    this.nationalityMaster = this.fb.group({
      Nationality_ID: 0,
      Nationality_Name: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
    })
  }

}
