import { AddTpaBenefitComponent } from './tpa-benefit/add-tpa-benefit/add-tpa-benefit.component';
import { TpaBenefitComponent } from './tpa-benefit/tpa-benefit.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {
    path: 'tpabenefits',
    component: TpaBenefitComponent
  },
  {
    path: 'tpabenefits/add',
    component: AddTpaBenefitComponent
  },
  {
    path: 'tpabenefits/:TPA_Benefit_ID',
    component: AddTpaBenefitComponent
  },
  {
    path: 'tpabenefits/view/:TPA_Benefit_ID',
    component: AddTpaBenefitComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BenefitOptionRoutingModule { }
