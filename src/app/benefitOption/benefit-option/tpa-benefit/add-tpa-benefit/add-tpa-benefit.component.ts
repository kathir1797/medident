import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router, ActivatedRoute } from '@angular/router';
import { TpaBenefitsService } from 'app/services/benefitoption/tpa-benefits.service';
import { DdlistService } from 'app/services/config/ddlist.service';
import { stringify } from 'querystring';

@Component({
  selector: 'app-add-tpa-benefit',
  templateUrl: './add-tpa-benefit.component.html',
  styleUrls: ['./add-tpa-benefit.component.css']
})
export class AddTpaBenefitComponent implements OnInit {

  tpaBenefitsForm: FormGroup;
  statusList = new Array();
  toptitle: string = 'Add Benefit Option';
  btntext: string = 'Save';
  urlText: any;
  isDisabled: boolean = true;
  obj = localStorage.getItem('currentuser');

  constructor(private fb: FormBuilder, private ddlist: DdlistService, private snackBar: MatSnackBar,
    private router: Router, private activatedRoute: ActivatedRoute, private tpaBenefits: TpaBenefitsService) {
    const url = this.router.url;
    this.urlText = url.split('/');
    this.initializeBenefitForm();
    this.activatedRoute.params.subscribe(param => {
      if (this.urlText[3] != 'add') {
        this.toptitle = 'Update Benefit Option'
        this.btntext = 'Update';
        this.tpaBenefits.getTPABenefits(param.TPA_Benefit_ID).subscribe(response => {
          if (response.success) {
            this.tpaBenefitsForm.patchValue(response.data[0])
          } else {
            this.snackBar.open(response.errorMessage, 'OK', { duration: 3000 });
          }
        })
      }
      if (this.urlText[3] == 'view') {
        this.toptitle = 'View TPA Benefit'
        this.isDisabled = false;
      }
    })
  }

  ngOnInit(): void {
    this.getStatusDDLists();
  }

  initializeBenefitForm() {
    this.tpaBenefitsForm = this.fb.group({
      TPA_Benefit_ID: 0,
      Benefit_Name: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      Model_Type: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      Flat_Fee: new FormControl({ value: 0, disabled: this.urlText[3] == 'view' ? true : false }),
      Service_Fee: new FormControl({ value: 0, disabled: this.urlText[3] == 'view' ? true : false }),
      Patient_Discount: new FormControl({ value: 0, disabled: this.urlText[3] == 'view' ? true : false }),
      Clinic_Admin_Fee: new FormControl({ value: 0, disabled: this.urlText[3] == 'view' ? true : false }),
      Company_State_ID: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      Claim_Type: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      // Threshold_Warning: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      // Threshold_Hold: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      // Threshold_Stop: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      Start_Date: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      End_Date: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      Benefit_Status_ID: new FormControl({ value: 1, disabled: this.urlText[3] == 'view' ? true : false }),
      Invoice_Day: new FormControl({ value: 1, disabled: this.urlText[3] == 'view' ? true : false }, [Validators.min(1), Validators.max(31)]),
      Invoice_Frequency: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      User_ID: JSON.parse(this.obj).User_ID,
    })
  }

  getStatusDDLists() {
    this.ddlist.getStatusDDList().subscribe(response => {
      if (response.success) {
        this.statusList = response.data;
      } else {
        this.snackBar.open(response.errorMessage, 'OK', { duration: 3000 });
      }
    });
  }

  saveTPABenefits() {
    if (this.tpaBenefitsForm.get('Benefit_Name').value != "" && this.tpaBenefitsForm.get('Benefit_Name').value != null && this.tpaBenefitsForm.get('Benefit_Name').value != undefined) {
      this.tpaBenefits.addTPABenefits(JSON.stringify(this.tpaBenefitsForm.value)).subscribe(response => {
        if (response.success) {
          this.tpaBenefitsForm.reset();
          this.router.navigate(['/benefitOption/tpabenefits']);
          this.snackBar.open('TPABenefit Saved Successfully', 'OK', { duration: 3000 });
        } else {
          this.snackBar.open(response.errorMessage, 'OK', { duration: 3000 });
        }
      });
    } else {
      this.snackBar.open('Enter Benefit Name', 'OK', { duration: 3000 });
    }

  }

}
