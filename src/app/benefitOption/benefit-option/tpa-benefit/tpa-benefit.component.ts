import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { TpaBenefitsService } from 'app/services/benefitoption/tpa-benefits.service';
import { TpaUserService } from 'app/services/users/tpa-user.service';
import { param } from 'jquery';

@Component({
  selector: 'app-tpa-benefit',
  templateUrl: './tpa-benefit.component.html',
  styleUrls: ['./tpa-benefit.component.css']
})
export class TpaBenefitComponent implements OnInit, AfterViewInit {

  @ViewChild(MatPaginator) paginator: MatPaginator;
  dataSource = new MatTableDataSource<any>();
  length: number
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['Benefit_Name', 'Model_Type', 'Claim_Type', 'Invoice_Day', 'Invoice_Frequency', 'Start_Date', 'End_Date', 'Action'];

  constructor(private snackBar: MatSnackBar, private tpaBenefits: TpaBenefitsService) {

  }

  ngOnInit(): void {
    this.tpaBenefits.getTPABenefits(0).subscribe(response => {
      if (response.success) {
        this.dataSource.data = response.data
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      } else {
        this.dataSource.data = []
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.snackBar.open(response.errorMessage, 'OK', { duration: 3000 });
      }
    })
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort
  }

}
