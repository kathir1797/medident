import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TpaBenefitComponent } from './tpa-benefit.component';

describe('TpaBenefitComponent', () => {
  let component: TpaBenefitComponent;
  let fixture: ComponentFixture<TpaBenefitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TpaBenefitComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TpaBenefitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
