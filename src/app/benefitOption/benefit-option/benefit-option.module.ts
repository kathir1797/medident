import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BenefitOptionRoutingModule } from './benefit-option-routing.module';
import { TpaBenefitComponent } from './tpa-benefit/tpa-benefit.component';
import { AddTpaBenefitComponent } from './tpa-benefit/add-tpa-benefit/add-tpa-benefit.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatRippleModule, MatNativeDateModule } from '@angular/material/core';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatRadioModule } from '@angular/material/radio';
import { MatSelectModule } from '@angular/material/select';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { MatTooltipModule } from '@angular/material/tooltip';
import { BreadcrumbModule } from 'angular-crumbs';
import { ComponentsModule } from 'app/components/components.module';


@NgModule({
  declarations: [TpaBenefitComponent, AddTpaBenefitComponent],
  imports: [
    CommonModule,
    BenefitOptionRoutingModule,
    ComponentsModule,
    FormsModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatRippleModule,
    MatNativeDateModule,
    MatInputModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatTooltipModule,
    MatSnackBarModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatIconModule,
    MatDatepickerModule,
    MatCardModule,
    MatCheckboxModule,
    MatSidenavModule,
    BreadcrumbModule,
    MatRadioModule,
    MatCardModule,
    MatListModule,
  ]
})
export class BenefitOptionModule { }
