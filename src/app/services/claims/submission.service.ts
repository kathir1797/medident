import { ConfigService } from './../config/config.service';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SubmissionService {

  apiRoute = this.configService.api;

  constructor(private configService: ConfigService) { }

  getDoctorsByClinicID(id) {
    return this.configService.contactApiWithoutToken('post', `${this.apiRoute}/getDoctorsByClinicID`, JSON.stringify({ Clinic_ID: id }))
  }

  getPatientDetails(id) {
    return this.configService.contactApiWithoutToken('post', `${this.apiRoute}/getPatientDetails`, JSON.stringify({ Key_Value: id }))
  }

  saveTPAClaims(data) {
    return this.configService.contactApiWithoutToken('post', `${this.apiRoute}/addTPAClaims`, data)
  }

  getTPAClaims(data) {
    return this.configService.contactApiWithoutToken('post', `${this.apiRoute}/getTPAClaims`, JSON.stringify({ TPA_Claim_ID: data }))
  }

  saveToothCodeTPAClaims(data) {
    return this.configService.contactApiWithoutToken('post', `${this.apiRoute}/addClaimToothCode`, data)
  }

  saveToothSurfaceCodeClaims(data) {
    return this.configService.contactApiWithoutToken('post', `${this.apiRoute}/addClaimSurface`, data)
  }

  getToothCodeByTreatmentCodeID(id) {
    return this.configService.contactApiWithoutToken('post', `${this.apiRoute}/toothCodeByTreatmentCode`, JSON.stringify({ Treatment_Code_ID: id }))
  }

  checkTPAClaimPatient(data) {
    return this.configService.contactApiWithoutToken('post', `${this.apiRoute}/checkTPAClaimPatient`, data)
  }

  saveTPAClaimDetails(data) {
    return this.configService.contactApiWithoutToken('post', `${this.apiRoute}/addTPAClaimsDetails`, data)
  }

  getTPAClaimDetails(data) {
    return this.configService.contactApiWithoutToken('post', `${this.apiRoute}/getTPAClaimDetails`, JSON.stringify({ TPA_Claim_ID: data }))
  }

  patientEligibleCheck(data) {
    return this.configService.contactApiWithoutToken(
      'post',
      `${this.apiRoute}/patientEligibility`,
      JSON.stringify(
        {
          Patient_ID: data.Patient_ID,
          Dependent_ID: data.Dependent_ID,
          Clinic_ID: data.Clinic_ID
        }
      )
    );
  }

}
