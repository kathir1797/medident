import { Injectable } from '@angular/core';
import { ConfigService } from '../config/config.service';

@Injectable({
  providedIn: 'root'
})
export class InvoiceService {

  apiRoute = `${this.configService.api}`;

  constructor(private configService: ConfigService) { }

  generateInvoiceNumber(customerType: string) {
    return this.configService.contactApiWithoutToken('post', `${this.apiRoute}/getInvoiceNumber`, JSON.stringify({ Customer_Type: customerType }))
  }

  saveCompanyInvoice(data) {
    return this.configService.contactApiWithoutToken('post', `${this.apiRoute}/addTPACompanyInvoice`, data)
  }

  saveCompanyInvoiceDetails(data) {
    return this.configService.contactApiWithoutToken('post', `${this.apiRoute}/addTPACompanyInvoiceDetails`, data)
  }

  saveClinicInvoice(data) {
    return this.configService.contactApiWithoutToken('post', `${this.apiRoute}/addTPAClinicInvoice`, data)
  }

  saveClinicInvoiceDetails(data) {
    return this.configService.contactApiWithoutToken('post', `${this.apiRoute}/addTPAClinicInvoiceDetails`, data)
  }
}
