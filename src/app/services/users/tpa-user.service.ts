import { ConfigService } from './../config/config.service';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TpaUserService {

  apiRoute = `${this.configService.api}`;

  constructor(private configService: ConfigService) { }

  addUsers(data) {
    return this.configService.contactApiWithoutToken('post', `${this.apiRoute}/addTPAUser`, data)
  }


  getTPAUsers(userRequest) {
    return this.configService.contactApiWithoutToken('post', `${this.apiRoute}/getTPAUser`, JSON.stringify({ User_ID: userRequest }))
  }


  addTPAUserType(data) {
    return this.configService.contactApiWithoutToken('post', `${this.apiRoute}/addTPAUserType`, data)
  }


  getTPAUserType(userTypeRequest) {
    return this.configService.contactApiWithoutToken('post', `${this.apiRoute}/getTPAUserType`, JSON.stringify({ User_Type_ID: userTypeRequest }))
  }

  addUserClinic(data) {
    return this.configService.contactApiWithoutToken('post', `${this.apiRoute}/adduserclinic`, data)
  }

  getUserClinic(id) {
    return this.configService.contactApiWithoutToken('post', `${this.apiRoute}/getuserclinic`, JSON.stringify({ User_Clinic_ID: id }))
  }

}
