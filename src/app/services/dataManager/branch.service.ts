import { Injectable } from '@angular/core';
import { ConfigService } from '../config/config.service';
import { branchRequest } from 'app/models/dataManager';

@Injectable({
  providedIn: 'root'
})
export class BranchService {

  apiRoute = `${this.configService.api}`;

  constructor(private configService: ConfigService) { }

  getBranchMaster(branchList:branchRequest){
    return this.configService.contactApiWithoutToken('post', `${this.apiRoute}/getbranch`, branchList)
  }

  addBranchMaster(data) {
    return this.configService.contactApiWithoutToken('post', `${this.apiRoute}/addbranch`, data)
  }

  editBranchMaster(Branch_ID) {
    return this.configService.contactApiWithoutToken('post', `${this.apiRoute}/getbranch`, {Branch_ID:Branch_ID})
  }

}

 