import { Injectable } from '@angular/core';
import { ConfigService } from '../config/config.service';
import { dependentRequest } from 'app/models/dataManager';

@Injectable({
  providedIn: 'root'
})
export class DependentService {
  apiRoute = `${this.configService.api}`;

  constructor(private configService: ConfigService) { }

  getDependentList(dependentList:dependentRequest){
    return this.configService.contactApiWithoutToken('post', `${this.apiRoute}/getdependents`, dependentList)
  }

  addDependentData(data) {
    return this.configService.contactApiWithoutToken('post', `${this.apiRoute}/adddependents`, data)
  }

  editDependentData(Dependent_ID) {
    return this.configService.contactApiWithoutToken('post', `${this.apiRoute}/getdependents`, {Dependent_ID:Dependent_ID})
  }

}
