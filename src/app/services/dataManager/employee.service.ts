import { Injectable } from '@angular/core';
import { ConfigService } from '../config/config.service';
import { employeeRequest } from 'app/models/employeeRequest';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  apiRoute = `${this.configService.api}`;

  constructor(private configService: ConfigService) { }

  getEmployeeList(employeeList:employeeRequest){
    return this.configService.contactApiWithoutToken('post', `${this.apiRoute}/getemployee`, employeeList)
  }

  addEmployee(data) {
    return this.configService.contactApiWithoutToken('post', `${this.apiRoute}/addemployee`, data)
  }

  EditEmployee(Employee_ID) {
    return this.configService.contactApiWithoutToken('post', `${this.apiRoute}/getemployee`, {Employee_ID:Employee_ID})
  }

  getDependentByPrincipal(id) {
    return this.configService.contactApiWithoutToken('post', `${this.apiRoute}/getdependentsByPrincipal`, JSON.stringify({ Prinicipal_ID: id }))
  }
}
