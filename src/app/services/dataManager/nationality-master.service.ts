import { ConfigService } from './../config/config.service';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class NationalityMasterService {

  apiRoute = `${this.configService.api}`;

  constructor(private configService: ConfigService) { }

  addNationality(data) {
    return this.configService.contactApiWithoutToken('post', `${this.apiRoute}/addNationality`, data)
  }

  getNationality(nationalityID) {
    return this.configService.contactApiWithoutToken('post', `${this.apiRoute}/getNationality`, JSON.stringify({ Nationality_ID: nationalityID }))
  }

}
