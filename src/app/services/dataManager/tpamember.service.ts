import { Injectable } from '@angular/core';
import { ConfigService } from '../config/config.service';
import { tpaMemberImport, tpaMemberRequest } from 'app/models/dataManager';

@Injectable({
  providedIn: 'root'
})
export class TpamemberService {

  apiRoute = `${this.configService.api}`;

  constructor(private configService: ConfigService) { }

  getTPAMember(TPAMenderList: tpaMemberRequest) {
    return this.configService.contactApiWithoutToken('post', `${this.apiRoute}/getTPAMember`, TPAMenderList)
  }

  addTPAMember(data) {
    return this.configService.contactApiWithoutToken('post', `${this.apiRoute}/addTPAMember`, data)
  }

  editTPAMember(TPA_Member_ID) {
    return this.configService.contactApiWithoutToken('post', `${this.apiRoute}/getTPAMember`, { TPA_Member_ID: TPA_Member_ID })
  }

  generateTPAID() {
    return this.configService.contactApiWithoutToken('get', `${this.apiRoute}/generateTPAID`)
  }

  //#region TPAMemberImport
  generateBatchTPAMemberImport() {
    return this.configService.contactApiWithoutToken('get', `${this.apiRoute}/generateTPAImportBatch`)
  }

  saveTPAMemberImport(tpaMemberImport) {
    return this.configService.contactApiWithoutToken('post', `${this.apiRoute}/addTPAImportMember`, tpaMemberImport)
  }

  getTPAMemberImportList(batchNumber) {
    return this.configService.contactApiWithoutToken('post', `${this.apiRoute}/getTPAMemberImport`, JSON.stringify({ Batch_Number: batchNumber }))
  }

  inserTPAMemberImport() {
    return this.configService.contactApiWithoutToken('post', `${this.apiRoute}/inserTPAMemberImport`)
  }

  updateTPAMemberImportBatchNumber(batchNumber) {
    return this.configService.contactApiWithoutToken('post', `${this.apiRoute}/updateBatchNumberTPAMemberImport`, JSON.stringify({ Batch_Number: batchNumber }))
  }

  getBatchnumberTPAMemberImport() {
    return this.configService.contactApiWithoutToken('post', `${this.apiRoute}/getBatchNumberTPAMemberImport`)
  }
  //#endregion TPAMemberImport

  //#region TreatmentPlan

  getTreatmentPlan(id) {
    return this.configService.contactApiWithoutToken('post', `${this.apiRoute}/getTreatmentPlan`, JSON.stringify({ Treatment_Plan_ID: id }))
  }

  addTreatmentPlan(data) {
    return this.configService.contactApiWithoutToken('post', `${this.apiRoute}/addTreatmentPlan`, data)
  }

  addTreatmentGroupLimit(data) { //TreatmentGroupLimit--Treatment plan
    return this.configService.contactApiWithoutToken('post', `${this.apiRoute}/addTreatmentPlanGroupLimit`, data)
  }

  getTreatmentPlanGroupLimit(id) {
    return this.configService.contactApiWithoutToken('post', `${this.apiRoute}/getTreatmentPlanGroupLimit`, JSON.stringify({ Treatment_Plan_Group_Limit_ID: id }))
  }

  addTreatmentCodeLimit(data) { //TreatmentLimit--Treatment plan
    return this.configService.contactApiWithoutToken('post', `${this.apiRoute}/addTreatmentPlanCodeLimit`, data)
  }

  getTreatmentPlanCodeLimit(id) {
    return this.configService.contactApiWithoutToken('post', `${this.apiRoute}/getTreatmentPlanCodeLimit`, JSON.stringify({ Treatment_Plan_Code_Limit_ID: id }))
  }

  deleteTreatmentPlanGroup(treatmentPlanID) {
    return this.configService.contactApiWithoutToken('post', `${this.apiRoute}/deleteTreatmentPlanGroup`, JSON.stringify({ Treatment_Plan_ID: treatmentPlanID }))
  }

  deleteTreatmentPlanCode(treatmentPlanID) {
    return this.configService.contactApiWithoutToken('post', `${this.apiRoute}/deleteTreatmentPlanCode`, JSON.stringify({ Treatment_Plan_ID: treatmentPlanID }))
  }

  //#endregion TreatmentPlan



}
