import { ConfigService } from './../config/config.service';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CityMasterService {

  apiRoute = `${this.configService.api}`;

  constructor(private configService: ConfigService) { }

  addCity(cityData) {
    return this.configService.contactApiWithoutToken('post', `${this.apiRoute}/addCityMaster`, cityData)
  }

  getCity(cityid) {
    return this.configService.contactApiWithoutToken('post', `${this.apiRoute}/getCityMaster`, JSON.stringify({ City_ID: cityid }))
  }
}
