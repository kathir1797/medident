import { Injectable } from '@angular/core';
import { ConfigService } from '../config/config.service';
import { msterTPASetupRequest } from 'app/models/dataManager';

@Injectable({
  providedIn: 'root'
})
export class MasterTPASetupService {

  apiRoute = `${this.configService.api}`;

  constructor(private configService: ConfigService) { }

  getMasterTPASetup(masterTPAList: msterTPASetupRequest) {
    return this.configService.contactApiWithoutToken('post', `${this.apiRoute}/getmastersetup`, masterTPAList)
  }

  addMasterTPASetup(data) {
    return this.configService.contactApiWithoutToken('post', `${this.apiRoute}/addmastersetup`, data)
  }

  editMasterTPASetup(Master_TPA_Setup_ID) {
    return this.configService.contactApiWithoutToken('post', `${this.apiRoute}/getmastersetup`, { Master_TPA_Setup_ID: Master_TPA_Setup_ID })
  }

  generateMASTERTPAID() {
    return this.configService.contactApiWithoutToken('get', `${this.apiRoute}/generateMasterTPAID`)
  }

}
