import { Insurer } from './../../models/dataManager';
import { Injectable } from '@angular/core';
import { ConfigService } from '../config/config.service';

@Injectable({
  providedIn: 'root'
})
export class InsurerService {

  apiRoute = `${this.configService.api}`;

  constructor(private configService: ConfigService) { }

  getInsurer(insurerReq: Insurer) {
    return this.configService.contactApiWithoutToken('post', `${this.apiRoute}/getInsurer`, insurerReq)
  }

  uploadInsurerImage(file){
    return this.configService.contactFileUploadAPI('post', `${this.apiRoute}/uploadInsurerImage`, file)
  }

  addInsuranceCompany(data){
    return this.configService.contactApiWithoutToken('post', `${this.apiRoute}/addInsurer`, data)
  }

}
