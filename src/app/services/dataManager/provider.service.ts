import { Injectable } from '@angular/core';
import { ConfigService } from '../config/config.service';
import { providerRequest } from 'app/models/ProviderRequest';

@Injectable({
  providedIn: 'root'
})
export class ProviderService {

  apiRoute = `${this.configService.api}`;

  constructor(private configService: ConfigService) { }

  getProvider(providerList:providerRequest){
    return this.configService.contactApiWithoutToken('post', `${this.apiRoute}/getprovider`, providerList)
  }

  addProvider(data) {
    return this.configService.contactApiWithoutToken('post', `${this.apiRoute}/addprovider`, data)
  }

  EditProvider(Provider_ID) {
    return this.configService.contactApiWithoutToken('post', `${this.apiRoute}/getprovider`, {Provider_ID:Provider_ID})
  }

}
