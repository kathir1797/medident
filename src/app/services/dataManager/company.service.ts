import { Injectable } from '@angular/core';
import { ConfigService } from '../config/config.service';
import { getCompanyRequest } from 'app/models/getCompanyRequest';

@Injectable({
  providedIn: 'root'
})
export class CompanyService {

  apiRoute = `${this.configService.api}`;

  constructor(private configService: ConfigService) { }

  addCompany(data) {
    return this.configService.contactApiWithoutToken('post', `${this.apiRoute}/addcompany`, data)
  }

  EditCompany(Company_ID) {
    return this.configService.contactApiWithoutToken('post', `${this.apiRoute}/getcompany`, {Company_ID:Company_ID})
  }

  getCompany(compayList:getCompanyRequest){
    return this.configService.contactApiWithoutToken('post', `${this.apiRoute}/getcompany`, compayList)
  }

  deleteCompany(Company_ID) {
    return this.configService.contactApiWithoutToken('post', `${this.apiRoute}/deletecompany`, {Company_ID:Company_ID})
  }
}
