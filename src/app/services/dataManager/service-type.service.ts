import { Injectable } from '@angular/core';
import { ConfigService } from '../config/config.service';

@Injectable({
  providedIn: 'root'
})
export class ServiceTypeService {

  apiRoute = `${this.configService.api}`;

  constructor(private configService: ConfigService) { }

  addServiceType(data) {
    return this.configService.contactApiWithoutToken('post', `${this.apiRoute}/addServiceType`, data)
  }

  getServiceType(id) {
    return this.configService.contactApiWithoutToken('post', `${this.apiRoute}/getServiceType`, JSON.stringify({ Service_Type_ID: id }))
  }
}
