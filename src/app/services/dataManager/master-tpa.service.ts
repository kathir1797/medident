import { Injectable } from '@angular/core';
import { ConfigService } from '../config/config.service';
import { msterTPARequest } from 'app/models/dataManager';

@Injectable({
  providedIn: 'root'
})
export class MasterTPAService {

  apiRoute = `${this.configService.api}`;

  constructor(private configService: ConfigService) { }

  getMasterTPA(masterTPAList:msterTPARequest){
    return this.configService.contactApiWithoutToken('post', `${this.apiRoute}/getmasterTPA`, masterTPAList)
  }

  addMasterTPA(data) {
    return this.configService.contactApiWithoutToken('post', `${this.apiRoute}/addmasterTPA`, data)
  }

  editMasterTPA(MasterTPAID) {
    return this.configService.contactApiWithoutToken('post', `${this.apiRoute}/getmasterTPA`, {MasterTPAID:MasterTPAID})
  }

  generateNewMASTERTPAID(){
    return this.configService.contactApiWithoutToken('get', `${this.apiRoute}/generateNewMasterTPAID`)
  }

}
