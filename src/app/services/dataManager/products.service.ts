import { Injectable } from '@angular/core';
import { ConfigService } from '../config/config.service';
import { productRequest } from 'app/models/dataManager';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  apiRoute = `${this.configService.api}`;

  constructor(private configService: ConfigService) { }

  getProductMaster(productList:productRequest){
    return this.configService.contactApiWithoutToken('post', `${this.apiRoute}/getproduct`, productList)
  }

  addProductMaster(data) {
    return this.configService.contactApiWithoutToken('post', `${this.apiRoute}/addproduct`, data)
  }

  editProductMaster(Product_ID) {
    return this.configService.contactApiWithoutToken('post', `${this.apiRoute}/getproduct`, {Product_ID:Product_ID})
  }
}
