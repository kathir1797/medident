import { Injectable } from '@angular/core';
import { doctorRequest } from 'app/models/dataManager';
import { ConfigService } from '../config/config.service';

@Injectable({
  providedIn: 'root'
})
export class DoctorService {

  apiRoute = `${this.configService.api}`;

  constructor(private configService: ConfigService) { }

  addDoctor(doctorRequest) {
    return this.configService.contactApiWithoutToken('post', `${this.apiRoute}/addDoctor`, doctorRequest);
  }

  getDoctor(doctorList: doctorRequest) {
    return this.configService.contactApiWithoutToken('post', `${this.apiRoute}/getDoctor`, doctorList);
  }

  addDoctorClinic(doctorClinicRequest) {
    return this.configService.contactApiWithoutToken('post', `${this.apiRoute}/addDoctorClinic`, doctorClinicRequest);
  }

  getDoctorClinicList(doctorClinicId) {
    return this.configService.contactApiWithoutToken('post', `${this.apiRoute}/getDoctorClinicList`, doctorClinicId);
  }
}
