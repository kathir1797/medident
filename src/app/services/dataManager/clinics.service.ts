import { apiResponseObj } from './../../models/apiResponse';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { clinicsRequest } from 'app/models/dataManager';
import { ConfigService } from '../config/config.service';

@Injectable({
  providedIn: 'root'
})
export class ClinicsService {

  apiRoute = `${this.configService.api}`;

  constructor(private configService: ConfigService) { }

  getClinicsList(cliniList: clinicsRequest): Observable<apiResponseObj> {
    return this.configService.contactApiWithoutToken('post', `${this.apiRoute}/getClinics`, cliniList)
  }

  addClinics(data) {
    return this.configService.contactApiWithoutToken('post', `${this.apiRoute}/addClinics`, data)
  }

  EditEditClinics(Clinic_ID) {
    return this.configService.contactApiWithoutToken('post', `${this.apiRoute}/getClinics`, { Clinic_ID: Clinic_ID })
  }

  addClinicTypes(data) {
    return this.configService.contactApiWithoutToken('post', `${this.apiRoute}/addClinicType`, data)
  }

  getClinictypes(id) {
    return this.configService.contactApiWithoutToken('post', `${this.apiRoute}/getClinicType`, JSON.stringify({ Clinic_Type_ID: id }))
  }
}
