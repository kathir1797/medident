import { stringify } from 'querystring';
import { ConfigService } from './../config/config.service';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SupportService {

  apiRoute = this.configService.api;

  constructor(private configService: ConfigService) { }


  getRequestType() {
    return this.configService.contactApiWithoutToken('post', `${this.apiRoute}/getRequestType`)
  }

  saveServiceRequest(data) {
    return this.configService.contactApiWithoutToken('post', `${this.apiRoute}/saveServiceRequest`, data)
  }

  getServiceRequest(data) {
    return this.configService.contactApiWithoutToken('post', `${this.apiRoute}/getServiceRequest`, JSON.stringify(data))
  }
}
