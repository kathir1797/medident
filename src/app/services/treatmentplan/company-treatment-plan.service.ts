
import { Injectable } from '@angular/core';
import { companyTreatmentPlanRequest } from 'app/models/dataManager';
import { ConfigService } from '../config/config.service';

@Injectable({
  providedIn: 'root'
})
export class CompanyTreatmentPlanService {

  apiRoute = `${this.configService.api}`;

  constructor(private configService: ConfigService) { }

  addCompanyTreatmentPlan(data) {
    return this.configService.contactApiWithoutToken('post', `${this.apiRoute}/addTreatmentPlanCompany`, data)
  }

  getCompanyTreatmentPlan(data: companyTreatmentPlanRequest) {
    return this.configService.contactApiWithoutToken('post', `${this.apiRoute}/getTreatmentPlanCompany`, data)
  }
}
