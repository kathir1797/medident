import { ConfigService } from './../config/config.service';
import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class TpaBenefitsService {

  apiRoute = `${this.configService.api}`;

  constructor(private configService: ConfigService) { }

  addTPABenefits(data) {
    return this.configService.contactApiWithoutToken('post', `${this.apiRoute}/addTPABenefits`, data)
  }

  getTPABenefits(id) {
    return this.configService.contactApiWithoutToken('post', `${this.apiRoute}/getTPABenefits`, JSON.stringify({ TPA_Benefit_ID: id }))
  }

  //Master TPA Company Setup
  addmasterTPAbenefits(data) {
    return this.configService.contactApiWithoutToken('post', `${this.apiRoute}/addMasterTPABenefits`, data)
  }

  getMasterTPAbenefits(tpaid, benefitID) {
    return this.configService.contactApiWithoutToken('post', `${this.apiRoute}/getMasterTPABenefits`, JSON.stringify({ Master_TPA_FLoat_ID: benefitID, Master_TPA_ID: tpaid }))
  }

  //MasterTPA Subscription Setup
  saveMasterTPASubscriptionSetup(data) {
    return this.configService.contactApiWithoutToken('post', `${this.apiRoute}/saveMasterTPASubscription`, data)
  }

  getMasterTPASubscription(id, tpaid) {
    return this.configService.contactApiWithoutToken('post', `${this.apiRoute}/getMasterTPASubscription`, JSON.stringify({ Master_TPA_Subscription_ID: id, MasterTPAID: tpaid }))
  }

  //Custom Validators for Float and Subscription in MasterTPA Benefits
  rangeValidation(control: string) {
    if (control == 'Flat_Fee' || control == 'Float_Amount') {
      return (formGroup: FormGroup) => {
        const rangeControl = formGroup.controls[control];
        if (!rangeControl) {
          return null;
        }

        if (rangeControl.errors && !rangeControl.errors.flat) {
          return null;
        }

        if (rangeControl.value < 0) {
          rangeControl.setErrors({ flat: true });
        } else {
          rangeControl.setErrors(null);
        }
      }
    }
    if (control == 'Service_Fee' || control == 'Threshold_Stop' || control == 'Threshold_Warning' || control == 'Threshold_Hold') {
      return (formGroup: FormGroup) => {
        const rangeControl = formGroup.controls[control];
        if (!rangeControl) {
          return null;
        }

        if (rangeControl.errors && !rangeControl.errors.service) {
          return null;
        }

        if (rangeControl.value < 0 || rangeControl.value > 100) {
          rangeControl.setErrors({ service: true });
        } else {
          rangeControl.setErrors(null);
        }
      }
    }
  }

}
