import { treatmentGroup } from './../../models/treatmentManager';
import { ConfigService } from '../config/config.service';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TreatmentGroupService {

  apiRoute = `${this.configService.api}`;

  constructor(private configService: ConfigService) { }

  addTreatmentGroup(data) {
    return this.configService.contactApiWithoutToken('post', `${this.apiRoute}/addTreatmentGroup`, data);
  }

  getTreatmentGroup(data: treatmentGroup) {
    return this.configService.contactApiWithoutToken('post', `${this.apiRoute}/getTreatmentGroup`, data)
  }
}
