import { Injectable } from '@angular/core';
import { treatmentMasterSetup } from 'app/models/treatmentManager';
import { ConfigService } from '../config/config.service';

@Injectable({
  providedIn: 'root'
})
export class TreatmentMasterService {

  apiRoute = `${this.configService.api}`;

  constructor(private configService: ConfigService) { }

  addTreatmentMaster(data) {
    return this.configService.contactApiWithoutToken('post', `${this.apiRoute}/addTreatmentMaster`, data)
  }

  getTreatmentMaster(data: treatmentMasterSetup) {
    return this.configService.contactApiWithoutToken('post', `${this.apiRoute}/getTreatmentMaster`, data)
  }

  addTreatmentMasterToothCode(data) {
    return this.configService.contactApiWithoutToken('post', `${this.apiRoute}/addTreatmentMasterToothCode`, data)
  }

  getTreatmentMasterToothCode(data: treatmentMasterSetup) {
    return this.configService.contactApiWithoutToken('post', `${this.apiRoute}/getTreatmentMasterToothCode`, data)
  }

  deleteTreatmentToothMaster(masterID) {
    return this.configService.contactApiWithoutToken('post', `${this.apiRoute}/deleteTreatmentMasterToothCode`, JSON.stringify({ Treatment_master_ID: masterID }))
  }

  getTreatmentMasterUnassignedCode() {
    return this.configService.contactApiWithoutToken('post', `${this.apiRoute}/getTreatmentMasterUnassignedCode`)
  }

}
