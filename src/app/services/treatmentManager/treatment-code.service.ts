import { ConfigService } from '../config/config.service';
import { Injectable } from '@angular/core';
import { treatmentCodeByClinic, treatmentCodeRequest } from 'app/models/treatmentManager';

@Injectable({
  providedIn: 'root'
})
export class TreatmentCodeService {

  apiRoute = `${this.configService.api}`;

  constructor(private configService: ConfigService) { }

  addTreatmentCode(dataRequest) {
    return this.configService.contactApiWithoutToken('post', `${this.apiRoute}/addTreatmentCode`, dataRequest)
  }

  getTreatmentCode(treatmentCodeReq: treatmentCodeRequest) {
    return this.configService.contactApiWithoutToken('post', `${this.apiRoute}/getTreatmentCode`, treatmentCodeReq)
  }

  addTreatmentCodeByClinics(data) {
    return this.configService.contactApiWithoutToken('post', `${this.apiRoute}/addTreatmentCodeByClinic`, data)
  }

  getTreatmentCodeByClinic(dataRequest: treatmentCodeByClinic) {
    return this.configService.contactApiWithoutToken('post', `${this.apiRoute}/getTreatmentCodeByClinic`, dataRequest)
  }
}
