import { Injectable } from '@angular/core';
import { treatmentCategory } from 'app/models/treatmentManager';
import { ConfigService } from '../config/config.service';

@Injectable({
  providedIn: 'root'
})
export class TreatmentCategoryService {

  apiRoute = `${this.configService.api}`;

  constructor(private configService: ConfigService) { }

  addTreatmentCategory(data) {
    return this.configService.contactApiWithoutToken('post', `${this.apiRoute}/addTreatmentCategory`, data)
  }


  getTreatmentCategory(data: treatmentCategory) {
    return this.configService.contactApiWithoutToken('post', `${this.apiRoute}/getTreatmentCategory`, data)
  }

  saveTreatmentPlanCategory(data) {
    return this.configService.contactApiWithoutToken('post', `${this.apiRoute}/addTreatmentPlanCategory`, data)
  }

  getTreatmentPlanCategory(id) {
    return this.configService.contactApiWithoutToken('post', `${this.apiRoute}/getTreatmentPlanCategory`, JSON.stringify({ Treatment_Plan_Category_ID: id }))
  }

  getUnassignedTreatmentCategory(id) {
    return this.configService.contactApiWithoutToken('post', `${this.apiRoute}/getTreatmentCategoryDD`, JSON.stringify({ Treatment_Plan_ID: id }))
  }

}
