import { apiResponseObj } from './../../models/apiResponse';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { toothCodeSurface } from 'app/models/dataManager';
import { ConfigService } from '../config/config.service';

@Injectable({
  providedIn: 'root'
})
export class ToothCodeService {

  apiRoute = `${this.configService.api}`;

  constructor(private configService: ConfigService) { }

  addToothCode(data) {
    return this.configService.contactApiWithoutToken('post', `${this.apiRoute}/addToothCode`, data)
  }

  getToothCode(toothCodeReq): Observable<apiResponseObj> {
    return this.configService.contactApiWithoutToken('post', `${this.apiRoute}/getToothCode`, toothCodeReq)
  }

  addToothSurfaceCode(data) {
    return this.configService.contactApiWithoutToken('post', `${this.apiRoute}/addToothSurface`, data)
  }

  getToothSurfaceCode(data: toothCodeSurface) {
    return this.configService.contactApiWithoutToken('post', `${this.apiRoute}/getToothSurface`, data)
  }
}
