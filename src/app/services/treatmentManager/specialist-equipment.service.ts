import { ConfigService } from '../config/config.service';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SpecialistEquipmentService {

  apiRoute = `${this.configService.api}`;

  constructor(private configService: ConfigService) { }

  addSpecialistEquipmentCode(data){
    return this.configService.contactApiWithoutToken('post', `${this.apiRoute}/addSpecialEquipment`, data)
  }

  getSpecialistEquipementCode(data){
    return this.configService.contactApiWithoutToken('post', `${this.apiRoute}/getSpecialEquipment`, data)
  }
}
