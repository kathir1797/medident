import { Injectable } from '@angular/core';
import { login } from 'app/models/userlogin';
import { ConfigService } from '../config/config.service';


@Injectable({
  providedIn: 'root'
})
export class LoginService {

  apiRoute = `${this.sf.api}`;

  constructor(private sf: ConfigService) { }

  login(user: login) {
    return this.sf.contactApiWithoutToken('post', `${this.apiRoute}/login`, user);
  }

  loginUserDetails(userDetails: login) {
    return this.sf.contactApi('post', `${this.apiRoute}/loginDetails`, userDetails)
  }
}


