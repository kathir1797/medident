import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { environment } from 'environments/environment';
import { apiResponseObj } from 'app/models/apiResponse';
import configJson from '../../../assets/config/config.json';

@Injectable({
  providedIn: 'root'
})
export class ConfigService {

  api: string = environment.api;
  configJson = configJson
  constructor(private http: HttpClient,
    private router: Router) {
  }

  // Contacting Api without token
  contactApiWithoutToken = (method: string, endPoint: string, data?: any): Observable<apiResponseObj> => {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
      })
    };

    switch (method) {
      case 'get': return this.http.get<apiResponseObj>(endPoint);
      case 'post': return this.http.post<apiResponseObj>(endPoint, data, httpOptions);
    }
  }

  // Contacting Api
  contactApi = (method: string, endPoint: string, data?: any): Observable<apiResponseObj> => {
    const token = this.getFromLocalStorage("token");
    const httpOptions = {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*",
        Authorization: 'Bearer ' + token,
      }),
    };

    switch (method) {
      case "get":
        return this.http.get<apiResponseObj>(endPoint, httpOptions);
      case "post":
        return this.http.post<apiResponseObj>(endPoint, data, httpOptions);
    }
  };

  contactFileUploadAPI = (
    method: string,
    endPoint: string,
    data?: any
  ): Observable<apiResponseObj> => {
    const options = {
      headers: new HttpHeaders({ "Access-Control-Allow-Origin": "*" }),
    };

    switch (method) {
      case "get":
        return this.http.get<apiResponseObj>(endPoint);
      case "post":
        return this.http.post<apiResponseObj>(endPoint, data, options);
    }
  };

  getFromLocalStorage(item: string) {
    if (localStorage.getItem(item) != null) {
      return localStorage.getItem(item);
    }
  }

}
