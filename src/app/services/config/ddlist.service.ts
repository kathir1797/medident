import { Injectable } from '@angular/core';
import { ConfigService } from '../config/config.service';

@Injectable({
  providedIn: 'root'
})
export class DdlistService {

  apiRoute = `${this.configService.api}`;

  constructor(private configService: ConfigService) { }

  getStateList() {
    return this.configService.contactApiWithoutToken('get', `${this.apiRoute}/stateddlist`)
  }

  getCityList() {
    return this.configService.contactApiWithoutToken('get', `${this.apiRoute}/cityddlist`)
  }

  getProviderList() {
    return this.configService.contactApiWithoutToken('get', `${this.apiRoute}/providerddlist`)
  }

  getUserList() {
    return this.configService.contactApiWithoutToken('get', `${this.apiRoute}/usertypeddlist`)
  }

  getNationalityDDList() {
    return this.configService.contactApiWithoutToken('get', `${this.apiRoute}/nationalityddlist`)
  }

  getRaceDDList() {
    return this.configService.contactApiWithoutToken('get', `${this.apiRoute}/raceddlist`)
  }

  getGenderDDList() {
    return this.configService.contactApiWithoutToken('get', `${this.apiRoute}/genderddlist`)
  }

  getMaritialStatusDDList() {
    return this.configService.contactApiWithoutToken('get', `${this.apiRoute}/maritialstatusddlist`)
  }

  getCompanyDDList() {
    return this.configService.contactApiWithoutToken('get', `${this.apiRoute}/companyddlist`)
  }

  getEmployeeDDList() {
    return this.configService.contactApiWithoutToken('get', `${this.apiRoute}/employeeddlist`)
  }

  getRelationsShipDDList() {
    return this.configService.contactApiWithoutToken('get', `${this.apiRoute}/relationshipddlist`)
  }

  getMasterMPADDList() {
    return this.configService.contactApiWithoutToken('get', `${this.apiRoute}/masterTPAddlist`)
  }

  getMasterTPASetupDDList() {
    return this.configService.contactApiWithoutToken('get', `${this.apiRoute}/masterTPASetupddlist`)
  }

  getZoneDDList() {
    return this.configService.contactApiWithoutToken('get', `${this.apiRoute}/zoneddlist`)
  }

  getClinicTypesDDList() {
    return this.configService.contactApiWithoutToken('get', `${this.apiRoute}/clinictypesddlist`)
  }

  getStatusDDList() {
    return this.configService.contactApiWithoutToken('get', `${this.apiRoute}/statusddlist`)
  }

  getBranchDDList() {
    return this.configService.contactApiWithoutToken('get', `${this.apiRoute}/branchddlist`)
  }

  getProductDDList() {
    return this.configService.contactApiWithoutToken('get', `${this.apiRoute}/productddlist`)
  }

  getCustomerType(id) {
    return this.configService.contactApiWithoutToken('post', `${this.apiRoute}/customerTypeddlist`, JSON.stringify({ Customer_Type_ID: id }))
  }

  getTaxType(id) {
    return this.configService.contactApiWithoutToken('post', `${this.apiRoute}/taxTypeddlist`, JSON.stringify({ Tax_Type_ID: id }))
  }
}
