import { Router, ActivatedRoute } from '@angular/router';
import { CityMasterService } from './../../../services/dataManager/city-master.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { DdlistService } from 'app/services/config/ddlist.service';

@Component({
  selector: 'app-add-city-master',
  templateUrl: './add-city-master.component.html',
  styleUrls: ['./add-city-master.component.css']
})
export class AddCityMasterComponent implements OnInit {

  cityMaster: FormGroup;
  toptitle: string = 'Add City';
  btntext: string = 'Save';
  urlText: any;
  isDisabled: boolean = true;
  obj = localStorage.getItem('currentuser');
  statusList = new Array();
  stateList = new Array();

  constructor(private fb: FormBuilder, private ddlist: DdlistService, private snackBar: MatSnackBar,
    private cityService: CityMasterService, private router: Router, private activated: ActivatedRoute) {
    const url = this.router.url;
    this.urlText = url.split('/');
    console.log(this.urlText);
    this.initializeCityForm();
    this.activated.params.subscribe(param => {
      if (this.urlText[3] != undefined) {
        this.toptitle = 'Edit City Master'
        this.btntext = 'Update';
        this.cityService.getCity(param.City_ID).subscribe(response => {
          if (response.success) {
            this.cityMaster.patchValue({
              City_ID: response.data[0].City_ID,
              City_Name: response.data[0].City_Name,
              State_ID: response.data[0].State_ID,
              City_Status_ID: response.data[0].Status_ID,
              User_ID: JSON.parse(this.obj).User_ID
            })
          } else {
            this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 })
          }
        });
      }
      if (this.urlText[3] == 'view') {
        this.toptitle = 'View City Master';
        this.isDisabled = false
      }
    })
  }

  ngOnInit(): void {
    this.getStatusDDLists();
    this.getStateDDList();
  }

  getStatusDDLists() {
    this.ddlist.getStatusDDList().subscribe(response => {
      if (response.success) {
        this.statusList = response.data;
      } else {
        this.snackBar.open(response.errorMessage, 'OK', { duration: 3000 });
      }
    });
  }

  getStateDDList() {
    this.ddlist.getStateList().subscribe(response => {
      if (response.success) {
        this.stateList = response.data;
      } else {
        this.snackBar.open(response.errorMessage, 'OK', { duration: 3000 });
      }
    });
  }

  initializeCityForm() {
    this.cityMaster = this.fb.group({
      City_ID: 0,
      City_Name: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      State_ID: new FormControl({ value: '', disabled: this.urlText[3] == 'view' ? true : false }),
      City_Status_ID: new FormControl({ value: 1, disabled: this.urlText[3] == 'view' ? true : false }),
      User_ID: JSON.parse(this.obj).User_ID
    })
  }

  addCity() {
    this.cityService.addCity(JSON.stringify(this.cityMaster.value)).subscribe(response => {
      if (response.success) {
        this.cityMaster.reset();
        this.router.navigate(['/city']);
        this.snackBar.open('City Saved Successfully', 'ok', { duration: 3000 })
      } else {
        this.snackBar.open(response.errorMessage, 'ok', { duration: 3000 })
      }
    })
  }

}
