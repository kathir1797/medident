import { AddCityMasterComponent } from './city-master/add-city-master/add-city-master.component';
import { CityMasterComponent } from './city-master/city-master.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {
    path: '',
    component: CityMasterComponent
  },
  {
    path: 'addCity',
    component: AddCityMasterComponent
  },
  {
    path: 'addCity/:City_ID',
    component: AddCityMasterComponent
  },
  {
    path: 'addCity/view/:City_ID',
    component: AddCityMasterComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CitiesRoutingModule { }
